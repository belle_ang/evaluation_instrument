import time

import numpy as np
import pyvisa

timetrack = []


class OnlineE4980A:
    def __init__(self):
        self.inst: pyvisa.resources.MessageBasedResource = pyvisa.ResourceManager().open_resource("GPIB0::17::INSTR")
        self.inst.timeout = 600000
        self.initial_system()

    def iwrite(self, command):
        time_start = time.perf_counter()
        self.inst.write(command)
        timetrack.append([command, time.perf_counter() - time_start])

    def iread(self, command):
        time_start = time.perf_counter()
        read_message = self.inst.query(command)
        timetrack.append([command, time.perf_counter() - time_start])
        return read_message

    def initial_system(self):
        idn = self.iread("*IDN?")
        self.iwrite(":FUNC:IMP CPG")
        self.iwrite("FORM:DATA ASCII")
        self.iwrite("TRIG:SOUR BUS")
        return idn

    def set_freq_4980(self, frequency_ = 9999):
        time_start = time.perf_counter()
        self.frequency = self.__check_frequency(frequency_)
        self.iwrite(f"FREQ:CW {self.frequency}")
        return time.perf_counter() - time_start

    def set_cmu84_4980(self, integration_, signal_level_):
        time_start = time.perf_counter()
        integration_ = self.__check_integration(integration_)
        signal_level_ = self.__check_signal_level(signal_level_)
        self.iwrite("VOLT " + str(signal_level_) + "V")
        self.iwrite("APER " + integration_)
        self.iwrite("BIAS:STAT OFF")
        self.iwrite("BIAS:VOLT 0")
        return time.perf_counter() - time_start

    def force_v_4980(self, output_voltage):
        time_start = time.perf_counter()
        output_voltage = self.__check_voltage(output_voltage)
        self.iwrite("BIAS:VOLT " + str(output_voltage) + "V")
        self.iwrite("BIAS:STAT ON")
        time_stamp = time.perf_counter() - time_start
        return time_stamp

    def measure_cmu84_4980(self, measurement_range = 0):
        time_start = time.perf_counter()
        if measurement_range == 0:
            self.iwrite("FUNC:IMP:RANG:AUTO ON")
        else:
            self.iwrite("FUNC:IMP:RANG:AUTO OFF")
            # self.iwrite("FUNC:IMP:RANG:VAL {}".format(measurement_range))
            self.iwrite("FUNC:IMP:RANG " + str(measurement_range) + "OHM")

        # Spot Measurement
        self.iwrite("DISP:PAGE MEAS")
        # self.iwrite("TRIG:SOUR BUS") # todo
        self.iwrite(":ABOR")
        self.iwrite("DISP:PAGE MEAS")
        self.iwrite("TRIG:IMM")


        capacitance, conductance, status = self.iread("FETC?").split(",")
        print('original: ', capacitance, conductance)
        capacitance, conductance = float(capacitance), float(conductance)
        l_hptrx, l_usrhptrx_h, l_usrhptrx_l, l_usrhpcoax_h, l_usrhpcoax_l = 3, 0, 0, 4, 4
        capacitance, conductance = self.compen_5250(self.frequency, l_hptrx, l_usrhptrx_h, l_usrhptrx_l,
                                                    l_usrhpcoax_h,
                                                    l_usrhpcoax_l, capacitance, conductance)
        time_stamp = time.perf_counter() - time_start
        return capacitance, conductance


        # try:
        #     capacitance, conductance, status = self.iread("FETC?").split(",")
        #     print (capacitance, conductance)
        #     capacitance, conductance = float(capacitance), float(conductance)
        #     l_hptrx, l_usrhptrx_h, l_usrhptrx_l, l_usrhpcoax_h, l_usrhpcoax_l = 3, 0, 0, 4, 4
        #     capacitance, conductance = self.compen_5250(self.frequency, l_hptrx, l_usrhptrx_h, l_usrhptrx_l,
        #                                                 l_usrhpcoax_h,
        #                                                 l_usrhpcoax_l, capacitance, conductance)
        #     time_stamp = time.perf_counter() - time_start
        #     return status, time_stamp, capacitance, conductance
        # except:
        #     print("***Fetch data failed!")

    def set_cv84_4980(self, start_voltage, stop_voltage, step_number, hold_time = 0, delay_time = 0):
        time_start = time.perf_counter()
        step_aperture = (stop_voltage - start_voltage) / (step_number - 1)
        self.bias_values = str(
            [x for x in np.arange(start_voltage, stop_voltage + step_aperture, step_aperture)]
        ).strip("[]")
        self.iwrite("TRIG:DEL {}".format(delay_time))
        self.iwrite("TRIG:TDEL {}".format(hold_time))
        self.iwrite("LIST:MODE SEQ")
        self.iwrite(":LIST:BIAS:VOLT " + self.bias_values)

        time_stamp = time.perf_counter() - time_start
        return time_stamp

    def sweep_cv84_4980(self):
        time_start = time.perf_counter()
        self.iwrite("BIAS:STAT ON")
        self.iwrite("DISP:PAGE LIST")
        self.iwrite("DISP:ENAB OFF")
        self.iwrite("INIT:CONT ON")
        self.iwrite(":TRIG:IMM")

        time.sleep(0.1)
        capacitance_lst, conductance_lst, status = [], [], []

        fetch_data = self.iread(":FETC?").split(",")
        for skip_step in range(0, len(fetch_data), 4):
            capacitance_lst.append(fetch_data[skip_step])
            conductance_lst.append(fetch_data[skip_step + 1])

        self.iwrite("BIAS:STAT OFF")
        bias_value = self.bias_values.split(",")
        bias_value = [float(dc) for dc in bias_value]
        return time.perf_counter() - time_start, capacitance_lst, conductance_lst, bias_value

    def __check_frequency(self, frequency_):
        if frequency_ < 20 or frequency_ > 2e6:
            print("*** ErrorIncorrect Input Frequency Range:20Hz to 2MHz")
        else:
            return frequency_

    def __check_integration(self, integration_):
        integration_mode = {1: "SHOR", 2: "MED", 3: "LONG"}
        try:
            integration_speed = integration_mode[integration_]
            return integration_speed
        except:
            print("*** Error Integration {} , set to default MED".format(integration_))
            return "MED"

    def __check_signal_level(self, signal_level_):
        if 0 <= signal_level_ <= 20:
            return str(signal_level_)
        else:
            print("*** Error signal level {} , set to default 0.03V".format(signal_level_))
            return "0.03"

    def __check_voltage(self, voltage_):
        if -40 <= voltage_ <= 40:
            return str(voltage_)
        else:
            print("***ERROR voltage:{} ,set to default 2V".format(voltage_))
            return "2"


    def compen_5250(self, freq_val, l_hptrx, l_usrhptrx_h, l_usrhptrx_l, l_usrhpcoax_h, l_usrhpcoax_l, raw_cap, raw_g):
        """

        :param freq_val: Measured frequency [Hz]
        :param l_hptrx: HP Triax Cable Length [m]
        :param l_usrhptrx_h: User Triax Cable Length (High) [m]
        :param l_usrhptrx_l: User Triax Cable Length (Low) [m]
        :param l_usrhpcoax_h: User Coax Cable Length (High) [m]
        :param l_usrhpcoax_l: User Coax Cable Length (Low) [m]
        :param raw_cap: R_c. Raw Capacitance Data
        :param raw_g: R_g. Raw Conductance Data
        :return: c_c: Compensated Capacitance Data; c_g: Compensated Conductance Data; sta: status
        """
        # Rlc_data:
        #               R [ohm]       L [H]         C [F]
        #       DATA1   74.65E-3,  140.00E-9,   58.44E-12  ! Frame Path 1
        #       DATA2   75.41E-3,   90.00E-9,   67.13E-12  ! Frame Path 2
        #       DATA3  231.41E-3,  450.00E-9,  178.85E-12  ! Card Path High
        #       DATA4  177.56E-3,  390.00E-9,  135.45E-12  ! Card Path Low
        #       DATA5  100.70E-3,  400.00E-9,   80.00E-12  ! HP Triax Cable   [/m]
        #       DATA6  100.70E-3,  400.00E-9,   80.00E-12  ! User Triax Cbl H [/m]
        #       DATA7  100.70E-3,  400.00E-9,   80.00E-12  ! User Triax Cbl L [/m]
        #       DATA8  114.00E-3,  544.00E-9,  130.00E-12  ! User Coax Cbl H  [/m]
        #       DATA9  114.00E-3,  544.00E-9,  130.00E-12  ! User Coax Cbl L  [/m]
        #       DATA10   0.00E-3,    0.00E-9,    1.20E-12  ! Stray Capacitance


        data1 = [0, 0.07465, 0.00000014, 0.00000000005844]
        data2 = [0, 0.07541, 0.00000009, 0.00000000006713]
        data3 = [0, 0.23141, 0.00000045, 0.00000000017885]
        data4 = [0, 0.17756, 0.00000039, 0.00000000013545]
        data5 = [0, 0.1007, 0.0000004, 0.00000000008]
        data6 = [0, 0.1007, 0.0000004, 0.00000000008]
        data7 = [0, 0.1007, 0.0000004, 0.00000000008]
        data8 = [0, 0.114, 0.000000544, 0.00000000013]
        data9 = [0, 0.114, 0.000000544, 0.00000000013]
        data10 = [0, 0, 0, 0.0000000000012]

        pi = 3.14159265359

        if l_hptrx < 0 or l_usrhptrx_h < 0 or l_usrhptrx_l < 0 or l_usrhpcoax_h < 0 or l_usrhpcoax_l < 0:
            sys.exit("*** Incorrect Input Parameter is not correct.")

        # Adjust Factor Value
        for i in range(1, 4):
            data5[i] = data5[i] * l_hptrx
            data6[i] = data5[i] * l_usrhptrx_h
            data7[i] = data5[i] * l_usrhptrx_l
            data8[i] = data5[i] * l_usrhpcoax_h
            data9[i] = data5[i] * l_usrhpcoax_l

        # Calculate Coef Value
        tmp1 = 2 * data1[3] + 4 * data2[3] + 2 * data3[3] + 2 * data4[3] + 4 * data5[3] + 2 * data6[3] \
               + 2 * data7[3] + 2 * data8[3] + 2 * data9[3]


        # tmp = 2
        tmp2 = 2 * data2[3] + 2 * data3[3] + 2 * data4[3] + 4 * data5[3] + 2 * data6[3] + 2 * data7[3] \
               + 2 * data8[3] + 2 * data9[3]
        tmp3 = 1 * data3[3] + 2 * data5[3] + 2 * data6[3] + 2 * data8[3]
        tmp4 = 1 * data4[3] + 2 * data5[3] + 2 * data7[3] + 2 * data9[3]
        tmp5 = 2 * data5[3] + 2 * data6[3] + 2 * data7[3] + 2 * data8[3] + 2 * data9[3]
        tmp6 = 1 * data6[3] + 2 * data8[3]
        tmp7 = 1 * data7[3] + 2 * data9[3]
        tmp8 = 1 * data8[3] * data8[2] + 1 * data9[3] * data9[2]


        coef = [54088, 54088, 54088, 54088, 54088]
        coef[0] = -2 * pi * pi * (
                tmp1 * data1[2] + tmp2 * data2[2] + tmp3 * data3[2] + tmp4 * data4[2] + tmp5 * data5[2] +
                tmp6 * data6[2] + tmp7 * data7[2] + tmp8)

        tmp8 = 1 * data8[3] * data8[1] + 1 * data9[3] * data9[1]

        coef[1] = pi * (
                tmp1 * data1[1] + tmp2 * data2[1] + tmp3 * data3[1] + tmp4 * data4[1] + tmp5 * data5[1] +
                tmp6 * data6[1] + tmp7 * data7[1] + tmp8)
        coef[2] = 2 * data1[1] + 2 * data2[1] + 1 * data3[1] + 1 * data4[1] + 2 * data5[1] + 1 * data6[1] + \
                  1 * data7[1] + 1 * data8[1] + 1 * data9[1]
        coef[3] = pi * (
                4 * data1[2] + 4 * data2[2] + 2 * data3[2] + 2 * data4[2] + 4 * data5[2] +
                2 * data6[2] + 2 * data7[2] + 2 * data8[2] + 2 * data9[2])
        coef[4] = data10[3]

        # Setup Complex Value
        y0_r = 0
        y0_i = 2 * pi * freq_val * coef[4]

        coef1_r = coef[0] * freq_val * freq_val + 1
        coef1_i = coef[1] * freq_val
        coef2_r = coef[2]
        coef2_i = coef[3] * freq_val

        # Convert : Cp-G to G-B[Ym]
        ym_r = raw_g
        ym_i = 2 * pi * freq_val * raw_cap

        # Compen : Ym to Yt
        ynof_r = ym_r - y0_r
        ynof_i = ym_i - y0_i

        nume_r = ynof_r * coef1_r - ynof_i * coef1_i
        nume_i = ynof_r * coef1_i + ynof_i * coef1_r
        deno_r = 1 - (ynof_r * coef2_r - ynof_i * coef2_i)
        deno_i = -1 * (ynof_r * coef2_i + ynof_i * coef2_r)
        yt_r = (nume_r * deno_r + nume_i * deno_i) / (deno_r * deno_r + deno_i * deno_i)
        yt_i = (nume_i * deno_r - nume_r * deno_i) / (deno_r * deno_r + deno_i * deno_i)

        # Convert : G-B[Yt] to Cp-G
        c_g = yt_r
        c_c = yt_i / (2 * pi * freq_val)

        return c_c, c_g


class OfflineE4980(OnlineE4980A):
    def iwrite(self, command):
        time_start = time.perf_counter()
        print(f"instrument send:{command}")
        timetrack.append([command, time.perf_counter() - time_start])

    def iread(self, command):
        time_start = time.perf_counter()
        print(f"instrument query:{command}")
        timetrack.append([command, time.perf_counter() - time_start])
        return command


lcr = OnlineE4980A()
lcr.set_freq_4980(100e3)
lcr.set_cmu84_4980(2, 0.05)
lcr.force_v_4980(0.7)
cap, cond = lcr.measure_cmu84_4980(0)
print (cap, cond)

