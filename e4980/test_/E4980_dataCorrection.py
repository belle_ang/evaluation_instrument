from datetime import datetime
from decimal import Decimal
import pprint
from e4980.E4980A import *

"""
testcase Guide: 
1. line 10~13 (observe parameter) 增加要收的變數
2. line 22~24 : for 越內層跳越快
csv如果沒給名字會自動以時間命名
"""
## 0,2.5
## 0,40
## -40,40

def cases():
    # Observe parameter
    freq_lst = [1e3, 1e4, 1e5]
    speed_lst = [1, 2, 3]
    step_lst = [41, 71, 101, 201]

    # freq_lst = [1e5]
    # speed_lst = [1, 2, 3]
    # step_lst = [41]
    # Constant parameter
    start_v = -40
    stop_v = 40
    signal_lev = 0.05
    hold_t = delay_t = 0

    testcases = []
    for freq_ in freq_lst:
        for step_ in step_lst:
            for speed_ in speed_lst:
                testcases.append([freq_, signal_lev, speed_, start_v, stop_v, step_, hold_t, delay_t])
    return testcases


class DataCorrection:
    """
    spot: freq, tc_swp_signal_lev, speed, delay_time
    sweep: frequency, tc_swp_signal_lev, speed, start_v, stop_v, step_num, hold_time, delay_time
    spot and sweep return format : [1. setting time], [2. measure time], [3. measure result]
    """

    def __init__(self):
        self.lcr = OnlineE4980A()

    def spot(self, spot_case):
        [frequency_, signal_level_, integration_, _, output_voltage_, _, _, _,] = spot_case
        freq_time = self.lcr.set_freq_4980(frequency_)
        cmu_time = self.lcr.set_cmu84_4980(integration_, signal_level_)
        force_time = self.lcr.force_v_4980(output_voltage_)
        status, meas_time, capacitance, conductance = self.lcr.measure_cmu84_4980()
        # freq, signal_l, integra, voltage_,freq_time, cmu_time, force_time, meas_time, capacitance, conductance

        spot_res = ("{}, " * 10).format(
            frequency_,
            signal_level_,
            integration_,
            output_voltage_,
            freq_time,
            cmu_time,
            force_time,
            meas_time,
            capacitance,
            conductance,
        )
        return spot_res

    def sweep(self, swp_case):
        [
            frequency_,
            signal_level_,
            integration_,
            start_voltage_,
            stop_voltage_,
            step_number_,
            hold_time_,
            delay_time_,
        ] = swp_case

        freq_time = self.lcr.set_freq_4980(frequency_)
        cmu_time = self.lcr.set_cmu84_4980(integration_, signal_level_)
        cv_time = self.lcr.set_cv84_4980(start_voltage_, stop_voltage_, step_number_, hold_time_, delay_time_)
        measure_time, capacitance_lst, conductance_lst, bias_values = self.lcr.sweep_cv84_4980()

        # swp_title = CSV_write.sweep_title()
        swp_para = "\n{}, {}, {}, {}, {} ,{}, {}, {}, {}, {}, {}, {}, {}\n".format(
            frequency_,
            signal_level_,
            integration_,
            signal_level_,
            start_voltage_,
            stop_voltage_,
            step_number_,
            hold_time_,
            delay_time_,
            freq_time,
            cmu_time,
            cv_time,
            measure_time,
        )
        cap_cond = "\nNO.,  DC BIAS , CAP [F] , G [S] \n"
        for idx in range(0, len(capacitance_lst)):
            # cap_cond += capacitance_lst[idx] + conductance_lst[idx]
            cap_cond += "{}, {}, {} ,{}\n".format(idx + 1, bias_values[idx], capacitance_lst[idx], conductance_lst[idx])

        return swp_para + cap_cond + "\n"

    # def write_time_tracking(self):
    #     CSV_write.save_timetrack(timetrack, datetime.now().strftime("%Y%m%d_%H%M_%S"))

    def run(self, csv_filename=datetime.now().strftime("%Y%m%d_%H%M_%S")):
        # ***
        spot_data = CSV_write.measure_header("SPOT") + CSV_write.spot_title()
        sweep_data = CSV_write.measure_header("SWEEP")
        for testcase_ in cases():
            spot_data += self.spot(testcase_) + "\n"
            sweep_data += CSV_write.sweep_title() + self.sweep(testcase_)

        # Save the result to csv

        CSV_write.save_timetrack(timetrack, datetime.now().strftime("%Y%m%d_%H%M_%S"))
        data2save = CSV_write.date() + spot_data + sweep_data
        CSV_write.report(data2save, csv_filename)


class CSV_write:
    @staticmethod
    def date():

        date_str = "\n" + datetime.now().strftime("%d/%m/%Y %H:%M:%S") + "\n"
        return date_str

    @staticmethod
    def measure_header(measure_type: str):
        # return sweep/ spot title
        next2line = "\n" * 2
        underline = "_" * 50
        return next2line + underline + measure_type + underline + next2line

    @staticmethod
    def sweep_title():
        parameter = "freq, test_signal, speed, signal_level, start_voltage, stop_voltage, step_number, hold, delay,"
        read = "setfreq_time, setcmu_time,setcv_time, sweep_time"
        return "\n" + parameter + read

    @staticmethod
    def spot_title():
        parameter = "freq, test_signal, speed, voltage,"
        read = "freq_time, cmu_time, force_time, meas_time, capacitance, conductance"
        return "\n" + parameter + read + "\n"

    @staticmethod
    def report(report_context, filename2save):
        with open(filename2save + "201_tact_time.csv", "w", encoding="utf-8") as f:
            f.write(report_context)

    @staticmethod
    def save_timetrack(timetrack_lst, filename2save):
        # Convert dic to a string
        num = 1
        dict2str = "NO.     TIME[s]    COMMAND\n"
        for command_, time_ in timetrack_lst:
            dict2str += '{:>3d},  {:.3e},  "{}"\n'.format(num, time_, command_)
            num += 1

        with open("time_track_" + filename2save + ".txt", "w", encoding="utf-8") as f:
            f.write(dict2str)


if __name__ == "__main__":
    dc1 = DataCorrection()
    dc1.run()
