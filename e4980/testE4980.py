from E4980A import *

class TestE4980:
    def setup(self):
        self.e4980 = OnlineE4980("gpib2,24")

    def test_init_system(self):
        msg = 'Agilent Technologies,E4980A,MY46412635,A.06.10'
        self.assertEqual(msg, self.e4980.init_system())


    def set_freq_4980(self, set_freq_freq):
        return self.e4980.set_freq_4980(set_freq_freq)


    def set_cmu_4980(self, set_cmu_Integration, set_cmu_signal_level, set_cmu_mode):
        return self.e4980.set_cmu_4980(set_cmu_Integration, set_cmu_signal_level, set_cmu_mode)

    def test_sweep_cv(self):
        """電容預期值為level = E-8 F,
        測試條件:
        v_start:-40 v ; v_stop: 40 v ; steps: 201 ; range_: 0 (auto) ; hold_t: 0.01s ; delay_t: 0.01s ; Mode= Cp-D ; AC_level=0.04V ;Freq = 100kHz
        """
        # test return cap value
        # t1 = time()
        self.e4980.set_freq_4980(100000)
        self.e4980.set_cmu_4980(1, 0.04, 1)
        self.e4980.set_cv_4980(-10, 10, 21, 0.01, 0.01)
        # addr, cap, g_out, _, status, voltage = self.e4980.sweep_cv_4980(0)
        addr, cap, g_out, _, status, voltage = self.e4980.sweep_cv_compen_4980(0)
        print(cap)
        print(g_out)
        print(voltage)
        # self.assertAlmostEqual(4.e-8, max([float(x) for x in cap.values()]))

    @unittest.skip("test_Freq_error1")
    def test_Freq_error1(self):
        """Freq 19 Hz<20 Hz"""
        try:
            self.e4980.set_freq_4980(20)
        except Exception as e:
            self.assertEqual("FREQ ErrorIncorrect Input Frequency Range:20Hz to 2MHz", e.args[1])

    @unittest.skip("test_Freq_error2")
    def test_Freq_error2(self):
        """Freq 2.00001 MHz<2 MHz"""
        try:
            self.e4980.set_freq_4980(2000001)
        except Exception as e:
            self.assertEqual("FREQ ErrorIncorrect Input Frequency Range:20Hz to 2MHz", e.args[1])

    @unittest.skip("test_cvsweep_vstart_error1")
    def test_cvsweep_vstart_error1(self):
        """Vstart = -40.1 < -40V """
        try:
            self.e4980.set_cv_4980_wayne(-40.1, 40, 201, 0.01, 0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input V_START is not in range -40~40", e.args[1])

    @unittest.skip("test_cvsweep_vstart_error2")
    def test_cvsweep_vstart_error2(self):
        """Vstart = 40.1> 40V """
        try:
            self.e4980.set_cv_4980_wayne(40.1, -40, 201, 0.01, 0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input V_START is not in range -40~40", e.args[1])

    @unittest.skip("test_cvsweep_vstop_error1")
    def test_cvsweep_vstop_error1(self):
        """Vtsop = -40.1 < -40V """
        try:
            self.e4980.set_cv_4980_wayne(40, -40.1, 201, 0.01, 0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input V_STOP is not in range -40~40", e.args[1])

    @unittest.skip("test_cvsweep_vstop_error2")
    def test_cvsweep_vstop_error2(self):
        """Vtsop = 40.1 > 40V """
        try:
            self.e4980.set_cv_4980_wayne(-40, 40.1, 201, 0.01, 0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input V_STOP is not in range -40~40", e.args[1])

    @unittest.skip("test_cvsweep_step_error1")
    def test_cvsweep_step_error1(self):
        """Step_point = 1 < 2 """
        try:
            self.e4980.set_cv_4980_wayne(-40, 40, 1, 0.01, 0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input Step_Number is not in range 2~201", e.args[1])

    @unittest.skip("test_cvsweep_step_error2")
    def test_cvsweep_step_error2(self):
        """Step_point = 202 > 201 """
        try:
            self.e4980.set_cv_4980_wayne(-40, 40, 202, 0.01, 0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input Step_Number is not in range 2~201", e.args[1])

    @unittest.skip("test_cvsweep_hold_error1")
    def test_cvsweep_hold_error1(self):
        """Hold_time = 999.1 > 999 """
        try:
            self.e4980.set_cv_4980_wayne(-40, 40, 201, 999.1, 0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input HOLD_T is not in range 0~999", e.args[1])

    @unittest.skip("test_cvsweep_hold_error2")
    def test_cvsweep_hold_error2(self):
        """Hold_time = -0.01 < 0 """
        try:
            self.e4980.set_cv_4980_wayne(-40, 40, 201, -0.01, 0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input HOLD_T is not in range 0~999", e.args[1])

    @unittest.skip("test_cvsweep_delay_error1")
    def test_cvsweep_delay_error1(self):
        """Delay_time = 999.1 > 999 """
        try:
            self.e4980.set_cv_4980_wayne(-40, 40, 201, 0.01, 999.1)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input Delay_T is not in range 0~999", e.args[1])

    @unittest.skip("test_cvsweep_delay_error2")
    def test_cvsweep_delay_error2(self):
        """Delay_time = -0.01 < 0 """
        try:
            self.e4980.set_cv_4980_wayne(-40, 40, 201, 0.01, -0.01)
        except Exception as e:
            self.assertEqual("CV Error code: Incorrect Input Delay_T is not in range 0~999", e.args[1])

    @unittest.skip("test_force_v_cmu_4980")
    def test_force_v_cmu_4980(self):
        sweep_cv_range = self.e4980.force_v_cmu_4980('cmh', 30, 0, 0.05)
        print('Range:')
        print(sweep_cv_range)

    @unittest.skip("test_force_v_cmu_4980")
    def test_measure_cmu_4980(self):
        measure_cmu_4980 = self.e4980.measure_cmu_4980(0)
        print('Range:')
        print(measure_cmu_4980)