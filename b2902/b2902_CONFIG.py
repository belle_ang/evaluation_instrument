import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
# LOG_DIR = r"C:/Raytrex/Ray CP/log/" if os.path.exists(r"C:/Raytrex/Ray CP/log/") else os.getcwd() + "//"

# Enable debug mode = True, Disable debug mode = False
TIS_DEBUG_MODE = True

# Enable TIS_driver_MultiControl_mode = True,
# Enable Multi to one = False
TIS_driver_MultiControl_mode = False

# Online = True, Offline = False
# TIS_Driver_OnOffLine = False

# select b2201 compen Triax Cable, select: 1.5、3 or 4, default: 3
Agilent_Triax_Cable = 3

# b1500_module:  input port with module; 將來會做成下拉選單，用選擇的，現在先填數字
# 0: MPSMU, 1: HRSMU, 2: HPSMU, 3: MFCMU, 4: HVSPGU, 5: WGFMU, 6: MCSMU, 7: HCSMU, 8: HVSMU,
# 9: HRSMU and MPSMU with ASU, 10: SCUU
B1500_SLOT1_module = 1
B1500_SLOT2_module = 1
B1500_SLOT3_module = 1
B1500_SLOT4_module = 1
B1500_SLOT5_module = 10
B1500_SLOT6_module = 1
B1500_SLOT7_module = 1
B1500_SLOT8_module = 1
B1500_SLOT9_module = 1
B1500_SLOT10_module = "HPSMU"

# [Configuration of Tester Instruments]
# key is instrument name, value is its address, it can be gpib or lan or usb address, it depends on your needs
# if you define 4 address in 'e4980', that means you have 4 e4980
config = {#"e4980": [],
          "e4980": [],#["gpib0,24", "gpib0,25", "gpib1,26", "gpib1,27"],
          "e5270": [],
          # "e5270": ["gpib0,17", "gpib1,18", "gpib1,19"],
          "b1500": [],#["gpib0,17"],
          # "b1500": ["gpib0,17", "gpib1,18", "gpib1,19"],
          "e5250": [],
          # "e5250": ["gpib0,17", "gpib1,18", "gpib1,19"],
          "b2201": [],
          # "b2201": ["gpib0,22", "gpib1,11"],
          "b2900": ["USBInstrument2"]
          }

# smu name : [smu module, swm input high, swm input Low]
# "B1500_SLOT1": ["HRSMU", 1, 0, "HRSMU", 54088, 8846, "HRSMU", 54088, 8847],
B1500_FRAME_config = {
                    "B1500_SLOT1": ["HRSMU", 1, 0],
                    "B1500_SLOT2": ["HRSMU", 2, 0],
                    "B1500_SLOT3": ["HRSMU", 3, 0],
                    "B1500_SLOT4": ["HRSMU", 4, 0],
                    "B1500_SLOT5": ["HRSMU", 5, 1],
                    "B1500_SLOT6": ["HRSMU", 0, 0],
                    "B1500_SLOT7": ["HRSMU", 0, 0],
                    "B1500_SLOT8": ["HRSMU", 0, 0],
                    "B1500_SLOT9": ["HRSMU", 0, 0],
                    "B1500_SLOT10": ["HRSMU", 0, 0]
                    }

# "E4980_FRAME": [13, 14, 9, 10],# "4980_disable", "4980_disable"]
E4980_FRAME_config = {
                "E4980_FRAME": [13, 14, 9, 10],# "4980_disable", "4980_disable"]
                }

# SWM output Interval output
Output_Interval = 5


TEST_HEAD_COUNT = 2
