from b2902.b2902 import *

SMU1 = OnlineB2902()

IS_ONLINE = True


class TestOnlineB2902:

    def test_online(self):
        idn = SMU1.iread("*IDN?")
        print(idn)

    def test_force_v(self):
        port, output_voltage, output_range, current_compliance = 1, 20, 0, 3

        volt1 = SMU1.iread(f"SOUR{port}:VOLT?")
        range1 = SMU1.iread(f"SOUR{port}:VOLT:RANG:AUTO?")
        comp1 = SMU1.iread(f"SENS{port}:CURR:PROT?")

        SMU1.force_v_2902(port, output_voltage, output_range, current_compliance)

        volt2 = SMU1.iread(f"SOUR{port}:VOLT?")
        range2 = SMU1.iread(f"SOUR{port}:VOLT:RANG:AUTO?")
        comp2 = SMU1.iread(f"SENS{port}:CURR:PROT?")

        print(f"Before : voltage1 :{volt1} , range1:{range1}, comp1:{comp1}")
        print(f"After : voltage2 :{volt2} , range2:{range2}, comp2:{comp2}")

    def test_force_i(self):
        port, output_current, output_range, voltage_compliance = 1, 0.5, 0, 20

        curr1 = SMU1.iread(f"SOUR{port}:CURR?")
        range1 = SMU1.iread(f"SOUR{port}:CURR:RANG:AUTO?")
        comp1 = SMU1.iread(f"SENS{port}:VOLT:PROT?")

        SMU1.force_i_2902(port, output_current, output_range, voltage_compliance)

        curr2 = SMU1.iread(f"SOUR{port}:CURR?")
        range2 = SMU1.iread(f"SOUR{port}:CURR:RANG:AUTO?")
        comp2 = SMU1.iread(f"SENS{port}:VOLT:PROT?")

        print(f"Before : curr1 :{curr1} , range1:{range1}, comp1:{comp1}")
        print(f"After : curr2 :{curr2} , range2:{range2}, comp2:{comp2}")

    def test_measure_i(self):
        # Pre-setting is necessary?
        current, status = SMU1.measure_i_2902(1)
        print(f"current: {current}\nstatus:{status}")

    def test_measure_v(self):
        # Pre-setting is necessary?
        volt, status = SMU1.measure_v_2902(1)
        print(f"volt:{volt}\nstatus:{status}")

    def test_set_smu_ch(self):
        pass

    def test_set_adc(self):
        pass

    def test_set_iv(self):
        pass
