import unittest
from ..b2900 import OnlineB2900
# from ..common import *


class TISDriver2900Test(unittest.TestCase):

    # @unittest.skip("skip")
    def setUp(self):
        self.b2900 = OnlineB2900("usb0[2391::53272::MY51143172::0]")
        # tis_initial(True)
        # self.b2900 = OfflineB2900("gpib2,9")

    @unittest.skip("skip")
    def test_1_init_system(self):
        msg = 'Agilent Technologies,B2902A,MY51140163,1.0.1121.1140'
        self.assertEqual(msg, self.b2900.init_system())

    # @unittest.skip("skip")
    def test_2_force_v(self):
        force_v_1500 = self.b2900.force_v_smu_2900(1, 0, 1, 0, 0.001)
        print(force_v_1500)

    @unittest.skip("test_3_force_i")
    def test_3_force_i(self):
        force_i_1500 = self.b2900.force_i_smu_2900(1, 0, 0.1, 0, 10)
        print(force_i_1500)

    @unittest.skip("test_4_measure_i")
    def test_4_measure_i(self):
        measure_i_1500 = self.b2900.measure_i_smu_2900(1, 0, 0)
        print(measure_i_1500)

    @unittest.skip("test_5_measure_v")
    def test_5_measure_v(self):
        measure_v_1500 = self.b2900.measure_v_smu_2900(1, 0, 0)
        print(measure_v_1500)

    def test_trigger_subsystem_bypass_event_detector(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ALL', channel=0, bypass=True)
        res = self.b2900.trigger_subsystem_settings_query(func="BYP", layer='ALL')
        self.assertEqual({'ACQ': 'ONCE', 'TRAN': 'ONCE'}, res[1]['ARM1'])

        self.b2900.set_trigger_subsystem_settings(layer='ARM', action='ACQ', channel=1, bypass=False)
        res = self.b2900.trigger_subsystem_settings_query(func="BYP", layer='ARM')
        self.assertEqual({'ACQ': 'OFF', 'TRAN': 'ONCE'}, res[1]['ARM1'])

    def test_trigger_subsystem_count_device_action(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ALL', channel=0, count=1)
        res = self.b2900.trigger_subsystem_settings_query(func="COUN", layer='ALL')
        self.assertEqual({'ACQ': '+1', 'TRAN': '+1'}, res[1]['ARM1'])

        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ACQ', channel=1, count=2)
        res = self.b2900.trigger_subsystem_settings_query(func="COUN", layer='TRIG')
        self.assertEqual({'ACQ': '+2', 'TRAN': '+1'}, res[1]['TRIG1'])

    def test_trigger_subsystem_delay_device_action(self):
        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ALL', channel=0, delay=0)
        res = self.b2900.trigger_subsystem_settings_query(func="DEL", layer='ALL')
        self.assertEqual({'ACQ': '+0.00000000E+000', 'TRAN': '+0.00000000E+000'}, res[1]['ARM1'])

        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ACQ', channel=1, delay=0)
        res = self.b2900.trigger_subsystem_settings_query(func="DEL", layer='TRIG')
        self.assertEqual({'ACQ': '+0.00000000E+000', 'TRAN': '+0.00000000E+000'}, res[1]['TRIG1'])

    def test_trigger_subsystem_source_device_action(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ALL', channel=0, sour='AINT')
        res = self.b2900.trigger_subsystem_settings_query(func="SOUR", layer='ALL')
        self.assertEqual({'ACQ': 'AINT', 'TRAN': 'AINT'}, res[1]['ARM1'])

        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ACQ', channel=1, sour='AINT')
        res = self.b2900.trigger_subsystem_settings_query(func="SOUR", layer='TRIG')
        self.assertEqual({'ACQ': 'AINT', 'TRAN': 'AINT'}, res[1]['TRIG1'])

    def test_trigger_subsystem_interval_timer_device_action(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ALL', channel=0, timer=3e-5)
        res = self.b2900.trigger_subsystem_settings_query(func="TIM", layer='ALL')
        self.assertEqual({'ACQ': '+3.00000000E-005', 'TRAN': '+3.00000000E-005'} , res[1]['ARM1'])

        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ACQ', channel=1, timer=3e-5)
        res = self.b2900.trigger_subsystem_settings_query(func="TIM", layer='TRIG')
        self.assertEqual({'ACQ': '+3.00000000E-005', 'TRAN': '+3.00000000E-005'} , res[1]['TRIG1'])

    def test_trigger_subsystem_select_trigger_output(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ALL', channel=0, trg_out="EXT1")
        res = self.b2900.trigger_subsystem_settings_query(func="TOUT:SIGN", layer='ALL')
        self.assertEqual({'ACQ': 'EXT1', 'TRAN': 'EXT1'}, res[1]['ARM1'])

        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ACQ', channel=1, trg_out="EXT2")
        res = self.b2900.trigger_subsystem_settings_query(func="TOUT:SIGN", layer='TRIG')
        self.assertEqual({'ACQ': 'EXT2', 'TRAN': 'EXT1'}, res[1]['TRIG1'])

    def test_trigger_subsystem_enable_trigger_output_mode(self):
        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ALL', channel=0, trg_mode="ON")
        res = self.b2900.trigger_subsystem_settings_query(func="TOUT:STAT", layer='ALL')
        self.assertEqual({'ACQ': '1', 'TRAN': '1'}, res[1]['ARM1'])

        self.b2900.set_trigger_subsystem_settings(layer='ALL', action='ACQ', channel=1, trg_mode="OFF")
        res = self.b2900.trigger_subsystem_settings_query(func="TOUT:STAT", layer='TRIG')
        self.assertEqual({'ACQ': '0', 'TRAN': '1'}, res[1]['TRIG1'])

    def test_trigger_subsystem_device_action_status(self):
        self.b2900.iprintf("*RST")
        res = self.b2900.device_action_status(channel=0)
        self.assertEqual({'ACQ': '1', 'TRAN': '1'}, res[1]['CH1'])

    def test_source_subsystem_set_digital_io_settings(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_digital_io_settings(pin=1, func='TOUT', polarity='POS', timing='BOTH', pul_width=1e-4, trg_type='EDGE')
        res = self.b2900.digital_io_settings_query(pin=1)
        self.assertEqual({'function': 'TOUT',
                          'polarity': 'POS',
                          'timing': 'BOTH',
                          'type': 'EDGE',
                          'width': '+1.00000000E-004'}, res['EXT1'])

    def test_source_subsystem_set_action_layer_trigger_output(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_action_layer_trigger_output(pin=1, stat='ON', action='ALL', channel=0)
        res = self.b2900.action_layer_trigger_output_query()
        self.assertEqual({'sens.output': 'EXT1',
                          'sens.stat': '1',
                          'sour.output': 'EXT1',
                          'sour.stat': '1'}, res[1]['CH1'])

    def test_set_pulse_mode(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_pulse_mode(puls_on=True, mode="CURR", channel=1, puls_width=5e-3, puls_del=4.5e-3)
        res = self.b2900.pulse_mode_query()
        self.assertEqual({'delay': '+4.50000000E-003',
                          'mode': 'CURR',
                          'shape': 'PULS',
                          'width': '+5.00000000E-003'}, res[1]['CH1'])

        self.b2900.set_pulse_mode(puls_on=False, mode="CURR", channel=0, puls_width=5e-3, puls_del=4.5e-3)
        res = self.b2900.pulse_mode_query()
        self.assertEqual({'delay': '+4.50000000E-003',
                         'mode': 'CURR',
                         'shape': 'DC',
                         'width': '+5.00000000E-003'}, res[1]['CH2'])

    def test_set_adc(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_adc(integ_value=0.01)
        res = self.b2900.ipromptf("SENS:VOLT:DC:APER?")
        self.assertEqual(float(res), 0.01)

    def test_set_smu_ch(self):
        self.b2900.iprintf("*RST")
        self.b2900.set_smu_ch(1, 0, 0)
        res = self.b2900.ipromptf(":OUTP1:FILT:LPAS:STAT? ")
        self.assertEqual(int(res), 0)

    @unittest.skip("test_6_set_iv")
    def test_6_set_iv(self):
        set_iv_port = 1
        set_iv_smu_module = 0
        set_iv_sweep_mode = 1
        set_iv_range = 0
        set_iv_start_v = -1
        set_iv_stop_v = 1
        set_iv_step = 21
        set_iv_hold_t = 0
        set_iv_delay_t = 0
        set_iv_compliance = 0.001
        set_iv_power_compliance = 0
        set_iv_stop_mode = 0
        set_iv_2900 = self.b2900.set_iv_2900(set_iv_port, set_iv_smu_module, set_iv_sweep_mode, set_iv_range,
                                             set_iv_start_v, set_iv_stop_v, set_iv_step, set_iv_hold_t,
                                             set_iv_delay_t, set_iv_compliance, set_iv_power_compliance,
                                             set_iv_stop_mode)
        print(set_iv_2900)

    @unittest.skip("test_7_sweep_iv")
    def test_7_sweep_iv(self):
        # sweep_iv_port, sweep_iv_module, sweep_iv_mode, sweep_iv_range
        sweep_iv_port = 1
        sweep_iv_module = 0
        sweep_iv_mode = 1
        sweep_iv_range = 0
        addr, sweep_iv_outputdata, sweep_iv_source, sweep_iv_status = self.b2900.sweep_iv_2900(sweep_iv_port, sweep_iv_module, sweep_iv_mode, sweep_iv_range)
        # r = int(sweep_iv_2900[2]) / int(sweep_iv_2900[1])
        r_list = []
        for source_count in range(len(sweep_iv_source)):
            r_raw = float(sweep_iv_source[source_count]) / float(sweep_iv_outputdata[source_count])
            r_list.append(r_raw)
        print(r_list)
        print(addr)
        print(sweep_iv_outputdata)
        print(sweep_iv_source)
        print(sweep_iv_status)

    @unittest.skip("test_7_sweep_iv")
    def test_8_disable_port_2900_rst(self):

        disable_port_2900_rst = self.b2900.disable_port_2900_rst()
        print(disable_port_2900_rst)

    @unittest.skip("test_9_algo_flow_res_i")
    def test_9_algo_flow_res_i(self):
        raw_data = []
        Vread = []
        Iforce = 0.001
        Vcomp = 100
        Delay_time = 0.1
        disable_port()
        CH1 = fn_port(3, 1, 1)
        force_i(CH1, Iforce, 0, Vcomp)
        wait(Delay_time)
        raw_data = measure_v(CH1, 0)
        for Addr in sorted(raw_data):
            Vread.append(raw_data[Addr][0])
            print(Vread)
        Res = [abs(float(Vread[0]) / float(Iforce))]
        print(Res)

    @unittest.skip("test_10_algo_flow_res_v")
    def test_10_algo_flow_res_v(self):
        Vforce = 2.0
        Irange = 1e-09
        Icomp = 0.01
        Delay_time = 0.1
        raw_data = []
        Iread = []
        disable_port()
        CH1 = fn_port(3, 1, 1)
        force_v(CH1, Vforce, 0, Icomp)
        wait(Delay_time)
        raw_data = measure_i(CH1, Irange)
        for Addr in sorted(raw_data):
            Iread.append(raw_data[Addr][0])
            print(Iread)
        Res = [abs(float(Vforce) / float(Iread[0]))]
        print(Res)

    # @unittest.skip("test_11_algo_flow_res_sweep")
    def test_11_algoflow_res_sweep(self):
        Vstart = -10.0
        Vstop = 10.0
        Step = 21
        Irange = 1e-09
        Icomp = 0.01
        Delay_time = 0.0
        Hold_time = 0.0
        raw_data = []
        Iread = []
        Vread = []
        CH1 = fn_port(3, 1, 1)
        set_iv(CH1, 1, 0, Vstart, Vstop, Step, Hold_time, Delay_time, Icomp, 0, 0)
        wait(Delay_time)
        raw_data = sweep_iv(CH1, 2, Irange)
        for IV in sorted(raw_data):
            # Iread = list(raw_data[IV][1].values())
            # Vread = list(raw_data[IV][0].values())
            Iread = list(raw_data[IV][0])
            Vread = list(raw_data[IV][1])
        # for x,y in zip(Vread,Iread) :
        #    Res.append(float(x)/(float(y)+1E-30))
        # Res = [abs(float(x) / (float(y) + 1E-30)) for x, y in zip(Vread, Iread)]
        Res = [abs(float(x) / float(y)) for x, y in zip(Vread, Iread)]
        disable_port()
        disconnect_all()

