from abc import ABCMeta, abstractmethod
import logging
from functools import wraps


class TISError(Exception):
    pass


class BaseDriver(metaclass=ABCMeta):
    """ E4980 driver class, define all tis method of E4980"""
    logger = logging.getLogger('tis_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    # fh = logging.FileHandler("{}\{}".format(LOG_DIR if os.path.exists(LOG_DIR) else str(Path.home()), 'tislog'))
    # fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s')
    # fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    # logger.addHandler(fh)
    # logger.addHandler(ch)

    @abstractmethod
    def iprintf(self):
        pass

    @abstractmethod
    def ipromptf(self):
        pass

    @abstractmethod
    def log_level(self, level):
        pass


def _error_decorator(func):
    """
    Check error code after sending command
    :param func:
    :return:
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        self = args[0]  # args[0] is object itself
        func(*args, **kwargs)
        self.check_error_code()

        return self.INST_ADDRESS, func.__name__
    return wrapper