def Spectral_Measure(current, min_w, max_w, comp, T_aper):
    spd = []
    peak_w = []
    fwhm = []
    center_w = []
### DON'T EDIT CODE ABOVE THIS LINE
    # initial spectrometer
    avs_dev = AVS_Initial()
    
    # setting trigger mode configuration to b2900
    set_action_tgr(fn_port(3, 1))
    set_digital_io(fn_port(3, 1))  # generate negtive trigger pulse

    # setting pulse mode configuration to b2900
    set_pulse_mode(fn_port(3, 1), peak=current, puls_width=T_aper, comp=comp)

    # setting hardware trigger mode to spectrometer
    ret = AVS_PrepareMeasure(avs_dev, set_MeasConfig(m_StopPixel=globals.avs_pixels - 1, m_Trigger_m_Mode=1,
                                                             m_SaturationDetection=1, m_IntegrationTime=T_aper,
                                                             m_CorDynDark_m_Enable=1))

    # setting measurement condition of spectrometer
    ret = AVS_Measure(avs_dev, None, 1)

    # initiate trigger
    init_trigger(channel=1)

    # check if collect correct wavelength data
    AVS_PollScan(avs_dev)

    timestamp, globals.avs_spectraldata = AVS_GetScopeData(avs_dev)

    result = SpectralDataAnalysis(globals.avs_spectraldata, globals.avs_wavelength, min_w=min_w, max_w=max_w, threshold=1000)
    spd = result.spd()
    fwhm = result.fwhm()
    center_w = result.center_wavelength()
    peak_w = result.peak_wavelength()
    result.plot()
    disable_port()

### DON'T EDIT CODE BELOW THIS LINE
    return spd, peak_w, fwhm, center_w

def LIV_Linear_Sweep(i_start, i_stop, step, delay_t, aper_t, v_comp, v_range, i_range, output_range_i, i_comp, output_limited_range1, meas_limited_range1, meas_limited_range2, I_rate2, I_rate, I_ro):
    I_LD = []
    I_LD_REAL = []
    I_PD = []
    V_LD = []
### DON'T EDIT CODE ABOVE THIS LINE
    CH1 = fn_port(port_type=3, port_slot=1)
    CH2 = fn_port(port_type=3, port_slot=2)
    
    set_smu_ch(CH1, filter=0)
    set_smu_ch(CH2, filter=0)
    set_adc(integ_value=aper_t)
    set_iv(CH1, sweep_mode=2, output_range=output_range_i, iv_start=i_start, iv_stop=i_stop, iv_step=step, delay_t=delay_t, compliance=v_comp, output_limited_range=output_limited_range1)
    set_iv(CH2, sweep_mode=1, iv_step=step, delay_t=delay_t, compliance=i_comp)
    raw_data = sweep_iv2(port1=CH1, meas_mode1=1, meas_range1=v_range, port2=CH2, meas_mode2=2, meas_range2=i_range, meas_limited_range1=meas_limited_range1, meas_limited_range2=meas_limited_range2)
    for ch in sorted(raw_data):
        I_LD = list(raw_data[ch][2])
        V_LD = list(raw_data[ch][0])
        I_LD_REAL = list(raw_data[ch][4])
        
        I_PD = list([abs(i) for i in raw_data[ch][1]])


    liv = LIVDataAnalysis(I_LD=I_LD_REAL, I_PD=I_PD, V_LD=V_LD, responsivity=[1 for _ in range(len(V_LD))], I_rate2=I_rate2, I_rate=I_rate, I_ro=I_ro, I_LDmax=i_stop)
    liv.plot(xlim=[0,i_stop],ylim1=[0,v_comp],ylim2=[0,i_comp])
    disable_port()
    
### DON'T EDIT CODE BELOW THIS LINE
    return I_LD, I_LD_REAL, I_PD, V_LD

def LIV_Pulse_Sweep(i_start, i_stop, step, delay_t, aper_t, v_comp, v_range, i_range, output_range_i, pulse_width, period, i_comp, output_limited_range1, meas_limited_range1, meas_limited_range2, I_rate2, I_rate, I_ro):
    I_LD = []
    I_LD_REAL = []
    I_PD = []
    V_LD = []
### DON'T EDIT CODE ABOVE THIS LINE
    CH1 = fn_port(port_type=3, port_slot=1)
    CH2 = fn_port(port_type=3, port_slot=2)
    set_smu_ch(CH1, filter=0)
    set_smu_ch(CH2, filter=0)
    set_adc(integ_value=aper_t)
    set_piv(CH1, puls_sweep_mode=2, output_range=output_range_i, puls_start=i_start, puls_stop=i_stop, num_step=step, puls_width=pulse_width, puls_per=period, delay_t=delay_t, compliance=v_comp, stop_mode=1, output_limited_range=output_limited_range1)
    set_iv(CH2, sweep_mode=1, iv_step=step, delay_t=delay_t, compliance=i_comp)
    raw_data = sweep_iv2(port1=CH1, meas_mode1=1, meas_range1=v_range, port2=CH2, meas_mode2=2, meas_range2=i_range, meas_limited_range1=meas_limited_range1, meas_limited_range2=meas_limited_range2)
    for ch in sorted(raw_data):
        I_LD = list(raw_data[ch][2])
        V_LD = list(raw_data[ch][0])
        I_LD_REAL = list(raw_data[ch][4])
        
        I_PD = list([abs(i) for i in raw_data[ch][1]])

    liv = LIVDataAnalysis(I_LD=I_LD_REAL, I_PD=I_PD, V_LD=V_LD, responsivity=[1 for _ in range(len(V_LD))], I_rate2=I_rate2, I_rate=I_rate, I_ro=I_ro, I_LDmax=i_stop)
    liv.plot(xlim=[0,i_stop],ylim1=[0,v_comp],ylim2=[0,i_comp])
    disable_port()
    
### DON'T EDIT CODE BELOW THIS LINE
    return I_LD, I_LD_REAL, I_PD, V_LD

def LIV_Pulse_Data_Analysis(i_start, i_stop, step, delay_t, aper_t, v_comp, v_range, i_range, output_range_i, pulse_width, period, kink_factor, i_comp, output_limited_range1, meas_limited_range1, meas_limited_range2, I_rate2, I_rate, I_ro):
    I_LD = []
    I_LD_REAL = []
    I_PD = []
    V_LD = []
    optical_power = []
    slope_eff = []
    ser_res = []
    power_conv = []
    threshold_curr = []
    rollover = []
    linearity = []
    kink_count = []
### DON'T EDIT CODE ABOVE THIS LINE
    CH1 = fn_port(port_type=3, port_slot=1)
    CH2 = fn_port(port_type=3, port_slot=2)
    set_smu_ch(CH1, filter=0)
    set_smu_ch(CH2, filter=0)
    set_adc(integ_value=aper_t)
    set_piv(CH1, puls_sweep_mode=2, output_range=output_range_i, puls_start=i_start, puls_stop=i_stop, num_step=step, puls_width=pulse_width, puls_per=period, delay_t=delay_t, compliance=v_comp, stop_mode=1, output_limited_range=output_limited_range1)
    set_iv(CH2, sweep_mode=1, iv_step=step, delay_t=delay_t, compliance=i_comp)
    raw_data = sweep_iv2(port1=CH1, meas_mode1=1, meas_range1=v_range, port2=CH2, meas_mode2=2, meas_range2=i_range, meas_limited_range1=meas_limited_range1, meas_limited_range2=meas_limited_range2)
    for ch in sorted(raw_data):
        I_LD = list(raw_data[ch][2])
        V_LD = list(raw_data[ch][0])
        I_LD_REAL = list(raw_data[ch][4])
        
        I_PD = list([abs(i) for i in raw_data[ch][1]])

    liv = LIVDataAnalysis(I_LD=I_LD_REAL, I_PD=I_PD, V_LD=V_LD, responsivity=[1 for _ in range(len(V_LD))], I_rate2=I_rate2, I_rate=I_rate, I_ro=I_ro, I_LDmax=i_stop)
    optical_power = liv.optical_power()
    slope_eff = liv.slope()
    ser_res = liv.ser_res()
    power_conv = liv.power_conv()
    threshold_curr = liv.threshold_curr()
    rollover = liv.rollover()
    linearity = liv.linearity()
    kink_count = liv.kink_count(kink_factor)
    liv.plot(xlim=[0,i_stop],ylim1=[0,v_comp],ylim2=[0,i_comp])
    disable_port()
    
### DON'T EDIT CODE BELOW THIS LINE
    return I_LD, I_LD_REAL, I_PD, V_LD, optical_power, slope_eff, ser_res, power_conv, threshold_curr, rollover, linearity, kink_count

def LIV_Reverse_Current(Vleak, Ileak_cmp, Ileak_rg):
    Ileak = []
### DON'T EDIT CODE ABOVE THIS LINE
    CH1 = fn_port(port_type=3, port_slot=1)
    force_v(CH1, Vleak, compliance=Ileak_cmp)
    res = measure_i(CH1, Ileak_rg)
    for ch in res:
        Ileak.append(res[ch][0])
    disable_port()
### DON'T EDIT CODE BELOW THIS LINE
    return Ileak

def LIV_Linear_Data_Analysis(i_start, i_stop, step, delay_t, aper_t, v_comp, v_range, i_range, output_range_i, kink_factor, i_comp, output_limited_range1, meas_limited_range1, meas_limited_range2, I_rate2, I_rate, I_ro):
    I_LD = []
    I_LD_REAL = []
    I_PD = []
    V_LD = []
    optical_power = []
    slope_eff = []
    ser_res = []
    power_conv = []
    threshold_curr = []
    rollover = []
    linearity = []
    kink_count = []
### DON'T EDIT CODE ABOVE THIS LINE
    CH1 = fn_port(port_type=3, port_slot=1)
    CH2 = fn_port(port_type=3, port_slot=2)
    
    set_smu_ch(CH1, filter=0)
    set_smu_ch(CH2, filter=0)
    set_adc(integ_value=aper_t)
    set_iv(CH1, sweep_mode=2, output_range=output_range_i, iv_start=i_start, iv_stop=i_stop, iv_step=step, delay_t=delay_t, compliance=v_comp,output_limited_range=output_limited_range1)
    set_iv(CH2, sweep_mode=1, iv_step=step, delay_t=delay_t, compliance=i_comp)
    raw_data = sweep_iv2(port1=CH1, meas_mode1=1, meas_range1=v_range, port2=CH2, meas_mode2=2, meas_range2=i_range, meas_limited_range1=meas_limited_range1, meas_limited_range2=meas_limited_range2)
    for ch in sorted(raw_data):
        I_LD = list(raw_data[ch][2])
        V_LD = list(raw_data[ch][0])
        I_LD_REAL = list(raw_data[ch][4])
        
        I_PD = list([abs(i) for i in raw_data[ch][1]])


    liv = LIVDataAnalysis(I_LD=I_LD_REAL, I_PD=I_PD, V_LD=V_LD, responsivity=[1 for _ in range(len(V_LD))], I_rate2=I_rate2, I_rate=I_rate, I_ro=I_ro, I_LDmax=i_stop)
    optical_power = liv.optical_power()
    slope_eff = liv.slope()
    ser_res = liv.ser_res()
    power_conv = liv.power_conv()
    threshold_curr = liv.threshold_curr()
    rollover = liv.rollover()
    linearity = liv.linearity()
    kink_count = liv.kink_count(kink_factor)
    liv.plot(xlim=[0,i_stop],ylim1=[0,v_comp],ylim2=[0,i_comp])
    disable_port()
    
### DON'T EDIT CODE BELOW THIS LINE
    return I_LD, I_LD_REAL, I_PD, V_LD, optical_power, slope_eff, ser_res, power_conv, threshold_curr, rollover, linearity, kink_count

