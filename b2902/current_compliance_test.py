import pyvisa


class OnlineB2902:
    def __init__(self):
        self.inst: pyvisa.resources.MessageBasedResource = pyvisa.ResourceManager().open_resource("GPIB0::4::INSTR")


    def iwrite(self, cmd):
        self.inst.write(cmd)
        print (cmd)
        print (self.iread("SYST:ERR?"))

    def iread(self, cmd):
        self.inst.query(cmd)

        
    def sweep_iv(self, port, measurement_mode, measurement_range, limited_range):
        """

        :param port:
        :param measurement_mode: 1:voltage measurement ; 2:current measurement
        :param measurement_range:
        :param limited_range:
        :return:
        """
        """
        This Subprogram conduct perform staircase sweep measurement.
        self.__check_sweep_mode:                  check and convert iv mode.
        self.__check_current_range:                    check current range.
        self.__check_sweep_mode:                  check voltage range.
                +----------------------------------------+
        '       | VOLT | CURR | RES | TIME | STAT | SOUR |
        '       +----------------------------------------+
        'Index      0      1     2      3      4     5
        :param sweep_iv_port: channel (SLOT1, SLOT2, ...SLOT10)
        :param sweep_iv_module: input port with module. 0: SMU
        :param sweep_iv_mode: 1: V mode; 2: I mode
        :param sweep_iv_range: Range
        :param limited_range: 0/1/2, 0: Not use limited range, 1: use lower limited range, 2: use upper limited range
        :return: self.INST_ADDRESS, sweep_iv_outputdata, sweep_iv_source, sweep_iv_status
        """

        self.iwrite(":FORM:DATA ASC")
        self.iwrite(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")

        measurement_dict = {1: "VOLT", 2: "CURR"}

        mode_str = measurement_dict[measurement_mode]
        self.iwrite(f":SENS{port}:FUNC \"{mode_str}\"")
        pattern = ":SENS{port}:" + mode_str

        if measurement_range is 0:
            self.iwrite(f"{pattern}:RANG:AUTO ON")
        else:
            if limited_range == 0:
                self.iwrite(f"{pattern}:RANG:AUTO OFF")
                self.iwrite(f"{pattern}:RANG {measurement_range}")
            elif limited_range == 1:
                self.iwrite(f"{pattern}:RANG:AUTO:LLIM {measurement_range}")
            elif limited_range == 2:
                self.iwrite(f"{pattern}:RANG:AUTO:ULIM {measurement_range}")
            else:
                print ("illegal limited range")

        # Enabling the Source Output
        self.iwrite(f":OUTP{port} ON")
        # Initiate transition and acquire
        self.iwrite(f":INIT (@{port})")
        return_val_array = []
        return_val_array.append(self.iread(f":FETC:ARR:VOLT? (@{port})"))
        return_val_array.append(self.iread(f":FETC:ARR:CURR? (@{port})"))
        return_val_array.append(self.iread(f":FETC:ARR:RES? (@{port})"))
        return_val_array.append(self.iread(f":FETC:ARR:TIME? (@{port})"))
        return_val_array.append(self.iread(f":FETC:ARR:STAT? (@{port})"))
        return_val_array.append(self.iread(f":FETC:ARR:SOUR? (@{port})"))

        # measure_point = self.iread(f":SYST:DATA:QUAN? (@{port})")

        print (return_val_array)
        # sweep_iv_outputdata = return_val_array[1]
        # sweep_iv_source = return_val_array[5].split(",")
        # if measurement_mode == 1:
        #     sweep_iv_outputdata = return_val_array[1].split(",")
        #     sweep_iv_source_real = return_val_array[0].split(",")
        #
        # if measurement_mode == 2:
        #     sweep_iv_outputdata = return_val_array[0].split(",")
        #     sweep_iv_source_real = return_val_array[1].split(",")
        #
        # # check SMU error code
        # self.__check_error()
        # self.__check_error_n_debug()
        #
        # sweep_iv_status = "sweep_iv_2902 => PASS"
        #
        # return self.INST_ADDRESS, sweep_iv_outputdata, sweep_iv_source, sweep_iv_source_real, sweep_iv_status

    def guess2(self, ch_, output_voltage_ = 2, current_compliance_ = 0.4):
        """
        R1 : 0 <= v <= 6
        R2 : 6 < v <= 21
        R3 : 21 < v <= 210
        -----------------------------------------------------
        |  v1  |  v2  |   i1             |    i2            |
        -----------------------------------------------------
         |  R1  |  R1  |  4 - i2         |  4 - i1          |
         |      |  R2  |  4 - 1.6*i2     |  (4 - i1)/1.6    |
         |  R2  |  R1  | 2.5 - 0.625*i2  |  (2.5-i1)/0.625  |
         |      |  R2  | 2.5 - i2        |  2.5 - i1        |
         ----------------------------------------------------
         * the less, return min(current_compliance_, MIN limit : R1: 3.03, R2: 1.515, R3:0.105)
        :param ch_:
        :param output_voltage_:
        :param current_compliance_:
        :return:
        """
        # ch1 ; ch2 is ON or OFF
        if ch_ is 1:  # if setting ch1
            ch2_status = int(self.inst.query("OUTP2:STAT?"))

            if ch2_status is 0:  # ch2 is OFF
                if 0 <= abs(output_voltage_) <= 6:
                    current_compliance_ = min(current_compliance_, 0.303)
                elif 6 < abs(output_voltage_) <= 21:
                    current_compliance_ = min(current_compliance_, 1.515)
                else:
                    current_compliance_ = min(current_compliance_, 0.105)

            else:  # ch2 is ON
                volt_2, limit_2 = int(self.inst.query("SOUR2:VOLT?")), float(self.inst.query("SENS2:CURR:PROT?"))
                if 0 <= abs(volt_2) <= 6:
                    if 0 <= abs(output_voltage_) <= 6:  # v2 in R1, v1 in R1
                        current_compliance_ = 4 - limit_2
                    elif 6 < abs(output_voltage_) <= 21:  # v2 in R1 , v1 in R2
                        current_compliance_ = 2.5 - limit_2 * 0.625
                elif 6 < abs(output_voltage_) <= 21:
                    if 0 <= abs(output_voltage_) <= 6:  # v2 in R2, v1 in R1
                        current_compliance_ = 4 - limit_2 * 1.6
                    elif 6 < abs(output_voltage_) <= 21:  # v2 in R2 , v1 in R2
                        current_compliance_ = 2.5 - limit_2
                else:
                    current_compliance_ = (min(current_compliance_, 0.105))
        # ch2 :
        else:
            ch1_status = int(self.inst.query("OUTP1:STAT?"))

            if ch1_status is 0:  # ch1 is OFF
                if 0 <= abs(output_voltage_) <= 6:
                    current_compliance_ = min(current_compliance_, 0.303)
                elif 6 < abs(output_voltage_) <= 21:
                    current_compliance_ = min(current_compliance_, 1.515)
                else:
                    current_compliance_ = min(current_compliance_, 0.105)

            else:  # ch1 is ON
                volt_1, limit_1 = int(self.inst.query("SOUR1:VOLT?")), float(self.inst.query("SENS1:CURR:PROT?"))

                if 0 <= abs(volt_1) <= 6:  # v1 in R1
                    if 0 <= abs(output_voltage_) <= 6:  # v1 in R1, v2 in R1
                        current_compliance_ = 4 - limit_1
                    elif 6 < abs(output_voltage_) <= 21:  # v1 in R1 , v2 in R2
                        current_compliance_ = (4 - limit_1) / 1.6

                elif 6 < abs(output_voltage_) <= 21:
                    if 0 <= abs(output_voltage_) <= 6:  # v1 in R2, v1 in R1
                        current_compliance_ = (2.5 - limit_1) / 0.625
                    elif 6 < abs(output_voltage_) <= 21:  # v1 in R2 ,v2 in R2
                        current_compliance_ = 2.5 - limit_1
                else:
                    current_compliance_ = (min(current_compliance_, 0.105))
        return current_compliance_


OnlineB2902()
