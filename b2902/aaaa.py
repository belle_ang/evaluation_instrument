import time

"""
1. argument 不一樣怎麼
2. wrap 裡要return的值怎麼傳出來
3. Finally, 要回傳status, 需加try...else, 
"""


def timetrack(func):
    def wrap():
        time_start = time.perf_counter()
        func()
        return time.perf_counter() - time_start

    return wrap


@timetrack
def one(second = 0.55):
    time.sleep(1.4 + second)
    print(f"arugment:{second}")
