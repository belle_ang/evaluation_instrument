import sys
import time
import pyvisa

timetrack = []


class OnlineB2902:
    def __init__(self):
        self.inst: pyvisa.resources.MessageBasedResource = pyvisa.ResourceManager().open_resource("GPIB0::4::INSTR")
        # print(self.iread("*IDN?"))

    def iwrite(self, cmd):
        time_start = time.perf_counter()
        self.inst.write(cmd)
        timetrack.append([cmd, time.perf_counter() - time_start])

    def iread(self, cmd):
        time_start = time.perf_counter()
        read_message = self.inst.query(cmd)
        timetrack.append([cmd, time.perf_counter() - time_start])
        return read_message

    def iread_bulk(self, cmd):
        time_start = time.perf_counter()
        read_message = self.inst.query_ascii_values(cmd)
        timetrack.append([cmd, time.perf_counter() - time_start])
        return read_message

    def check_error(self):
        """"
            This Subprogram check ERROR code. for B2902, Send "ERRX?" can clear B2902 error buffer.
        """
        msg_list = []
        while True:
            error_message = self.iread(":SYST:ERR?")
            print(error_message)
            error_message_output = error_message.rstrip().split(",")
            if error_message_output[0] != "+0":
                msg_list.append(f"{error_message_output[0]}, {error_message_output[1]}")
            else:
                break
        if len(msg_list) > 0:
            sys.exit("*** ERROR : B2902 Instrument Error : {}".format(";".join(msg_list)))

    def disable_port_2902(self):
        """
        目前在初始化也有做消除ERROR Buffer的動作，可防止上一次的ERROR MESSAGE沒有消除，被判定成錯誤而跳出程式
        因為Debug，所以先不在Disable的時候刪除，穩定後可以考慮使用
        """
        syst_err = self.iread(":SYST:ERR:ALL?")
        self.iwrite("*RST")

    def __check_current_compliance(self, ch_, output_voltage_, current_compliance_):
        """
        Table 2-5  @ Keysight B2900 SCPI cmd Reference
        R1 : 0 <= v <= 6
        R2 : 6 < v <= 21
        R3 : 21 < v <= 210
        +-------+------+-----------------+------------------+
        |  v1   |  v2   |  i1             |    i2            |
        +-------+------+------------------+------------------+
        |   R1  |  R1  |  4 - i2         |  4 - i1          |
         |      |  R2  |  4 - 1.6*i2     |  (4 - i1)/1.6    |
         |  R2  |  R1  | 2.5 - 0.625*i2  |  (2.5-i1)/0.625  |
         |      |  R2  | 2.5 - i2        |  2.5 - i1        |
         -------+------+-----------------+------------------+
         * the less, return min(current_compliance_, MIN limit : R1: 3.03, R2: 1.515, R3:0.105)
        :param ch_:
        :param output_voltage_:
        :param current_compliance_:
        :return: return the max of current after checking
        """
        # ch1 ; ch2 is ON or OFF
        if ch_ is 1:  # if setting ch1
            if int(self.inst.query("OUTP2:STAT?")) is 0:  # ch2 is OFF
                if 0 <= abs(output_voltage_) <= 6:
                    current_compliance_ = min(current_compliance_, 3.03)
                elif 6 < abs(output_voltage_) <= 21:
                    current_compliance_ = min(current_compliance_, 1.515)
                else:
                    current_compliance_ = min(current_compliance_, 0.105)
            else:  # ch2 is ON
                volt_2, limit_2 = float(self.inst.query("SOUR2:VOLT?")), float(self.inst.query("SENS2:CURR:PROT?"))
                if 0 <= abs(volt_2) <= 6:
                    if 0 <= abs(output_voltage_) <= 6:  # v2 in R1, v1 in R1
                        current_compliance_ = 4 - limit_2
                    elif 6 < abs(output_voltage_) <= 21:  # v2 in R1 , v1 in R2
                        current_compliance_ = 2.5 - limit_2 * 0.625
                elif 6 < abs(output_voltage_) <= 21:
                    if 0 <= abs(output_voltage_) <= 6:  # v2 in R2, v1 in R1
                        current_compliance_ = 4 - limit_2 * 1.6
                    elif 6 < abs(output_voltage_) <= 21:  # v2 in R2 , v1 in R2
                        current_compliance_ = 2.5 - limit_2
                else:
                    current_compliance_ = (min(current_compliance_, 0.105))
        else:  # setting port is channel 2
            if int(self.inst.query("OUTP1:STAT?")) is 0:  # ch1 is OFF
                if 0 <= abs(output_voltage_) <= 6:
                    current_compliance_ = min(current_compliance_, 3.03)
                elif 6 < abs(output_voltage_) <= 21:
                    current_compliance_ = min(current_compliance_, 1.515)
                else:
                    current_compliance_ = min(current_compliance_, 0.105)
            else:  # ch1 is ON
                volt_1, limit_1 = float(self.inst.query("SOUR1:VOLT?")), float(self.inst.query("SENS1:CURR:PROT?"))
                if 0 <= abs(volt_1) <= 6:  # v1 in R1
                    if 0 <= abs(output_voltage_) <= 6:  # v1 in R1, v2 in R1
                        current_compliance_ = 4 - limit_1
                    elif 6 < abs(output_voltage_) <= 21:  # v1 in R1 , v2 in R2
                        current_compliance_ = (4 - limit_1) / 1.6
                elif 6 < abs(output_voltage_) <= 21:
                    if 0 <= abs(output_voltage_) <= 6:  # v1 in R2, v1 in R1
                        current_compliance_ = (2.5 - limit_1) / 0.625
                    elif 6 < abs(output_voltage_) <= 21:  # v1 in R2 ,v2 in R2
                        current_compliance_ = 2.5 - limit_1
                else:
                    current_compliance_ = (min(current_compliance_, 0.105))
        return current_compliance_

    def force_v_2902(self, port, output_voltage, output_range=0, current_compliance=1e-4):

        current_compliance = self.__check_current_compliance(port, output_voltage, current_compliance)
        output_range = self.__check_voltage_range(output_range)

        self.iwrite(f"SOUR{port}:FUNC:MODE VOLT")
        if output_range is 0:
            self.iwrite(f"SOUR{port}:VOLT:RANG:AUTO ON")
        else:
            self.iwrite(f"SOUR{port}:VOLT:RANG:AUTO {output_range}")
        self.iwrite(f"SENS{port}:CURR:PROT {current_compliance}")
        self.iwrite(f"SOUR{port}:VOLT {output_voltage}")
        self.iwrite(f":OUTP{port} ON")

    def __check_voltage_compliance(self, output_current_, voltage_compliance):
        # b2900 curr->volt, 0.105->210, 1.515-> 21, 3.03->6, return voltage_compliance
        # voltage compliance return the max if only the input is exceed the limit
        if output_current_ <= 0.105:
            return min(voltage_compliance, 210)
        elif output_current_ <= 1.515:
            return min(voltage_compliance, 21)
        elif output_current_ <= 3.03:
            return min(voltage_compliance, 6)
        else:
            sys.exit("Error :output current out of range")

    def force_i_2902(self, port, output_current, output_range=0, voltage_compliance=20):
        output_range = self.__check_current_range(output_range)
        voltage_compliance = self.__check_voltage_compliance(output_current, voltage_compliance)

        self.iwrite(f":SOUR{port}:FUNC:MODE CURR")
        if output_range == 0:
            self.iwrite(f":SOUR{port}:CURR:RANG:AUTO ON")
        else:
            self.iwrite(f":SOUR{port}:CURR:RANG {output_range}")
        self.iwrite(f":SENS{port}:VOLT:PROT {voltage_compliance}")
        self.iwrite(f":SOUR{port}:CURR {output_current}")
        self.iwrite(f":OUTP{port} ON")

    def measure_i_2902(self, port, measurement_range=0):
        # 4770 Measure_i (port address, current value name, measurement_range, time stamp)

        current_range = self.__check_current_range(measurement_range)
        self.iwrite(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR") # todo only current?
        self.iwrite(":FORM:DATA ASC")
        self.iwrite(f':SENS{port}:FUNC "CURR"')

        if current_range is 0:
            self.iwrite(f":SENS{port}:CURR:RANG:AUTO ON")
        else:
            self.iwrite(f":SENS{port}:CURR:RANG:AUTO OFF")
            self.iwrite(f":SENS{port}:CURR:RANG {measurement_range}")


        # self.iwrite(":FORM:ELEM:SENS CURR")
        read_results = self.iread(f":MEAS? (@{port})")
        measured_v, measured_i, resistance, timestamp, status_, sour = read_results.split(",")
        # todo : only return current
        return float(measured_i), float(status_)

    def measure_v_2902(self, port, measurement_range=0):
        # 4770 Measure_v(port, voltage value name, measurement_range, time_stamp
        measurement_range = self.__check_voltage_range(measurement_range)
        self.iwrite(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")　# todo : time?
        self.iwrite(":FORM:DATA ASC")
        self.iwrite(f':SENS{port}:FUNC "VOLT"')
        if measurement_range == 0:
            self.iwrite(f":SENS{port}:VOLT:RANG:AUTO ON")
        else:
            self.iwrite(f":SENS{port}:VOLT:RANG:AUTO OFF")
            self.iwrite(f":SENS{port}:VOLT:RANG {measurement_range}")

        _, measured_voltage, _, _, status_, s = self.iread(f":MEAS? (@{port})").split(",")
        measured_voltage = "%e" % float(measured_voltage)

        # self.check_error() # todo : 要找出可能出error的原因, 再看能不能合併
        # self.check_error_n_debug()

        return float(measured_voltage), float(status_)

    def set_smu_ch(self, port, a2d_converter=0, filter_mode=0):
        """
        This Subprogram conduct ADC converter to using for SMU and Filter on/off.
        But 2902 only one type of ADC, thus don't need to select adc mode for SMU.

        :param A/D converter:  0:High speed; 1:High Resolution
        :param filter_mode: Filter switch; 0:Off; 1:ON
        """
        self.iwrite(f":OUTP{port}:FILT:LPAS:STAT {filter_mode}")

    def set_adc(self, integ_mode, integ_value=None, auto_zero=1):
        """ The subprogram sets the operation parameters of analog-to-digital converter (high-speed A/D converter or
            high-resolution A/D converter) that SMUs use for voltage and current measurements.

            Because there is no different type of adc in b2902, argument:adc, integ_mode, auto_zero are useless.
        :param integ_value: integration time, +8E-6 to +2 seconds
        """
        if integ_value is None:
            integration_value = "AUTO ON"
        else:
            integration_value = integ_value
        self.iwrite(f":SENS1:VOLT:DC:APER {integration_value}")
        self.iwrite(f":SENS1:CURR:DC:APER {integration_value}")
        self.iwrite(f":SENS2:VOLT:DC:APER {integration_value}")
        self.iwrite(f":SENS2:CURR:DC:APER {integration_value}")

    def __check_sweep_mode(self, sweep_mode_):

        sweepmode_dict = {
            1: ["VOLT", "LIN", "SING", "CURR"],
            2: ["CURR", "LIN", "SING", "VOLT"],
            3: ["VOLT", "LIN", "DOUB", "CURR"],
            4: ["CURR", "LIN", "DOUB", "VOLT"],
            -1: ["VOLT", "LOG", "SING", "CURR"],
            -2: ["CURR", "LOG", "SING", "VOLT"],
            -3: ["VOLT", "LOG", "DOUB", "CURR"],
            -4: ["CURR", "LOG", "DOUB", "VOLT"], }

        try:
            return sweepmode_dict[sweep_mode_]
        except IndexError:
            sys.exit(f"*** ERROR : Sweep mode selected wrongly.Using +-1、+-2、+-3、+-4.{sweep_mode_}")

    def set_iv_2902(self, port, sweep_mode, output_range, start_val, stop_val, step_number, hold_time=0,
                    delay_time=0, compliance=1E-4, power_compliance=0, stop_mode=1):
        """
        This Subprogram conduct setup parameters for staircase sweep measurement.
        :param port:
        :param sweep_mode: odd: current sweep , even: voltage sweep, positive: linear, negative: log
        :param output_range: if current mode : voltage range; voltage mode, current mode
        :param compliance: ***
        :param power_compliance: *** 0, 0.001 to 2, resolution : 0.001
        :param stop_mode: 1: continues if reaches compliance, 2:abort and return 99999 ( power_comp set to 0)
        :return:
        """

        sweep_source, sweep_mode, sweep_direction, _ = self.__check_sweep_mode(sweep_mode)
        if sweep_source is 'volt':
            compliance = self.__check_current_compliance(port, stop_val, compliance)
            output_range = self.__check_voltage_range(output_range)
        else:
            compliance = self.__check_voltage_compliance(stop_val, compliance)
            output_range = self.__check_current_range(output_range)

        self.__check_setiv_other(step_number, hold_time, delay_time, power_compliance, stop_mode)

        self.iwrite(f":ARM{port}:TRAN:DEL 0")
        self.iwrite(f":ARM{hold_time}:ACQ:DEL ")
        self.iwrite(f":TRIG{port}:TRAN:DEL 0")
        self.iwrite(f":TRIG{port}:ACQ:DEL {delay_time}")
        pattern = f":SOUR{port}:{sweep_source}"

        # set iv
        self.iwrite(f":SOUR{port}:FUNC:MODE {sweep_source}")
        self.iwrite(f"{pattern}:MODE SWE")
        self.iwrite(f":SOUR{port}:SWE:SPAC {sweep_mode}")
        self.iwrite(f":SOUR{port}:SWE:STA {sweep_direction}")

        if output_range is 0:
            self.iwrite(f"{pattern}:RANG:AUTO ON")
        else:
            if compliance is 0:
                self.iwrite(f"{pattern}:RANG {output_range}")
            else:
                self.iwrite(f"{pattern}:RANG:AUTO:LLIM {output_range}")

        self.iwrite(f"{pattern}:STAR {start_val}")
        self.iwrite(f"{pattern}:STOP {stop_val}")
        self.iwrite(f"{pattern}:POIN {step_number}")
        self.iwrite(f":SENS{port}:{sweep_source}:PROT {output_range}")
        self.iwrite(f":TRIG{port}:ALL:SOUR AINT")
        self.iwrite(f":TRIG{port}:ALL:COUN {step_number}")

    def set_piv(self, port, sweep_mode, output_range, pulse_base, start_val, stop_val, step_number,
                pulse_width=0.005, pulse_period=0.2, hold_time=0, compliance=0, stop_mode=1):
        """

        :param port:
        :param sweep_mode:
        :param range:
        :param pulse_base:
        :param start_val:
        :param stop_val:
        :param step_number:
        :param pulse_width:
        :param pulse_period:
        :param hold_time:
        :param compliance:
        :param stop_mode:
        1 : pulsed voltage or current is swept to the last specified step, even if an SMU reaches voltage or current compliance
        2 :the pulsed voltage or current sweep aborts and returns dummy data (9999999.99999) if an SMU reaches voltage or current compliance.
        :return:
        """

        sweep_source, sweep_mode, sweep_direction, sweep_sense = self.__check_sweep_mode(sweep_mode)
        if sweep_source is "VOLT":
            compliance = self.__check_current_compliance(port, stop_val, compliance)
            output_range = self.__check_voltage_range(output_range)
        else:
            compliance = self.__check_voltage_compliance(stop_val, compliance)
            output_range = self.__check_current_range(output_range)

        self.iwrite(f":ARM{port}:TRAN:DEL 0")
        self.iwrite(f":ARM{port}:ACQ:DEL {hold_time}")
        self.iwrite(f":TRIG{port}:TRAN:DEL 0")
        self.set_trigger_subsystem_settings(layer="TRIG", action="ACQ", channel=port, delay=hold_time)
        self.set_pulse_mode(puls_on=True, mode=sweep_source, channel=port, base=pulse_base,
                            puls_width=pulse_width)
        # set trigger period
        self.set_trigger_subsystem_settings(layer="TRIG", action="ALL", channel=port, timer=pulse_period)
        pattern = f":SOUR{port}:{sweep_source}"

        self.iwrite(f":SOUR{port}:FUNC:MODE {sweep_source}")
        self.iwrite(f"{pattern}:MODE SWE")
        self.iwrite(f":SOUR{port}:SWE:SPAC {sweep_mode}")
        self.iwrite(f":SOUR{port}:SWE:STA {sweep_direction}")

        if output_range is 0:
            self.iwrite(f"{pattern}:RANG:AUTO ON")
        else:
            if compliance is 0:
                self.iwrite(f"{pattern}:RANG {output_range}")
            else:
                self.iwrite(f"{pattern}:RANG:AUTO:LLIM {output_range}")

        self.iwrite(f"{pattern}:STAR {start_val}")
        self.iwrite(f"{pattern}:STOP {stop_val}")
        self.iwrite(f"`{pattern}`:POIN {step_number}")
        self.iwrite(f":SENS{port}:{sweep_sense}:PROT {compliance}")
        self.iwrite(f":TRIG{port}:ALL:SOUR AINT")
        self.iwrite(f":TRIG{port}:ALL:COUN {step_number}")

    def sweep_iv(self, port, measurement_mode, measurement_range, limited_range=0):
        # 4770 : Sweep_iv(port, measurement_mode, measurement_range, measurement_array_name, source array name,
        # dummy array name, status array name, time stamp array name

        """
        :param limited_range: 0/1/2, 0: Not use limited range, 1: use lower limited range, 2: use upper limited range
        :return: outputdata, source, real_source
        """
        self.iwrite(":FORM:DATA ASC")
        self.iwrite(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")

        self.__check_sweep_iv_measurement_range(port, measurement_mode, measurement_range, limited_range)

        # Initiate transition and acquire
        self.iwrite(f":INIT (@{port})")

        source_real = self.iread(f":FETC:ARR:SOUR? (@{port})")
        channel_curr = self.iread(f":FETC:ARR:CURR? (@{port})")
        channel_volt = self.iread(f":FETC:ARR:VOLT? (@{port})")

        if measurement_mode == 1:  # VF
            return source_real, channel_volt, channel_curr
        else:
            return source_real, channel_curr, channel_volt

    def __check_sweep_iv_measurement_range(self, port_, measurement_mode_, measurement_range_, range_limited):
        if measurement_mode_ == 1:  # VF
            measurement_range_ = self.__check_current_range(measurement_range_)
            mode_str = "VOLT"
        elif measurement_mode_ == 2:  # IF
            measurement_range_ = self.__check_voltage_range(measurement_range_)
            mode_str = "CURR"
        else:
            sys.exit(f"*** ERROR: No '{measurement_mode_}' mode, either 1 (VF) of 2(IF).")
        self.iwrite(f':SENS{port_}:FUNC "{mode_str}"')
        self.check_error()
        pattern = f':SENS{port_}:{mode_str}'
        if measurement_range_ == 0:
            self.iwrite(f'{pattern}:RANG:AUTO ON')
        else:
            if range_limited == 0:
                self.iwrite(f'{pattern}:RANG:AUTO OFF')
                self.iwrite(f'{pattern}:RANG {measurement_range_}')
            elif range_limited == 1:
                self.iwrite(f':RANG:AUTO:LLIM {measurement_range_}')
            elif range_limited == 2:
                self.iwrite(f':RANG:AUTO:ULIM {measurement_range_}')
        self.iwrite(f':OUTP{port_} ON')
        self.check_error()

    def sweep_iv2(self, port1, meas_mode1, meas_range1, port2, meas_mode2, meas_range2, range_limited=0):
        self.iwrite(":FORM:DATA ASC")
        self.check_error()
        self.iwrite(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")
        self.check_error()

        self.__check_sweep_iv_measurement_range(port1, meas_mode1, meas_range1, range_limited)
        self.__check_sweep_iv_measurement_range(port2, meas_mode2, meas_range2, range_limited)

        # Initiate transition and acquire
        self.iwrite(f":INIT (@{port1},{port2})")
        self.check_error()

        source1 = self.iread(f":FETC:ARR:SOUR? (@{port1})").split(',')
        source2 = self.iread(f":FETC:ARR:SOUR? (@{port2})").split(',')
        ch1_curr = self.iread(f":FETC:ARR:CURR? (@{port1})").split(",")
        ch1_volt = self.iread(f":FETC:ARR:VOLT? (@{port1})").split(",")
        ch2_curr = self.iread(f":FETC:ARR:CURR? (@{port2})").split(",")
        ch2_volt = self.iread(f":FETC:ARR:VOLT? (@{port2})").split(",")

        if meas_mode1 == 1:  # VF
            output_data1, source1_real = ch1_volt, ch1_curr
        else:
            output_data1, source1_real = ch1_curr, ch1_volt

        if meas_mode2 == 1:  # VF
            output_data2, source2_real = ch2_volt, ch2_curr
        else:
            output_data2, source2_real = ch2_curr, ch2_volt

        self.check_error()

        return [output_data1, output_data2], [source1, source2], [source1_real, source2_real]

    def set_trigger_subsystem_settings(self, layer="TRIG", action="TRAN", channel=1, bypass=None, count=None,
                                       delay=None, sour=None, timer=None, trg_out=None, trg_mode=None, ):
        """
        Set trigger subsystem settings, you should specify the function you want to use,
        layer you want to trigger, action you want to tirgger, and channel you want to initiate.
        Besides, you also need to specify the setting value of function you use.

        :param layer: ARM/TRIG/ALL
        :param action: ACQ/TRAN/ALL
        :param channel: 0/1/2, 0 means use channel 1,2
        :param bypass: True: ONCE, False: OFF
        :param count: 1 ~ 100000, 2147483647 indicates infinity
        :param delay: 0 ~ 100000 sec
        :param sour: EXT1~EXT14, AINT, BUS, TIMer, INT1, INT2
        :param timer: 1e-5 ~ 1e+5
        :param trg_out: EXT1 ~ EXT14, INT1, INT2
        :param trg_mode: ON/OFF
        :return:
        """
        layer, action = layer.upper(), action.upper()
        if bypass is not None:
            bypass = "ONCE" if bypass else "OFF"

        value = {"BYP": bypass, "COUN": count, "DEL": delay, "SOUR": sour, "TIM": timer, "TOUT:SIGN": trg_out,
                 "TOUT:STAT": trg_mode, }

        if layer == "ALL":
            if channel > 0:
                for func in value:
                    if value[func] is not None:
                        self.iwrite(f":ARM{channel}:{action}:{func} {value[func]}")
                        self.iwrite(f":TRIG{channel}:{action}:{func} {value[func]}")
            elif channel == 0:
                for func in value:
                    if value[func] is not None:
                        self.iwrite(f":ARM1:{action}:{func} {value[func]}")
                        self.iwrite(f":ARM2:{action}:{func} {value[func]}")
                        self.iwrite(f":TRIG1:{action}:{func} {value[func]}")
                        self.iwrite(f":TRIG2:{action}:{func} {value[func]}")

        elif layer == "ARM" or layer == "TRIG":
            if channel > 0:
                for func in value:
                    if value[func] is not None:
                        self.iwrite(f":{layer}{channel}:{action}:{func} {value[func]}")
            elif channel == 0:
                for func in value:
                    if value[func] is not None:
                        self.iwrite(f":{layer}1:{action}:{func} {value[func]}")
                        self.iwrite(f":{layer}2:{action}:{func} {value[func]}")

    def trigger_subsystem_settings_query(self, func, layer="ALL"):
        """
        Query the settings of trigger subsystem, please speficy the function and layer you want to query.

        :param func: BYP/COUN/DEL/SOUR/TIM/TOUT:SIGN/TPUT:STAT
        :param layer: ALL/TRIG/ARM
        :return: dict
        """
        res = dict()
        layer = layer.upper()
        res["ARM1"], res["ARM2"] = dict(), dict()
        res["TRIG1"], res["TRIG2"] = dict(), dict()

        if layer == "ARM" or layer == "ALL":
            res["ARM1"]["ACQ"] = self.iread(f":ARM1:ACQ:{func}?")
            res["ARM2"]["ACQ"] = self.iread(f":ARM2:ACQ:{func}?")
            res["ARM1"]["TRAN"] = self.iread(f":ARM1:TRAN:{func}?")
            res["ARM2"]["TRAN"] = self.iread(f":ARM2:TRAN:{func}?")

        if layer == "TRIG" or layer == "ALL":
            res["TRIG1"]["ACQ"] = self.iread(f":TRIG1:ACQ:{func}?")
            res["TRIG2"]["ACQ"] = self.iread(f":TRIG2:ACQ:{func}?")
            res["TRIG1"]["TRAN"] = self.iread(f":TRIG1:TRAN:{func}?")
            res["TRIG2"]["TRAN"] = self.iread(f":TRIG2:TRAN:{func}?")
        return res

    def send_imm_trigger(self, layer="ALL", action="ALL", channel=0):
        """
        Send immediate trigger for the specified device action to the specified channel.

        :param layer: ALL/ARM/TRIG
        :param action: ACQ/TRAN/ALL
        :param channel: 0/1/2, 0 means use channel 1,2
        """
        layer, action = layer.upper(), action.upper()

        if layer == "ALL":
            if channel > 0:
                self.iwrite(f":ARM:{action} (@{channel})")
                self.iwrite(f":TRIG:{action} (@{channel})")
            elif channel == 0:
                self.iwrite(f":ARM:{action} (@1,2)")
                self.iwrite(f":ARM:{action} (@1,2)")
                self.iwrite(f":TRIG:{action} (@1,2)")
                self.iwrite(f":TRIG:{action} (@1,2)")
        elif layer == "ARM" or layer == "TRIG":
            if channel > 0:
                self.iwrite(f":{layer}:{action} (@{channel})")
            elif channel == 0:
                self.iwrite(f":{layer}:{action} (@1,2)")
                self.iwrite(f":{layer}:{action} (@1,2)")

    def initiates_trigger(self, channel=1, action="ALL"):
        """
        Initiates the specified device action for the specified channel. Trigger status is changed from idle to initiated.

        :param channel: 0/1/2, 0 means use channel 1,2
        :param action: ACQ/TRAN/ALL
        :return:
        """
        action = action.upper()
        if channel > 0:
            self.iwrite(f":INIT:IMM:{action} (@{channel})")
        elif channel == 0:
            self.iwrite(f":INIT:IMM:{action} (@1,2)")

    def abort_trigger(self, channel=0, action="ALL"):
        """
        Aborts the specified device action for the specified channel. Trigger status is changed to idle.

        :param channel: 0/1/2, 0 means use channel 1,2
        :param action: ACQ/TRAN/ALL
        :return:
        """
        action = action.upper()
        if channel > 0:
            self.iwrite(f":ABOR:IMM:{action} (@{channel})")
        elif channel == 0:
            self.iwrite(f":ABOR:IMM:{action} (@1,2)")

    def device_action_status(self, channel=0):
        """
        Checks the status of the specified device action for the specified channel, and waits until the status is changed to idle.
        response returns 1 if the specified device action is in the idle state.

        :param channel: 0/1/2, 0 means use channel 1,2
        :return:
        """
        res = dict()
        res["CH1"], res["CH2"] = dict(), dict()
        if channel > 0:
            res[f"CH{channel}"]["ACQ"] = self.iread(f":IDLE{channel}:ACQ?")
            res[f"CH{channel}"]["TRAN"] = self.iread(f":IDLE{channel}:TRAN?")
        elif channel == 0:
            res[f"CH1"]["ACQ"] = self.iread(f":IDLE1:ACQ?")
            res[f"CH1"]["TRAN"] = self.iread(f":IDLE1:TRAN?")
            res[f"CH2"]["ACQ"] = self.iread(f":IDLE2:ACQ?")
            res[f"CH2"]["TRAN"] = self.iread(f":IDLE2:TRAN?")
        return res

    def set_digital_io_settings(self, pin=1, func=None, polarity=None, timing=None, pul_width=None, trg_type=None):
        """
        Set digital io (EXT1 ~ EXT14) settings, including function, polarity, trigger timing, pulse width, trigger type.

        :param pin: 1 ~ 14
        :param func: DIO/DINP/HVOL/TINP/TOUT
        :param polarity: NEG/POS
        :param timing: BEFORE/AFTER/BOTH
        :param pul_width: default is 1e-4
        :param trg_type: EDGE/LEVEL
        """
        if func:
            self.iwrite(f":SOUR:DIG:EXT{pin}:FUNC {func}")
        if polarity:
            self.iwrite(f":SOUR:DIG:EXT{pin}:POL {polarity}")
        if timing:
            self.iwrite(f":SOUR:DIG:EXT{pin}:TOUT:POS {timing}")
        if pul_width:
            self.iwrite(f":SOUR:DIG:EXT{pin}:TOUT:WIDT {pul_width}")
        if trg_type:
            self.iwrite(f":SOUR:DIG:EXT{pin}:TOUT:TYPE {trg_type}")

    def digital_io_settings_query(self, pin=1):
        """
        Read digital o setting from b2902

        :param pin: 1 ~ 14
        :return:
        """
        res = dict()
        res[f"EXT{pin}"] = dict()

        res[f"EXT{pin}"]["function"] = self.iread(f":SOUR:DIG:EXT{pin}:FUNC?")
        res[f"EXT{pin}"]["polarity"] = self.iread(f":SOUR:DIG:EXT{pin}:POL?")
        res[f"EXT{pin}"]["timing"] = self.iread(f":SOUR:DIG:EXT{pin}:TOUT:POS?")
        res[f"EXT{pin}"]["width"] = self.iread(f":SOUR:DIG:EXT{pin}:TOUT:WIDT?")
        res[f"EXT{pin}"]["type"] = self.iread(f":SOUR:DIG:EXT{pin}:TOUT:TYPE?")
        return res

    def set_action_layer_trigger_output(self, pin=1, stat=None, action=None, channel=1):
        """
        Set the trigger output for the status change between the trigger layer and the transient/acquire device action.

        :param pin: 1 ~ 14
        :param stat: ON/OFF
        :param action: ALL/TRAN/ACQ
        :param channel: 0/1/2, 0 means use channel 1,2
        :return:
        """
        if not action:
            return

        action = action.upper()
        if action == "ACQ" or action == "ALL":
            if channel > 0:
                self.iwrite(f":SENS{channel}:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iwrite(f":SENS{channel}:TOUT:STAT {stat}")
            elif channel == 0:
                self.iwrite(f":SENS1:TOUT:SIGN EXT{pin}")
                self.iwrite(f":SENS2:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iwrite(f":SENS1:TOUT:STAT {stat}")
                    self.iwrite(f":SENS2:TOUT:STAT {stat}")

        if action == "TRAN" or action == "ALL":
            if channel > 0:
                self.iwrite(f":SOUR{channel}:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iwrite(f":SOUR{channel}:TOUT:STAT {stat}")
            elif channel == 0:
                self.iwrite(f":SOUR1:TOUT:SIGN EXT{pin}")
                self.iwrite(f":SOUR2:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iwrite(f":SOUR1:TOUT:STAT {stat}")
                    self.iwrite(f":SOUR2:TOUT:STAT {stat}")

    def action_layer_trigger_output_query(self):
        """
        Query the settings for action layer trigger output.
        :return:
        """
        res = dict()
        res["CH1"], res["CH2"] = dict(), dict()

        res["CH1"]["sour.output"] = self.iread(":SOUR1:TOUT:SIGN?")
        res["CH2"]["sour.output"] = self.iread(":SOUR2:TOUT:SIGN?")
        res["CH1"]["sour.stat"] = self.iread(":SOUR1:TOUT:STAT?")
        res["CH2"]["sour.stat"] = self.iread(":SOUR2:TOUT:STAT?")
        res["CH1"]["sens.output"] = self.iread(":SENS1:TOUT:SIGN?")
        res["CH2"]["sens.output"] = self.iread(":SENS2:TOUT:SIGN?")
        res["CH1"]["sens.stat"] = self.iread(":SENS1:TOUT:STAT?")
        res["CH2"]["sens.stat"] = self.iread(":SENS2:TOUT:STAT?")
        return res

    def set_pulse_mode(self, puls_on=True, mode="CURR", channel=1, peak=0, base=0, puls_width=5e-5,
                       puls_del=0, comp=None):
        """Set the pulse mode settings, including pulse mode, pulse width and pulse delay

        :param puls_on: True/False
        :param mode: CURR/VOLT
        :param channel: 0/1/2, 0 means use channel 1,2
        :param peak : peak value of pulse
        :param base : base force voltage/current value
        :param puls_width: 5E-5 to 100000 seconds
        :param puls_del: 0.0 to 99999.9, in seconds
        :param comp: compliance value of current or voltage
        :return:
        """
        if channel > 0:
            if puls_on:
                self.iwrite(f":SOUR{channel}:FUNC:SHAP PULS")
                self.iwrite(f":SOUR{channel}:{mode}:TRIG {peak}")
                self.iwrite(f":SOUR{channel}:{mode} {base}")
                if comp:
                    if mode == "CURR":
                        self.iwrite(f":SENS{channel}:VOLT:PROT {comp}")
                    elif mode == "VOLT":
                        self.iwrite(f":SENS{channel}:CURR:PROT {comp}")
            else:
                self.iwrite(f":SOUR{channel}:FUNC:SHAP DC")
            self.iwrite(f":SOUR{channel}:FUNC:MODE {mode}")
            self.iwrite(f":SOUR{channel}:PULS:DEL {puls_del}")
            self.iwrite(f":SOUR{channel}:PULS:WIDT {puls_width}")
        elif channel == 0:
            if puls_on:
                self.iwrite(":SOUR1:FUNC:SHAP PULS")
                self.iwrite(f":SOUR1:{mode}:TRIG {peak}")
                self.iwrite(f":SOUR1:{mode} {base}")
                self.iwrite(":SOUR2:FUNC:SHAP PULS")
                self.iwrite(f":SOUR2:{mode}:TRIG {peak}")
                self.iwrite(f":SOUR2:{mode} {base}")
                if comp:
                    if mode == "CURR":
                        self.iwrite(f":SENS1:VOLT:PROT {comp}")
                        self.iwrite(f":SENS2:VOLT:PROT {comp}")
                    elif mode == "VOLT":
                        self.iwrite(f":SENS1:CURR:PROT {comp}")
                        self.iwrite(f":SENS2:CURR:PROT {comp}")
            else:
                self.iwrite(":SOUR1:FUNC:SHAP DC")
                self.iwrite(":SOUR2:FUNC:SHAP DC")
            self.iwrite(f":SOUR1:FUNC:MODE {mode}")
            self.iwrite(f":SOUR1:PULS:DEL {puls_del}")
            self.iwrite(f":SOUR1:PULS:WIDT {puls_width}")
            self.iwrite(f":SOUR2:FUNC:MODE {mode}")
            self.iwrite(f":SOUR2:PULS:DEL {puls_del}")
            self.iwrite(f":SOUR2:PULS:WIDT {puls_width}")

    def pulse_mode_query(self):
        """query pulse settings

        :return:
        """
        res = dict()
        res["CH1"], res["CH2"] = dict(), dict()

        res["CH1"]["shape"] = self.iread(":SOUR1:FUNC:SHAP?")
        res["CH2"]["shape"] = self.iread(":SOUR2:FUNC:SHAP?")
        res["CH1"]["mode"] = self.iread(":SOUR1:FUNC:MODE?")
        res["CH2"]["mode"] = self.iread(":SOUR2:FUNC:MODE?")
        res["CH1"]["delay"] = self.iread(":SOUR1:PULS:DEL?")
        res["CH2"]["delay"] = self.iread(":SOUR2:PULS:DEL?")
        res["CH1"]["width"] = self.iread(":SOUR1:PULS:WIDT?")
        res["CH2"]["width"] = self.iread(":SOUR2:PULS:WIDT?")
        return res

    def __check_voltage_range(self, output_range_):
        """" Table 2-3, 2-4 @ Keysight B2900 SCPI cmd Reference
            * output range decide the resolution
            check and convert range from itis check2902_voltage_range
            Using in:
            1. force_v_2902
            2. measure_v_2902
            3. set_iv_2902
            4. sweep_iv_2902
        """
        if abs(output_range_) is 0:
            return 0
        elif abs(output_range_) <= 0.21:
            return 0.2
        elif 0.21 < abs(output_range_) <= 2.1:
            return 2
        elif 2 < abs(output_range_) <= 21:
            return 20
        elif 21 < abs(output_range_) <= 200:
            return 200
        else:
            print(f"*** ERROR : Incorrect Input SMU Voltage Range : {output_range_} is not correct.")
            return -54088

    def __check_current_range(self, output_range_):
        """" Table 2-5 in Keysight B2900 User's Guide, E6

            check and convert range from itis check2902_current_range
            Using in:
            1. force_i_2902
            2. measure_i_2902
            3. sweep_iv_2902
            05/16
        :param : output_range_: input range
        :return:
        """

        if abs(output_range_) == 0:
            return 0
        elif abs(output_range_) <= 10.6e-9:
            return 10e-9
        elif 10e-9 < abs(output_range_) <= 106e-9:
            return 100e-9
        elif 100e-9 < abs(output_range_) <= 1.06e-6:
            return 1e-6
        elif 1e-6 < abs(output_range_) <= 10.6e-6:
            return 10e-6
        elif 10e-6 < abs(output_range_) <= 106e-6:
            return 100e-6
        elif 100e-6 < abs(output_range_) <= 1.06e-3:
            return 1e-3
        elif 1e-3 < abs(output_range_) <= 10.6e-3:
            return 10e-3
        elif 10e-3 < abs(output_range_) <= 106e-3:
            return 100e-3
        elif 100e-3 < abs(output_range_) <= 1.06:
            return 1
        elif 1 < abs(output_range_) <= 1.53:
            return 1.5
        elif 1.5 < abs(output_range_) <= 3.06:
            return 3
        elif 3 < abs(output_range_) <= 10.6:
            return 10
        else:
            print(f"*** ERROR : {output_range_} is invalid current range.")
            return -54088

    def __check_setiv_other(self, step_, hold_time, delay_time, power_compliance, stop_mode):
        if step_ < 2 or step_ > 1001:
            sys.exit("*** ERROR : step_ must be 2 to 1001.".format(step_))
        if hold_time < 0 or hold_time > 655.35:
            sys.exit("*** ERROR : hold_time must be 0 to 655.35.".format(hold_time))
        if delay_time < 0 or delay_time > 100:
            sys.exit(f"*** ERROR : delay_time must be 0 to 100 when using B2902 Series.".format(delay_time))
        if power_compliance != 0:
            sys.exit(f"*** ERROR : P_compliance must be 0 when using B2902 Series.{delay_time}")
        if stop_mode != 0 and stop_mode != 1:
            sys.exit("*** ERROR : stop_mode must be 0 or 1.".format(stop_mode))
        return "check step、hold time、delay time and stop mode : PASS"


class OfflineB2902(OnlineB2902):
    def __init__(self):
        print("OFFLINE SIMULATION")

    def iread(self, cmd):
        time_start = time.perf_counter()
        print(cmd)
        timetrack.append([cmd, time.perf_counter() - time_start])
        return cmd

    def iwrite(self, cmd):
        time_start = time.perf_counter()
        print (cmd)
        timetrack.append([cmd, time.perf_counter() - time_start])

