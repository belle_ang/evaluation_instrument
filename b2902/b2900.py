import numpy as np
from b2902.b2902_CONFIG import *
import pyvisa
from . import *


class OnlineB2900( BaseDriver):
    """ Online 2900 """
    fv_channel1_voltage = 0
    fv_channel2_voltage = 0
    # for select case in sweep iv from set_iv
    ivmode2900 = 1

    def __init__(self, inst_addr=None, newline='\n'):
        self.inst: pyvisa.resources.MessageBasedResource = pyvisa.ResourceManager().open_resource("GPIB0::4::INSTR")
        # self.inst = sicl.SICL(inst_addr, newline)
        # self.INST_ADDRESS = inst_addr

    def iprintf(self, command):
        return self.inst.iprintf(command)

    def ipromptf(self, command):
        result = self.inst.ipromptf(command)
        return result.replace("\n", "")

    def ipromptf_largebuff(self, command):
        result = self.inst.ipromptf_largebuff(command)
        return result.replace("\n", "")

    def log_level(self, level):
        self.logger.setLevel(level)

    def init_system(self):
        self.iprintf("*RST")
        idn = self.ipromptf("*IDN?")
        import time
        time.sleep(1)
        return idn



    def check_error_code_debug(self):
        """"
            This Subprogram check ERROR code in send any command
            for B2900,send ERRX? Can clear B2900 error buffer.
            When debug,change debug_mode = 1.
            In release,change debug_mode = 0.
        :return: No return
        """
        msg_list = []
        if TIS_driver_debug_mode:
            while True:
                error_message = self.ipromptf(":SYST:ERR?")
                error_message_output = error_message.rstrip().split(",")
                if error_message_output[0] != "+0":
                    msg_list.append(f"{error_message_output[0]}, {error_message_output[1]}")
                else:
                    break
            if len(msg_list) > 0:
                self.logger.warning(
                    "*** ERROR : B2900 Instrument Error : {}"
                    " ADDRESS : {}".format(';'.join(msg_list), self.INST_ADDRESS))
                raise TISError("*** ERROR : B2900 Instrument Error : {}"
                               " ADDRESS : {}".format(';'.join(msg_list), self.INST_ADDRESS))

    def check_smu_input_module(self, smu_input_module):
        """"
            This Subprogram check ERROR code in send any command
            follow b1500 structure.
            When debug,change debug_mode = 1.
            In release,change debug_mode = 0.
            Using in:
            1. force_v_smu_2900
            2. force_i_smu_2900
            3. measure_i_smu_2900
            4. force_v_cmu_2900

        :param smu_input_module: input port with module;
        0: MPSMU, 1: HRSMU, 2: HPSMU, 3: MFCMU, 4: HVSPGU, 5: WGFMU, 6: MCSMU, 7: HCSMU, 8: HVSMU,
        9: HRSMU and MPSMU with ASU, 10: SCUU
        :return: No return
        """
        if 0 <= smu_input_module <= 10:
            smu_output_module = smu_input_module
        else:
            self.logger.warning(
                "*** ERROR : Incorrect Input SMU Module is not correct."
                " ADDRESS : {}".format(smu_input_module, self.INST_ADDRESS))
            raise TISError("*** ERROR : Incorrect Input SMU Module is not correct."
                           " ADDRESS : {}".format(smu_input_module, self.INST_ADDRESS))
        return smu_output_module

    def check_fv_calc_current_compliance(self, channel, dv_input_voltage, force_v_smu_compliance, output_status):
        """"
            This Subprogram check Force_v Current compliance. follow VSPEC.
            calc_curr_comp_list [0: input compliance,
                                 1: channel1:on/off mode,
                                 2: channel1: input_voltage,
                                 3: channel1:transfer compliance,
                                 4: channel2:on/off mode,
                                 5: channel2: input_voltage,
                                 6: channel2:transfer compliance]
            Using in:
            1. force_v_smu_2900

        :param channel: input voltage,from myself command setting.
        :param dv_input_voltage: input compliance value.
        :param force_v_smu_compliance: input compliance value.
        :param output_status: Output on/off status.
        :return: B2900_calc_curr_comp_list
        """
        calc_curr_comp_list = [0, 0, 0, 0, 0, 0, 0]
        calc_curr_comp_list[0] = force_v_smu_compliance

        if channel == 1:
            calc_curr_comp_list[2] = dv_input_voltage
            calc_curr_comp_list[3] = 0.105
            self.fv_channel1_voltage = dv_input_voltage
        elif channel == 2:
            calc_curr_comp_list[5] = dv_input_voltage
            calc_curr_comp_list[6] = 0.105
            self.fv_channel2_voltage = dv_input_voltage
        # 當status = 0 , 兩個都關著， 21時 , 他們可以拿到最高1.515, 6時可以拿到最高3.03
        # if channel 1 using or channel 2 not using
        if output_status == 0: # 原本兩個都關著，所以第一個開的可以拿到最高的電壓
            if dv_input_voltage <= 21:
                calc_curr_comp_list[3] = 1.515
                calc_curr_comp_list[6] = 1.515
            if dv_input_voltage <= 6:
                calc_curr_comp_list[3] = 3.03
                calc_curr_comp_list[6] = 3.03
        else: # 有一個開著一個關著，
            # if channel 1 or channel 2 using
            if 0 < self.fv_channel1_voltage <= 6:
                if 0 < self.fv_channel2_voltage <= 6:
                    if channel == 1:
                        calc_curr_comp_list[3] = 4 - int(calc_curr_comp_list[6])
                    elif channel == 2:
                        calc_curr_comp_list[6] = 4 - int(calc_curr_comp_list[3])
                elif 6 < self.fv_channel2_voltage <= 21:
                    if channel == 1:
                        calc_curr_comp_list[3] = 4 - (int(calc_curr_comp_list[6]) * 1.6)
                    elif channel == 2:
                        calc_curr_comp_list[6] = (4 - int(calc_curr_comp_list[3])) / 4
                elif 21 < self.fv_channel2_voltage <= 210:
                    if channel == 1:
                        calc_curr_comp_list[3] = 3.03
                    elif channel == 2:
                        calc_curr_comp_list[6] = 0.105
            elif 6 < self.fv_channel1_voltage <= 21:
                if 0 < self.fv_channel2_voltage <= 6:
                    if channel == 1:
                        calc_curr_comp_list[3] = 2.5 - (int(calc_curr_comp_list[6]) * 0.625)
                    elif channel == 2:
                        calc_curr_comp_list[6] = (2.5 - int(calc_curr_comp_list[3])) / 0.625
                elif 6 < self.fv_channel2_voltage <= 21:
                    if channel == 1:
                        calc_curr_comp_list[3] = 2.5 - int(calc_curr_comp_list[6])
                    elif channel == 2:
                        calc_curr_comp_list[6] = 2.5 - int(calc_curr_comp_list[3])
                elif 21 < self.fv_channel2_voltage <= 210:
                    if channel == 1:
                        calc_curr_comp_list[3] = 1.515
                    elif channel == 2:
                        calc_curr_comp_list[6] = 0.105
            elif 21 < self.fv_channel1_voltage <= 210:
                if 0 < self.fv_channel2_voltage <= 6:
                    if channel == 1:
                        calc_curr_comp_list[3] = 0.105
                    elif channel == 2:
                        calc_curr_comp_list[6] = 3.03
                elif 6 < self.fv_channel2_voltage <= 21:
                    if channel == 1:
                        calc_curr_comp_list[3] = 0.105
                    elif channel == 2:
                        calc_curr_comp_list[6] = 1.515
                elif 21 < self.fv_channel2_voltage <= 210:
                    if channel == 1:
                        calc_curr_comp_list[3] = 0.105
                    elif channel == 2:
                        calc_curr_comp_list[6] = 0.105

        return calc_curr_comp_list

    def check_voltage_range(self, input_range, input_module):
        """"
            check and transfer range from itis check2900_voltage_range
            Using in:
            1. force_v_smu_2900
            2. measure_v_smu_2900
            3. set_iv_2900
            4. sweep_iv_2900

        :param : input_range: input range
        :param : input_module: input port with module; 0: SMU
        :return:
        """

        input_range = abs(input_range)
        if input_range == 0:
            output_range = 0
        elif input_range <= 0.2:
            output_range = 0.2
        elif 0.2 < abs(input_range) <= 2:
            output_range = 2
        elif 2 < abs(input_range) <= 20:
            output_range = 20
        elif 20 < abs(input_range) <= 200:
            output_range = 200
        else:
            output_range = -54088
            self.logger.warning(
                "*** ERROR : Incorrect Input SMU Voltage Range : {} is not correct."
                " ADDRESS : {}".format(output_range, self.INST_ADDRESS))
            raise TISError("*** ERROR : Incorrect Input SMU Voltage Range : {} is not correct."
                           " ADDRESS : {}".format(output_range, self.INST_ADDRESS))
        return output_range

    def check_current_range(self, input_range, input_module):
        """"
            check and transfer range from itis check2900_current_range
            Using in:
            1. force_i_smu_2900
            2. measure_i_smu_2900
            3. sweep_iv_2900
            05/16
        :param : input_range: input range
        :param : input_module: input port with module; 0: SMU
        :return:
        """

        if abs(input_range) == 0:
            output_range = 0
        elif abs(input_range) <= 10e-9:
            output_range = 10e-9
        elif 10e-9 < abs(input_range) <= 100e-9:
            output_range = 100e-9
        elif 100e-9 < abs(input_range) <= 1e-6:
            output_range = 1e-6
        elif 1e-6 < abs(input_range) <= 10e-6:
            output_range = 10e-6
        elif 10e-6 < abs(input_range) <= 100e-6:
            output_range = 100e-6
        elif 100e-6 < abs(input_range) <= 1e-3:
            output_range = 1e-3
        elif 1e-3 < abs(input_range) <= 10e-3:
            output_range = 10e-3
        elif 10e-3 < abs(input_range) <= 100e-3:
            output_range = 100e-3
        elif 100e-3 < abs(input_range) <= 1:
            output_range = 1
        elif 1 < abs(input_range) <= 1.5:
            output_range = 1.5
        elif 1.5 < abs(input_range) <= 3:
            output_range = 3
        elif 3 < abs(input_range) <= 10:
            output_range = 10
        else:
            output_range = -54088
            self.logger.warning(
                "*** ERROR : Incorrect Input SMU Current Range is not correct."
                " ADDRESS : {}".format(output_range, self.INST_ADDRESS))
            raise TISError("*** ERROR : Incorrect Input SMU Current Range is not correct."
                           " ADDRESS : {}".format(output_range, self.INST_ADDRESS))

        return output_range

    def _error_decorator(func):
        """
        Check error code after sending command
        :param func:
        :return:
        """

        @wraps(func)
        def wrapper(*args, **kwargs):
            self = args[0]  # args[0] is object itself
            func(*args, **kwargs)
            self.check_error_code()

            return self.INST_ADDRESS, func.__name__

        return wrapper

    @staticmethod
    def check_measure_status(input_val):
        """"
            This Subprogram check measure status. follow VSPEC.
            if return 9999 is not define status.
            Using in:
            1. measure_i_smu_2900
            2. measure_v_smu_2900
            3. sweep_iv_2900
            4. measure_cmu_2900

        :param input_val: input value,from myself command setting.
        :return: output_val
        """
        input_status = input_val[0]
        if input_status == "N":
            output_val = 0
        elif input_status == "T":
            output_val = 1
        elif input_status == "C":
            output_val = 2
        elif input_status == "V":
            output_val = 3
        elif input_status == "X":
            output_val = 4
        elif input_status == "G":
            output_val = 5
        elif input_status == "S":
            output_val = 6
        elif input_status == "U":
            output_val = 7
        elif input_status == "D":
            output_val = 8
        else:
            output_val = 9999

        return output_val

    def check_setiv_sweepmode(self, set_iv_sweep_mode):
        """"
            This Subprogram check sweep mode. follow VSPEC.
            Using in:
            1. set_iv_2900

        :param set_iv_sweep_mode: input sweep mode. 1,3,-1,-3: V mode; 2, 4, -2, -4: I mode.
        :return: output_val
        """
        if set_iv_sweep_mode == 1 or set_iv_sweep_mode == 2 or set_iv_sweep_mode == 3 or set_iv_sweep_mode == 4 or \
                set_iv_sweep_mode == -1 or set_iv_sweep_mode == -2 or set_iv_sweep_mode == -3 or set_iv_sweep_mode == -4:
            set_iv_sweep_mode = set_iv_sweep_mode
        else:
            self.logger.warning(
                "*** ERROR : Sweep mode selected wrongly."
                "Using +-1、+-2、+-3、+-4. ADDRESS : {}".format(set_iv_sweep_mode, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : Sweep mode selected wrongly."
                "Using +-1、+-2、+-3、+-4. ADDRESS : {}".format(set_iv_sweep_mode, self.INST_ADDRESS))

        return set_iv_sweep_mode

    def check_setiv_other(self, set_iv_2900_step, set_iv_hold_t, set_iv_delay_t, set_iv_power_compliance,
                          set_iv_stop_mode):
        """"
            This Subprogram check sweep mode. follow VSPEC.
            Using in:
            1. set_iv_2900

        :param set_iv_2900_step:        點數
        :param set_iv_hold_t:           起始點停頓ms
        :param set_iv_delay_t:          每個點的停頓ms
        :param set_iv_stop_mode:        stop_mode
        :param set_iv_power_compliance:        stop_mode
        :return: Massage
        """
        # check step
        if set_iv_2900_step < 2 or set_iv_2900_step > 1001:
            self.logger.warning(
                "*** ERROR : set_iv_2900_step must be 2 to 1001. ADDRESS : {}".format(set_iv_2900_step,
                                                                                      self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : set_iv_2900_step must be 2 to 1001. ADDRESS : {}".format(set_iv_2900_step,
                                                                                      self.INST_ADDRESS))
        # check hold time
        if set_iv_hold_t < 0 or set_iv_hold_t > 655.35:
            self.logger.warning(
                "*** ERROR : set_iv_hold_t must be 0 to 655.35. ADDRESS : {}".format(set_iv_hold_t, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : set_iv_hold_t must be 0 to 655.35. ADDRESS : {}".format(set_iv_hold_t, self.INST_ADDRESS))
        # check delay time
        if set_iv_delay_t < 0 or set_iv_delay_t > 100:
            self.logger.warning(
                "*** ERROR : set_iv_delay_t must be 0 to 100 when using B2900 Series. ADDRESS : {}".format(
                    set_iv_delay_t, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : set_iv_delay_t must be 0 to 100 when using B2900 Series. ADDRESS : {}".format(
                    set_iv_delay_t, self.INST_ADDRESS))
        # check power compliance
        if set_iv_power_compliance != 0:
            self.logger.warning(
                "*** ERROR : P_compliance must be 0 when using B2900 Series. ADDRESS : {}".format(
                    set_iv_delay_t, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : P_compliance must be 0 when using B2900 Series. ADDRESS : {}".format(
                    set_iv_delay_t, self.INST_ADDRESS))
        # check stop mode
        if set_iv_stop_mode == 0 or set_iv_stop_mode == 1:
            pass
        else:
            self.logger.warning(
                "*** ERROR : set_iv_stop_mode must be 0 or 1. ADDRESS : {}".format(set_iv_stop_mode, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : set_iv_stop_mode must be 0 or 1. ADDRESS : {}".format(set_iv_stop_mode, self.INST_ADDRESS))

        return "check step、hold time、delay time and stop mode : PASS"

    def check_sweepiv_transfer_ivmode(self, sweep_iv_mode):
        """"
            This Subprogram check and transfer sweep iv mode. follow VSPEC.
            Using in:
            1. sweep_iv_2900

        :param sweep_iv_mode:  iv mode
        :return: sweep_iv_mode_transfer
        """
        if sweep_iv_mode == 1:
            # sweep_iv_mode_case for select Voltage or Current mode
            sweep_iv_mode_transfer = 2
        elif sweep_iv_mode == 2:
            sweep_iv_mode_transfer = 1
        else:
            self.logger.warning(
                "*** ERROR : sweep_iv_mode must be 1 or 2. ADDRESS : {}".format(sweep_iv_mode, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : sweep_iv_mode must be 1 or 2. ADDRESS : {}".format(sweep_iv_mode, self.INST_ADDRESS))

        return sweep_iv_mode_transfer
    # check

    # Command
    # internal Command
    # internal Command
    # system
    def init_system(self):
        """
        initial system
        "*RST"  => Reset system to default.
        "*IDN?" => send HELLO command. Can check GPIB work.
        :return:
        """
        self.iprintf("*RST")
        idn = self.ipromptf("*IDN?")
        # self.check_error_code_debug()

        return idn

    @_error_decorator
    def disable_port_2900_rst(self):
        """
        Disable Port.
        ":SYST:ERR:ALL?"    => Reset 2900 error buffer.
        "*RST"              => Reset system to default.
        目前在初始化也有做消除ERROR Buffer的動作，可防止上一次的ERROR MESSAGE沒有消除，被判定成錯誤而跳出程式
        因為Debug，所以先不在Disable的時候刪除，穩定後可以考慮使用
        :return: GPIB address, System Error
        """
        syst_err = self.ipromptf(":SYST:ERR:ALL?")
        self.iprintf("*RST")
    # system

    # SMU spot

    @_error_decorator
    def force_v_smu_2900(self, force_v_smu_port, force_v_smu_module, force_v_smu_voltage, force_v_smu_range=0,
                         force_v_smu_compliance=0):
        """
        This Subprogram conduct Voltage on the SMU or Pin
        self.check_smu_input_module:                check input module
        self.check_fv_calc_current_compliance:      transfer compliance.
        self.check_voltage_range:                   check voltage range

        :param force_v_smu_port: input port: 1 or 2
        :param force_v_smu_module: input port with module; 0: SMU
        :param force_v_smu_voltage: force voltage
        :param force_v_smu_range: force range
        :param force_v_smu_compliance: I compliance
        :return:
        """
        # check input module
        force_v_smu_module = self.check_smu_input_module(force_v_smu_module)

        # check and transfer compliance
        output_status = 0
        if force_v_smu_compliance == 0:
            output1_onoff_status = self.iprintf("OUTP1:STAT?")
            output2_onoff_status = self.iprintf("OUTP2:STAT?")
            output_status = int(output1_onoff_status) + int(output2_onoff_status)
        fv_transfer_compliance = self.check_fv_calc_current_compliance(force_v_smu_port, force_v_smu_voltage,
                                                                       force_v_smu_compliance, output_status)
        if force_v_smu_port == 1:
            dv_input_compliance = fv_transfer_compliance[3]
        elif force_v_smu_port == 2:
            dv_input_compliance = fv_transfer_compliance[6]
        else:
            dv_input_compliance = force_v_smu_compliance

        # check range
        dv_input_range = self.check_voltage_range(force_v_smu_range, force_v_smu_module)

        # force V
        # Setting the Source Output Mode
        self.iprintf(":SOUR" + str(force_v_smu_port) + ":FUNC:MODE VOLT")
        # Setting the Output Range
        if force_v_smu_range == 0:
            self.iprintf(":SOUR" + str(force_v_smu_port) + ":VOLT:RANG:AUTO ON")
        else:
            self.iprintf(":SOUR" + str(force_v_smu_port) + ":VOLT:RANG " + str(dv_input_range))
        # Setting the Limit/Compliance Value
        self.iprintf(":SENS" + str(force_v_smu_port) + ":CURR:PROT " + str(dv_input_compliance))
        # Applying the DC Voltage
        self.iprintf(":SOUR" + str(force_v_smu_port) + ":VOLT " + str(force_v_smu_voltage))
        # Enabling the Source Output
        self.iprintf(":OUTP" + str(force_v_smu_port) + " ON")

    @_error_decorator
    def force_i_smu_2900(self, force_i_smu_port, force_i_smu_module, force_i_smu_current, force_i_smu_range,
                         force_i_smu_compliance):
        """
        This Subprogram conduct Voltage on the SMU or Pin
        self.check_smu_input_module: check input module
        self.check_current_range:    check and transfer range from itis check2900_current_mrange.

        :param force_i_smu_port: input port: 1 or 2
        :param force_i_smu_module: input port with module; 0: SMU
        :param force_i_smu_current: force voltage
        :param force_i_smu_range: force range
        :param force_i_smu_compliance: I compliance
        :return:
        """
        # check input module
        fi_smu_module = self.check_smu_input_module(force_i_smu_module)

        # check range
        fi_input_range = self.check_current_range(force_i_smu_range, fi_smu_module)

        # check and transfer compliance
        fi_input_compliance = force_i_smu_compliance
        if force_i_smu_compliance == 0:
            fi_input_compliance = 6
            if force_i_smu_current <= 1.515:
                fi_input_compliance = 21
            if force_i_smu_current <= 0.105:
                fi_input_compliance = 210

        # force i
        # Setting the Source Output Mode
        self.iprintf(":SOUR" + str(force_i_smu_port) + ":FUNC:MODE CURR")
        # Setting the Output Range
        if force_i_smu_range == 0:
            # AUTO range
            self.iprintf(":SOUR" + str(force_i_smu_port) + ":CURR:RANG:AUTO ON")
        else:
            # fixed range
            self.iprintf(":SOUR" + str(force_i_smu_port) + ":CURR:RANG " + str(fi_input_range))
        # Setting the Limit/Compliance Value
        self.iprintf(":SENS" + str(force_i_smu_port) + ":VOLT:PROT " + str(fi_input_compliance))
        # Applying the DC Voltage
        self.iprintf(":SOUR" + str(force_i_smu_port) + ":CURR " + str(force_i_smu_current))
        # Enabling the Source Output
        self.iprintf(":OUTP" + str(force_i_smu_port) + " ON")

    def measure_i_smu_2900(self, measure_i_smu_port, measure_i_smu_module, measure_i_smu_range):
        """
        This Subprogram conduct measure the current on the SMU or Pin
        self.check_smu_input_module:        check input module
        self.check_current_range:           check and transfer range from itis check2900_current_mrange
                +----------------------------------------+
        '       | VOLT | CURR | RES | TIME | STAT | SOUR |
        '       +----------------------------------------+
        'Index      0      1     2      3      4     5
        :param measure_i_smu_port: Measurement Port
        :param measure_i_smu_module: input port with module; 0: SMU
        :param measure_i_smu_range: Range
        :return: Current ,  measure_i_status
        """
        measure_i_smu_module = self.check_smu_input_module(measure_i_smu_module)
        # Check and transfer the range
        mi_transfer_range = self.check_current_range(measure_i_smu_range, measure_i_smu_module)

        # Peform measurement now
        # Sets measurement data output format
        self.iprintf(":FORM:DATA ASC")
        self.check_error_code()
        # Sets SMU measurement operation mode
        self.iprintf(":SENS" + str(measure_i_smu_port) + ":FUNC \"CURR\"")
        self.check_error_code()
        # Sets current measurement range
        if mi_transfer_range == 0:
            self.iprintf(":SENS" + str(measure_i_smu_port) + ":CURR:RANG:AUTO ON")
            self.check_error_code()
        else:
            self.iprintf(":SENS" + str(measure_i_smu_port) + ":CURR:RANG:AUTO OFF")
            self.check_error_code()
            self.iprintf(":SENS" + str(measure_i_smu_port) + ":CURR:RANG " + str(mi_transfer_range))
            self.check_error_code()
        self.iprintf(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")
        self.check_error_code()
        return_val_array = self.ipromptf(":MEAS? (@" + str(measure_i_smu_port) + ")")
        # only read current
        # return_val_array = self.ipromptf(":MEAS:CURR?")
        self.check_error_code()

        # trim data
        return_val_array = return_val_array.split(',')
        return_val = return_val_array[1]
        measure_current = '%e' % float(return_val)

        # trim error code(status)
        # measure_i_status = self.check_measure_status(return_val)
        measure_i_status = return_val_array[4]

        # check SMU error code
        self.check_error_code()

        return self.INST_ADDRESS, measure_current, measure_i_status

    def measure_v_smu_2900(self, measure_v_smu_port, measure_v_smu_module, measure_v_smu_range):
        """
        This Subprogram conduct measure the voltage on the SMU or Pin
        self.check_smu_input_module:        check input module
        self.check_voltage_range:           check and transfer range from itis Check2900_voltage_mrange
        self.check_measure_status:          trim error code(status)
        time.sleep(0.1):                    for multiple.Time may be shortened again.
        05/16
                +----------------------------------------+
        '       | VOLT | CURR | RES | TIME | STAT | SOUR |
        '       +----------------------------------------+
        'Index      0      1     2      3      4     5
        :param measure_v_smu_port: Measurement Port
        :param measure_v_smu_module: input port with module;
        0: MPSMU, 1: HRSMU, 2: HPSMU, 3: MFCMU, 4: HVSPGU, 5: WGFMU, 6: MCSMU, 7: HCSMU, 8: HVSMU,
        9: HRSMU and MPSMU with ASU, 10: SCUU
        :param measure_v_smu_range: Range
        :return: Current ,  measure_i_status
        """
        measure_v_smu_module = self.check_smu_input_module(measure_v_smu_module)
        # Check and transfer the range
        mv_transfer_range = self.check_voltage_range(measure_v_smu_range, measure_v_smu_module)

        # Peform measurement now
        # Sets measurement data output format
        self.iprintf(":FORM:DATA ASC")
        # Sets SMU measurement operation mode
        self.iprintf(":SENS" + str(measure_v_smu_port) + ":FUNC \"VOLT\"")
        # Sets current measurement range
        if mv_transfer_range == 0:
            self.iprintf(":SENS" + str(measure_v_smu_port) + ":VOLT:RANG:AUTO ON")
        else:
            self.iprintf(":SENS" + str(measure_v_smu_port) + ":VOLT:RANG:AUTO OFF")
            self.iprintf(":SENS" + str(measure_v_smu_port) + ":VOLT:RANG " + str(mv_transfer_range))
        self.iprintf(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")
        return_val_array = self.ipromptf(":MEAS? (@" + str(measure_v_smu_port) + ")")
        # only read current
        # return_val_array = self.ipromptf(":MEAS:CURR?")

        # trim data
        return_val_array = return_val_array.split(',')
        return_val = return_val_array[0]
        measure_voltage = '%e' % float(return_val)

        # trim error code(status)
        # measure_i_status = self.check_measure_status(return_val)
        measure_v_status = return_val_array[4]

        # check SMU error code
        self.check_error_code()
        self.check_error_code_debug()

        return self.INST_ADDRESS, measure_voltage, measure_v_status
    # SMU spot

    @_error_decorator
    def set_smu_ch(self, set_smu_ch_port, set_smu_ch_adc=0, set_smu_ch_filter=0):
        """
        This Subprogram conduct ADC converter to using for SMU and Filter on/off.
        But 2900 only one type of ADC, thus don't need to select adc mode for SMU.

        :param set_smu_ch_port: input port
        :param set_smu_ch_adc: ADC Type; 0:High speed A/D converter; 1:High Resolution A/D converter
        :param set_smu_ch_filter: Filter switch; 0:Off; 1:ON
        :return:
        """
        self.iprintf(f":OUTP{set_smu_ch_port}:FILT:LPAS:STAT {set_smu_ch_filter}")

    @_error_decorator
    def set_adc(self, adc=0, integ_mode=0, integ_value=None, auto_zero=0):
        """The subprogram sets the operation parameters of analog-to-digital converter
            (high-speed A/D converter or high-resolution A/D converter) that SMUs use
            for voltage and current measurements.

            Because there is no different type of adc in b2900, argument:adc, integ_mode, auto_zero are useless.
        :param adc: 0
        :param integ_mode: 0
        :param integ_value: integration time, +8E-6 to +2 seconds
        :param auto_zero: 0
        :return:
        """
        if integ_value is not None:
            self.iprintf(f":SENS1:VOLT:DC:APER {integ_value}")
            self.iprintf(f":SENS1:CURR:DC:APER {integ_value}")
            self.iprintf(f":SENS2:VOLT:DC:APER {integ_value}")
            self.iprintf(f":SENS2:CURR:DC:APER {integ_value}")
        else:
            self.iprintf(f":SENS1:VOLT:DC:APER:AUTO 1")
            self.iprintf(f":SENS1:CURR:DC:APER:AUTO 1")
            self.iprintf(f":SENS2:VOLT:DC:APER:AUTO 1")
            self.iprintf(f":SENS2:CURR:DC:APER:AUTO 1")

    # SMU sweep
    @_error_decorator
    def set_iv(self, set_iv_port, set_iv_smu_module, set_iv_sweep_mode, set_iv_range, set_iv_start_v,
                    set_iv_stop_v, set_iv_step, set_iv_hold_t, set_iv_delay_t, set_iv_compliance,
                    set_iv_power_compliance, set_iv_stop_mode, output_limited_range):
        """
        This Subprogram conduct setup parameters for staircase sweep measurement.
        self.check_setiv_sweepmode:                  check sweep mode.
        self.check_fv_calc_current_compliance:       check and transfer compliance..
        self.check_setiv_other:                      check step、hold time、delay time and stop mode.

        fv_transfer_compliance  [0: input compliance,
                                 1: channel1:on/off mode,
                                 2: channel1: input_voltage,
                                 3: channel1:transfer compliance,
                                 4: channel2:on/off mode,
                                 5: channel2: input_voltage,
                                 6: channel2:transfer compliance
                                 ]
        :param set_iv_port:             input port (101,201,301...by slot)
        :param set_iv_smu_module:       input port with module; 0: SMU
        :param set_iv_sweep_mode:       input sweep mode; V mode: 1,3,-1,-3. I mode: 2, 4, -2, -4
        :param set_iv_range:            output range
        :param set_iv_start_v:          start value
        :param set_iv_stop_v:           stop value
        :param set_iv_step:             點數
        :param set_iv_hold_t:           起始點停頓ms
        :param set_iv_delay_t:          每個點的停頓ms
        :param set_iv_compliance:       compliance
        :param set_iv_power_compliance: power_compliance
        :param set_iv_stop_mode:        stop_mode
        :param output_limited_range: 0/1, 0: Not use limited range, 1: use lower limited range
        :return:
        """
        # check sweep mode
        set_iv_sweep_mode = self.check_setiv_sweepmode(set_iv_sweep_mode)

        # Calculate compliance value if set by default
        dv_input_compliance = set_iv_compliance
        if set_iv_compliance == 0:
            if set_iv_sweep_mode % 2 == 1:
                output_status = 0
                if set_iv_compliance == 0:
                    output1_onoff_status = self.ipromptf("OUTP1:STAT?")
                    output2_onoff_status = self.ipromptf("OUTP2:STAT?")
                    output_status = int(output1_onoff_status) + int(output2_onoff_status)
                fv_transfer_compliance = self.check_fv_calc_current_compliance(set_iv_port, set_iv_stop_v,
                                                                               set_iv_compliance, output_status)
                if set_iv_port == 1:
                    dv_input_compliance = fv_transfer_compliance[3]
                if set_iv_port == 2:
                    dv_input_compliance = fv_transfer_compliance[6]
            else:
                dv_input_compliance = 6
                if set_iv_stop_v < 1.515:
                    dv_input_compliance = 21
                if set_iv_stop_v < 0.105:
                    dv_input_compliance = 210
        self.ivmode2900 = 1
        # # check Start、Stop and watts Voltage
        # set_iv_start_v, set_iv_stop_v = self.check_setiv_voltage(set_iv_smu_module, set_iv_start_v, set_iv_stop_v,
        #                                                          set_iv_compliance)
        # check step、hold time、delay time and stop mode
        self.check_setiv_other(set_iv_step, set_iv_hold_t, set_iv_delay_t, set_iv_power_compliance, set_iv_stop_mode)

        # command
        # set hold and delay time
        self.iprintf(":ARM" + str(set_iv_port) + ":TRAN:DEL 0")
        self.iprintf(":ARM" + str(set_iv_port) + ":ACQ:DEL " + str(set_iv_hold_t))
        self.iprintf(":TRIG" + str(set_iv_port) + ":TRAN:DEL 0")
        self.iprintf(":TRIG" + str(set_iv_port) + ":ACQ:DEL " + str(set_iv_delay_t))

        # check range
        set_iv_voltage_range = self.check_voltage_range(set_iv_range, set_iv_smu_module)
        set_iv_current_range = self.check_current_range(set_iv_range, set_iv_smu_module)
        # set iv
        setiv_sweepmode_dict = {
            1: ["VOLT", "LIN", "SING", "CURR"],
            2: ["CURR", "LIN", "SING", "VOLT"],
            3: ["VOLT", "LIN", "DOUB", "CURR"],
            4: ["CURR", "LIN", "DOUB", "VOLT"],
            -1: ["VOLT", "LOG", "SING", "CURR"],
            -2: ["CURR", "LOG", "SING", "VOLT"],
            -3: ["VOLT", "LOG", "DOUB", "CURR"],
            -4: ["CURR", "LOG", "DOUB", "VOLT"]
        }
        self.iprintf(":SOUR" + str(set_iv_port) + ":FUNC:MODE " + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]))
        self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":MODE SWE")
        self.iprintf(":SOUR" + str(set_iv_port) + ":SWE:SPAC " + str(setiv_sweepmode_dict[set_iv_sweep_mode][1]))
        self.iprintf(":SOUR" + str(set_iv_port) + ":SWE:STA " + str(setiv_sweepmode_dict[set_iv_sweep_mode][2]))
        if set_iv_sweep_mode % 2 == 1:
            pattern = ":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0])
            if set_iv_voltage_range == 0:  # Auto range mode
                self.iprintf(pattern + ":RANG:AUTO 1")
            else:
                if output_limited_range == 0:  # Fixed range mode
                    self.iprintf(pattern + ":RANG " + str(set_iv_voltage_range))
                elif output_limited_range == 1:  # Auto lower limited range mode
                    self.iprintf(pattern + ":RANG:AUTO:LLIM " + str(set_iv_voltage_range))
        else:
            pattern = ":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0])
            if set_iv_voltage_range == 0:  # Auto range mode
                self.iprintf(pattern + ":RANG:AUTO 1")
            else:
                if output_limited_range == 0:  # Fixed range mode
                    self.iprintf(pattern + ":RANG " + str(set_iv_current_range))
                elif output_limited_range == 1:  # Auto lower limited range mode
                    self.iprintf(pattern + ":RANG:AUTO:LLIM " + str(set_iv_current_range))

        self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":STAR " +
                     str(set_iv_start_v))
        self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":STOP " +
                     str(set_iv_stop_v))
        self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":POIN " +
                     str(set_iv_step))
        self.iprintf(":SENS" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][3]) + ":PROT " +
                     str(dv_input_compliance))
        self.iprintf(":TRIG" + str(set_iv_port) + ":ALL:SOUR AINT")
        self.iprintf(":TRIG" + str(set_iv_port) + ":ALL:COUN " + str(set_iv_step))

    @_error_decorator
    def set_piv(self, puls_port, set_iv_smu_module, puls_sweep_mode, output_range, puls_base, puls_start, puls_stop, num_step, puls_width, puls_per,
                hold_t, delay_t, compliance, stop_mode, output_limited_range):
        """
        This Subprogram conduct setup parameters for pulse sweep measurement.
        self.check_setiv_sweepmode:                  check sweep mode.
        self.check_fv_calc_current_compliance:       check and transfer compliance..
        self.check_setiv_other:                      check step、hold time、delay time and stop mode.

        fv_transfer_compliance  [0: input compliance,
                                 1: channel1:on/off mode,
                                 2: channel1: input_voltage,
                                 3: channel1:transfer compliance,
                                 4: channel2:on/off mode,
                                 5: channel2: input_voltage,
                                 6: channel2:transfer compliance
                                 ]
        :param puls_port:             input port (101,201,301...by slot)
        :param set_iv_smu_module:       input port with module; 0: SMU
        :param puls_sweep_mode:       input sweep mode; V mode: 1,3,-1,-3. I mode: 2, 4, -2, -4
        :param output_range:            output range
        :param puls_start:          start value
        :param puls_stop:           stop value
        :param num_step:             number of steps
        :param hold_t:           hold time
        :param delay_t:          measure delay
        :param compliance:       compliance
        :param stop_mode:        stop_mode
        :param output_limited_range: 0/1, 0: Not use limited range, 1: use lower limited range
        :return:
        """
        # check sweep mode
        puls_sweep_mode = self.check_setiv_sweepmode(puls_sweep_mode)

        # Calculate compliance value if set by default
        dv_input_compliance = compliance
        if compliance == 0:
            if puls_sweep_mode % 2 == 1:
                output_status = 0
                if compliance == 0:
                    output1_onoff_status = self.ipromptf("OUTP1:STAT?")
                    output2_onoff_status = self.ipromptf("OUTP2:STAT?")
                    output_status = int(output1_onoff_status) + int(output2_onoff_status)
                fv_transfer_compliance = self.check_fv_calc_current_compliance(puls_port, puls_stop,
                                                                               compliance, output_status)
                if puls_port == 1:
                    dv_input_compliance = fv_transfer_compliance[3]
                if puls_port == 2:
                    dv_input_compliance = fv_transfer_compliance[6]
            else:
                dv_input_compliance = 6
                if puls_stop < 1.515:
                    dv_input_compliance = 21
                if puls_stop < 0.105:
                    dv_input_compliance = 210
        self.ivmode2900 = 1

        # self.check_setiv_other(num_step, hold_t, set_iv_delay_t, set_iv_power_compliance, stop_mode)

        # command
        # set hold and delay time
        self.iprintf(":ARM" + str(puls_port) + ":TRAN:DEL 0")
        self.iprintf(":ARM" + str(puls_port) + ":ACQ:DEL " + str(hold_t))
        self.iprintf(":TRIG" + str(puls_port) + ":TRAN:DEL 0")
        self.set_trigger_subsystem_settings(layer='TRIG', action='ACQ', channel=puls_port, delay=delay_t)
        # self.iprintf(":TRIG" + str(puls_port) + ":ACQ:DEL " + str(delay_t))

        # check range
        set_iv_voltage_range = self.check_voltage_range(output_range, set_iv_smu_module)
        set_iv_current_range = self.check_current_range(output_range, set_iv_smu_module)
        # set iv
        setiv_sweepmode_dict = {
            1: ["VOLT", "LIN", "SING", "CURR"],
            2: ["CURR", "LIN", "SING", "VOLT"],
            3: ["VOLT", "LIN", "DOUB", "CURR"],
            4: ["CURR", "LIN", "DOUB", "VOLT"],
            -1: ["VOLT", "LOG", "SING", "CURR"],
            -2: ["CURR", "LOG", "SING", "VOLT"],
            -3: ["VOLT", "LOG", "DOUB", "CURR"],
            -4: ["CURR", "LOG", "DOUB", "VOLT"]
        }

        # set pulse mode
        self.set_pulse_mode(puls_on=True, mode=setiv_sweepmode_dict[puls_sweep_mode][0], channel=puls_port,
                            base=puls_base, puls_width=puls_width)
        # set trigger period
        self.set_trigger_subsystem_settings(layer='TRIG', action='ALL', channel=puls_port, timer=puls_per)

        self.iprintf(":SOUR" + str(puls_port) + ":FUNC:MODE " + str(setiv_sweepmode_dict[puls_sweep_mode][0]))
        self.iprintf(":SOUR" + str(puls_port) + ":" + str(setiv_sweepmode_dict[puls_sweep_mode][0]) + ":MODE SWE")
        self.iprintf(":SOUR" + str(puls_port) + ":SWE:SPAC " + str(setiv_sweepmode_dict[puls_sweep_mode][1]))
        self.iprintf(":SOUR" + str(puls_port) + ":SWE:STA " + str(setiv_sweepmode_dict[puls_sweep_mode][2]))
        if puls_sweep_mode % 2 == 1:
            pattern = ":SOUR" + str(puls_port) + ":" + str(setiv_sweepmode_dict[puls_sweep_mode][0])
            if set_iv_voltage_range == 0:
                self.iprintf(pattern + ":RANG:AUTO 1")
            else:
                if output_limited_range == 0:  # Fixed range mode
                    self.iprintf(pattern + ":RANG " + str(set_iv_voltage_range))
                elif output_limited_range == 1:  # Auto lower limited range mode
                    self.iprintf(pattern + ":RANG:AUTO:LLIM " + str(set_iv_voltage_range))
        else:
            pattern = ":SOUR" + str(puls_port) + ":" + str(setiv_sweepmode_dict[puls_sweep_mode][0])
            if set_iv_current_range == 0:
                self.iprintf(pattern + ":RANG:AUTO 1")
            else:
                if output_limited_range == 0:  # Fixed range mode
                    self.iprintf(pattern + ":RANG " + str(set_iv_current_range))
                elif output_limited_range == 1:  # Auto lower limited range mode
                    self.iprintf(pattern + ":RANG:AUTO:LLIM " + str(set_iv_current_range))

        self.iprintf(":SOUR" + str(puls_port) + ":" + str(setiv_sweepmode_dict[puls_sweep_mode][0]) + ":STAR " +
                     str(puls_start))
        self.iprintf(":SOUR" + str(puls_port) + ":" + str(setiv_sweepmode_dict[puls_sweep_mode][0]) + ":STOP " +
                     str(puls_stop))
        self.iprintf(":SOUR" + str(puls_port) + ":" + str(setiv_sweepmode_dict[puls_sweep_mode][0]) + ":POIN " +
                     str(num_step))
        self.iprintf(":SENS" + str(puls_port) + ":" + str(setiv_sweepmode_dict[puls_sweep_mode][3]) + ":PROT " +
                     str(dv_input_compliance))
        self.iprintf(":TRIG" + str(puls_port) + ":ALL:SOUR AINT")
        self.iprintf(":TRIG" + str(puls_port) + ":ALL:COUN " + str(num_step))

    def sweep_iv(self, sweep_iv_port, sweep_iv_module, sweep_iv_mode, sweep_iv_range, meas_limited_range):
        """
        This Subprogram conduct perform staircase sweep measurement.
        self.check_setiv_sweepmode:                  check and transfer iv mode.
        self.check_current_range:                    check current range.
        self.check_setiv_sweepmode:                  check voltage range.
                +----------------------------------------+
        '       | VOLT | CURR | RES | TIME | STAT | SOUR |
        '       +----------------------------------------+
        'Index      0      1     2      3      4     5
        :param sweep_iv_port: channel (SLOT1, SLOT2, ...SLOT10)
        :param sweep_iv_module: input port with module. 0: SMU
        :param sweep_iv_mode: 1: V mode; 2: I mode
        :param sweep_iv_range: Range
        :param meas_limited_range: 0/1/2, 0: Not use limited range, 1: use lower limited range, 2: use upper limited range
        :return: self.INST_ADDRESS, sweep_iv_outputdata, sweep_iv_source, sweep_iv_status
        """
        # transfer sweep iv mode
        sweep_iv_mode_transfer = self.check_sweepiv_transfer_ivmode(sweep_iv_mode)
        # # transfer MM command
        # sweep_iv_mode_mmcommand = self.check_sweepiv_mmcommand(self.ivmode2900, self.pbias2900)

        # command
        # Sets measurement data output format
        self.iprintf(":FORM:DATA ASC")
        self.check_error_code()
        self.iprintf(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")
        self.check_error_code()
        sweep_iv_transfer_range = 0
        mode_str = "CURR"
        if sweep_iv_mode_transfer == 1:
            # check and transfer range
            sweep_iv_transfer_range = self.check_current_range(sweep_iv_range, sweep_iv_module)
            mode_str = "CURR"
        if sweep_iv_mode_transfer == 2:
            # check and transfer range
            sweep_iv_transfer_range = self.check_voltage_range(sweep_iv_range, sweep_iv_module)
            mode_str = "VOLT"

        self.iprintf(":SENS" + str(sweep_iv_port) + ":FUNC \"" + mode_str + "\"")
        self.check_error_code()
        pattern = ":SENS" + str(sweep_iv_port) + ":" + mode_str
        if sweep_iv_transfer_range == 0:
            self.iprintf(pattern + ":RANG:AUTO ON")
        else:
            if meas_limited_range == 0:
                self.iprintf(pattern + ":RANG:AUTO OFF")
                self.iprintf(pattern + ":RANG " + str(sweep_iv_transfer_range))
            elif meas_limited_range == 1:
                self.iprintf(pattern + ":RANG:AUTO:LLIM " + str(sweep_iv_transfer_range))
            elif meas_limited_range == 2:
                self.iprintf(pattern + ":RANG:AUTO:ULIM " + str(sweep_iv_transfer_range))

        # Enabling the Source Output
        self.iprintf(":OUTP" + str(sweep_iv_port) + " ON")
        self.check_error_code()
        # Initiate transition and acquire
        self.iprintf(":INIT (@" + str(sweep_iv_port) + ")")
        self.check_error_code()
        return_val_array = []
        return_val_array.append(self.ipromptf_largebuff(":FETC:ARR:VOLT? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf_largebuff(":FETC:ARR:CURR? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf_largebuff(":FETC:ARR:RES? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf_largebuff(":FETC:ARR:TIME? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf_largebuff(":FETC:ARR:STAT? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf_largebuff(":FETC:ARR:SOUR? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()

        measure_point = self.ipromptf(":SYST:DATA:QUAN? (@" + str(sweep_iv_port) + ")")
        # check SMU error code
        self.check_error_code()

        # sweep_iv_outputdata = {}
        # sweep_iv_source = {}
        # sweep_iv_outputdata["sweep_iv_outputdata"] = 0
        # sweep_iv_source["sweep_iv_source"] = 0
        # sweep_iv_outputdata["sweep_iv_outputdata"] = return_val_array[1]
        sweep_iv_outputdata = return_val_array[1]
        sweep_iv_source = return_val_array[5].split(",")
        # sweep_iv_source["sweep_iv_source"] = return_val_array[5]
        if sweep_iv_mode_transfer == 1:
            sweep_iv_outputdata = return_val_array[1].split(",")
            sweep_iv_source_real = return_val_array[0].split(",")

        if sweep_iv_mode_transfer == 2:
            sweep_iv_outputdata = return_val_array[0].split(",")
            sweep_iv_source_real = return_val_array[1].split(",")

        # check SMU error code
        self.check_error_code()
        self.check_error_code_debug()

        sweep_iv_status = "sweep_iv_2900 => PASS"

        return self.INST_ADDRESS, sweep_iv_outputdata, sweep_iv_source, sweep_iv_source_real, sweep_iv_status
    # SMU sweep
    # Command

    def sweep_iv2(self, sweep_iv_port1, sweep_iv_module1, sweep_iv_mode1, sweep_iv_range1,
                  sweep_iv_port2, sweep_iv_module2, sweep_iv_mode2, sweep_iv_range2,
                  meas_limited_range1=0, meas_limited_range2=0):
        """
        This Subprogram conduct perform staircase sweep measurement for two channel simultaneously.
        self.check_setiv_sweepmode:                  check and transfer iv mode.
        self.check_current_range:                    check current range.
        self.check_setiv_sweepmode:                  check voltage range.
                +----------------------------------------+
        '       | VOLT | CURR | RES | TIME | STAT | SOUR |
        '       +----------------------------------------+
        'Index      0      1     2      3      4     5
        :param sweep_iv_port1: channel (SLOT1, SLOT2, ...SLOT10)
        :param sweep_iv_module1: input port with module. 0: SMU
        :param sweep_iv_mode1: 1: V mode; 2: I mode
        :param sweep_iv_range1: Range
        :param sweep_iv_port2: channel (SLOT1, SLOT2, ...SLOT10)
        :param sweep_iv_module2: input port with module. 0: SMU
        :param sweep_iv_mode2: 1: V mode; 2: I mode
        :param sweep_iv_range2: Range
        :param meas_limited_range1: 0/1/2, 0: Not use limited range, 1: use lower limited range, 2: use upper limited range
        :param meas_limited_range2: 0/1/2, 0: Not use limited range, 1: use lower limited range, 2: use upper limited range
        :return: self.INST_ADDRESS, sweep_iv_outputdata, sweep_iv_source, sweep_iv_status
        """
        # transfer sweep iv mode
        sweep_iv_mode_transfer1 = self.check_sweepiv_transfer_ivmode(sweep_iv_mode1)
        # # transfer MM command
        # sweep_iv_mode_mmcommand = self.check_sweepiv_mmcommand(self.ivmode2900, self.pbias2900)

        # command for channel 1
        # Sets measurement data output format
        self.iprintf(":FORM:DATA ASC")
        self.check_error_code()
        self.iprintf(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")
        self.check_error_code()
        sweep_iv_transfer_range = 0
        mode_str = "CURR"
        if sweep_iv_mode_transfer1 == 1:
            # check and transfer range
            sweep_iv_transfer_range = self.check_current_range(sweep_iv_range1, sweep_iv_module1)
            mode_str = "CURR"
        if sweep_iv_mode_transfer1 == 2:
            # check and transfer range
            sweep_iv_transfer_range = self.check_voltage_range(sweep_iv_range1, sweep_iv_module1)
            mode_str = "VOLT"

        self.iprintf(":SENS" + str(sweep_iv_port1) + ":FUNC \"" + mode_str + "\"")
        self.check_error_code()
        pattern = ":SENS" + str(sweep_iv_port1) + ":" + mode_str
        if sweep_iv_transfer_range == 0:
            self.iprintf(pattern + ":RANG:AUTO ON")
        else:
            if meas_limited_range1 == 0:
                self.iprintf(pattern + ":RANG:AUTO OFF")
                self.iprintf(pattern + ":RANG " + str(sweep_iv_transfer_range))
            elif meas_limited_range1 == 1:
                self.iprintf(pattern + ":RANG:AUTO:LLIM " + str(sweep_iv_transfer_range))
            elif meas_limited_range1 == 2:
                self.iprintf(pattern + ":RANG:AUTO:ULIM " + str(sweep_iv_transfer_range))

        # Enabling the Source Output
        self.iprintf(":OUTP" + str(sweep_iv_port1) + " ON")
        self.check_error_code()

        # transfer sweep iv mode
        sweep_iv_mode_transfer2 = self.check_sweepiv_transfer_ivmode(sweep_iv_mode2)
        # # transfer MM command
        # sweep_iv_mode_mmcommand = self.check_sweepiv_mmcommand(self.ivmode2900, self.pbias2900)

        # command for channel 2
        sweep_iv_transfer_range = 0
        mode_str = "CURR"
        if sweep_iv_mode_transfer2 == 1:
            # check and transfer range
            sweep_iv_transfer_range = self.check_current_range(sweep_iv_range2, sweep_iv_module2)
            mode_str = "CURR"
        if sweep_iv_mode_transfer2 == 2:
            # check and transfer range
            sweep_iv_transfer_range = self.check_voltage_range(sweep_iv_range2, sweep_iv_module2)
            mode_str = "VOLT"

        self.iprintf(":SENS" + str(sweep_iv_port2) + ":FUNC \"" + mode_str + "\"")
        self.check_error_code()
        pattern = ":SENS" + str(sweep_iv_port2) + ":" + mode_str
        if sweep_iv_transfer_range == 0:
            self.iprintf(pattern + ":RANG:AUTO ON")
        else:
            if meas_limited_range1 == 0:
                self.iprintf(pattern + ":RANG:AUTO OFF")
                self.iprintf(pattern + ":RANG " + str(sweep_iv_transfer_range))
            elif meas_limited_range1 == 1:
                self.iprintf(pattern + ":RANG:AUTO:LLIM " + str(sweep_iv_transfer_range))
            elif meas_limited_range1 == 2:
                self.iprintf(pattern + ":RANG:AUTO:ULIM " + str(sweep_iv_transfer_range))

        # Enabling the Source Output
        self.iprintf(":OUTP" + str(sweep_iv_port2) + " ON")
        self.check_error_code()


        # Initiate transition and acquire
        self.iprintf(f":INIT (@{sweep_iv_port1},{sweep_iv_port2})")
        self.check_error_code()
        return_val_array = []
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:VOLT? (@{sweep_iv_port1})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:CURR? (@{sweep_iv_port1})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:RES? (@{sweep_iv_port1})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:TIME? (@{sweep_iv_port1})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:STAT? (@{sweep_iv_port1})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:SOUR? (@{sweep_iv_port1})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:VOLT? (@{sweep_iv_port2})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:CURR? (@{sweep_iv_port2})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:RES? (@{sweep_iv_port2})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:TIME? (@{sweep_iv_port2})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:STAT? (@{sweep_iv_port2})"))
        return_val_array.append(self.ipromptf_largebuff(f":FETC:ARR:SOUR? (@{sweep_iv_port2})"))

        measure_point = self.ipromptf(f":SYST:DATA:QUAN? (@{sweep_iv_port1},{sweep_iv_port2})")
        # check SMU error code
        self.check_error_code()

        sweep_iv_source1 = return_val_array[5].split(",")
        sweep_iv_source2 = return_val_array[11].split(",")
        # sweep_iv_source["sweep_iv_source"] = return_val_array[5]
        if sweep_iv_mode_transfer1 == 1:
            sweep_iv_outputdata1 = return_val_array[1].split(",")
            sweep_iv_source1_real = return_val_array[0].split(",")

        if sweep_iv_mode_transfer1 == 2:
            sweep_iv_outputdata1 = return_val_array[0].split(",")
            sweep_iv_source1_real = return_val_array[1].split(",")

        if sweep_iv_mode_transfer2 == 1:
            sweep_iv_outputdata2 = return_val_array[7].split(",")
            sweep_iv_source2_real = return_val_array[6].split(",")

        if sweep_iv_mode_transfer2 == 2:
            sweep_iv_outputdata2 = return_val_array[6].split(",")
            sweep_iv_source2_real = return_val_array[7].split(",")

        # check SMU error code
        self.check_error_code()
        self.check_error_code_debug()

        sweep_iv_status = "sweep_iv_2900 => PASS"

        return self.INST_ADDRESS, [sweep_iv_outputdata1, sweep_iv_outputdata2], [sweep_iv_source1, sweep_iv_source2], \
               [sweep_iv_source1_real, sweep_iv_source2_real], sweep_iv_status

    @_error_decorator
    def set_trigger_subsystem_settings(self, layer='TRIG', action='TRAN', channel=1, bypass=None, count=None, delay=None, sour=None, timer=None,
                                       trg_out=None, trg_mode=None):
        """
        Set trigger subsystem settings, you should specify the function you want to use,
        layer you want to trigger, action you want to tirgger, and channel you want to initiate.
        Besides, you also need to specify the setting value of function you use.

        :param layer: ARM/TRIG/ALL
        :param action: ACQ/TRAN/ALL
        :param channel: 0/1/2, 0 means use channel 1,2
        :param bypass: True: ONCE, False: OFF
        :param count: 1 ~ 100000, 2147483647 indicates infinity
        :param delay: 0 ~ 100000 sec
        :param sour: EXT1~EXT14, AINT, BUS, TIMer, INT1, INT2
        :param timer: 1e-5 ~ 1e+5
        :param trg_out: EXT1 ~ EXT14, INT1, INT2
        :param trg_mode: ON/OFF
        :return:
        """
        layer, action = layer.upper(), action.upper()
        if bypass is not None:
            bypass = 'ONCE' if bypass else 'OFF'
        value = {"BYP": bypass,
                 "COUN": count,
                 "DEL": delay,
                 "SOUR": sour,
                 "TIM": timer,
                 "TOUT:SIGN": trg_out,
                 "TOUT:STAT": trg_mode}

        if layer == 'ALL':
            if channel > 0:
                for func in value:
                    if value[func] is not None:
                        self.iprintf(f":ARM{channel}:{action}:{func} {value[func]}")
                        self.iprintf(f":TRIG{channel}:{action}:{func} {value[func]}")
            elif channel == 0:
                for func in value:
                    if value[func] is not None:
                        self.iprintf(f":ARM1:{action}:{func} {value[func]}")
                        self.iprintf(f":ARM2:{action}:{func} {value[func]}")
                        self.iprintf(f":TRIG1:{action}:{func} {value[func]}")
                        self.iprintf(f":TRIG2:{action}:{func} {value[func]}")
        elif layer == 'ARM' or layer == 'TRIG':
            if channel > 0:
                for func in value:
                    if value[func] is not None:
                        self.iprintf(f":{layer}{channel}:{action}:{func} {value[func]}")
            elif channel == 0:
                for func in value:
                    if value[func] is not None:
                        self.iprintf(f":{layer}1:{action}:{func} {value[func]}")
                        self.iprintf(f":{layer}2:{action}:{func} {value[func]}")

    def trigger_subsystem_settings_query(self, func, layer="ALL"):
        """
        Query the settings of trigger subsystem, please speficy the function and layer you want to query.

        :param func: BYP/COUN/DEL/SOUR/TIM/TOUT:SIGN/TPUT:STAT
        :param layer: ALL/TRIG/ARM
        :return: dict
        """
        res = dict()
        layer = layer.upper()
        res['ARM1'], res['ARM2'] = dict(), dict()
        res['TRIG1'], res['TRIG2'] = dict(), dict()

        if layer == 'ARM' or layer == 'ALL':
            res['ARM1']['ACQ'] = self.ipromptf(f":ARM1:ACQ:{func}?")
            res['ARM2']['ACQ'] = self.ipromptf(f":ARM2:ACQ:{func}?")
            res['ARM1']['TRAN'] = self.ipromptf(f":ARM1:TRAN:{func}?")
            res['ARM2']['TRAN'] = self.ipromptf(f":ARM2:TRAN:{func}?")

        if layer == 'TRIG' or layer == 'ALL':
            res['TRIG1']['ACQ'] = self.ipromptf(f":TRIG1:ACQ:{func}?")
            res['TRIG2']['ACQ'] = self.ipromptf(f":TRIG2:ACQ:{func}?")
            res['TRIG1']['TRAN'] = self.ipromptf(f":TRIG1:TRAN:{func}?")
            res['TRIG2']['TRAN'] = self.ipromptf(f":TRIG2:TRAN:{func}?")

        return self.INST_ADDRESS, res

    @_error_decorator
    def send_imm_trigger(self, layer='ALL', action='ALL', channel=0):
        """
        Send immediate trigger for the specified device action to the specified channel.

        :param layer: ALL/ARM/TRIG
        :param action: ACQ/TRAN/ALL
        :param channel: 0/1/2, 0 means use channel 1,2
        :return:
        """
        layer, action = layer.upper(), action.upper()

        if layer == 'ALL':
            if channel > 0:
                self.iprintf(f":ARM:{action} (@{channel})")
                self.iprintf(f":TRIG:{action} (@{channel})")
            elif channel == 0:
                self.iprintf(f":ARM:{action} (@1,2)")
                self.iprintf(f":ARM:{action} (@1,2)")
                self.iprintf(f":TRIG:{action} (@1,2)")
                self.iprintf(f":TRIG:{action} (@1,2)")
        elif layer == 'ARM' or layer == 'TRIG':
            if channel > 0:
                self.iprintf(f":{layer}:{action} (@{channel})")
            elif channel == 0:
                self.iprintf(f":{layer}:{action} (@1,2)")
                self.iprintf(f":{layer}:{action} (@1,2)")

    @_error_decorator
    def initiates_trigger(self, channel=1, action='ALL'):
        """
        Initiates the specified device action for the specified channel. Trigger status is changed from idle to initiated.

        :param channel: 0/1/2, 0 means use channel 1,2
        :param action: ACQ/TRAN/ALL
        :return:
        """
        action = action.upper()
        if channel > 0:
            self.iprintf(f":INIT:IMM:{action} (@{channel})")
        elif channel == 0:
            self.iprintf(f":INIT:IMM:{action} (@1,2)")

    @_error_decorator
    def abort_trigger(self, channel=0, action='ALL'):
        """
        Aborts the specified device action for the specified channel. Trigger status is changed to idle.

        :param channel: 0/1/2, 0 means use channel 1,2
        :param action: ACQ/TRAN/ALL
        :return:
        """
        action = action.upper()
        if channel > 0:
            self.iprintf(f":ABOR:IMM:{action} (@{channel})")
        elif channel == 0:
            self.iprintf(f":ABOR:IMM:{action} (@1,2)")

    def device_action_status(self, channel=0):
        """
        Checks the status of the specified device action for the specified channel, and waits until the status is changed to idle.
        response returns 1 if the specified device action is in the idle state.

        :param channel: 0/1/2, 0 means use channel 1,2
        :return:
        """
        res = dict()
        res['CH1'], res['CH2'] = dict(), dict()
        if channel > 0:
            res[f"CH{channel}"]["ACQ"] = self.ipromptf(f":IDLE{channel}:ACQ?")
            res[f"CH{channel}"]["TRAN"] = self.ipromptf(f":IDLE{channel}:TRAN?")
        elif channel == 0:
            res[f"CH1"]["ACQ"] = self.ipromptf(f":IDLE1:ACQ?")
            res[f"CH1"]["TRAN"] = self.ipromptf(f":IDLE1:TRAN?")
            res[f"CH2"]["ACQ"] = self.ipromptf(f":IDLE2:ACQ?")
            res[f"CH2"]["TRAN"] = self.ipromptf(f":IDLE2:TRAN?")

        return self.INST_ADDRESS, res

    @_error_decorator
    def set_digital_io_settings(self, pin=1, func=None, polarity=None, timing=None, pul_width=None, trg_type=None):
        """
        Set digital io (EXT1 ~ EXT14) settings, including function, polarity, trigger timing, pulse width, trigger type.

        :param pin: 1 ~ 14
        :param func: DIO/DINP/HVOL/TINP/TOUT
        :param polarity: NEG/POS
        :param timing: BEFORE/AFTER/BOTH
        :param pul_width: default is 1e-4
        :param trg_type: EDGE/LEVEL
        :return:
        """
        if func:
            self.iprintf(f":SOUR:DIG:EXT{pin}:FUNC {func}")
        if polarity:
            self.iprintf(f":SOUR:DIG:EXT{pin}:POL {polarity}")
        if timing:
            self.iprintf(f":SOUR:DIG:EXT{pin}:TOUT:POS {timing}")
        if pul_width:
            self.iprintf(f":SOUR:DIG:EXT{pin}:TOUT:WIDT {pul_width}")
        if trg_type:
            self.iprintf(f":SOUR:DIG:EXT{pin}:TOUT:TYPE {trg_type}")

    def digital_io_settings_query(self, pin=1):
        """
        Read digital o setting from b2900

        :param pin: 1 ~ 14
        :return:
        """
        res = dict()
        res[f'EXT{pin}'] = dict()

        res[f'EXT{pin}']["function"] = self.ipromptf(f":SOUR:DIG:EXT{pin}:FUNC?")
        res[f'EXT{pin}']["polarity"] = self.ipromptf(f":SOUR:DIG:EXT{pin}:POL?")
        res[f'EXT{pin}']["timing"] = self.ipromptf(f":SOUR:DIG:EXT{pin}:TOUT:POS?")
        res[f'EXT{pin}']["width"] = self.ipromptf(f":SOUR:DIG:EXT{pin}:TOUT:WIDT?")
        res[f'EXT{pin}']["type"] = self.ipromptf(f":SOUR:DIG:EXT{pin}:TOUT:TYPE?")

        return self.INST_ADDRESS, res

    @_error_decorator
    def set_action_layer_trigger_output(self, pin=1, stat=None, action=None, channel=1):
        """
        Set the trigger output for the status change between the trigger layer and the transient/acquire device action.

        :param pin: 1 ~ 14
        :param stat: ON/OFF
        :param action: ALL/TRAN/ACQ
        :param channel: 0/1/2, 0 means use channel 1,2
        :return:
        """
        if not action:
            return

        action = action.upper()
        if action == "ACQ" or action == "ALL":
            if channel > 0:
                self.iprintf(f":SENS{channel}:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iprintf(f":SENS{channel}:TOUT:STAT {stat}")
            elif channel == 0:
                self.iprintf(f":SENS1:TOUT:SIGN EXT{pin}")
                self.iprintf(f":SENS2:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iprintf(f":SENS1:TOUT:STAT {stat}")
                    self.iprintf(f":SENS2:TOUT:STAT {stat}")

        if action == "TRAN" or action == "ALL":
            if channel > 0:
                self.iprintf(f":SOUR{channel}:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iprintf(f":SOUR{channel}:TOUT:STAT {stat}")
            elif channel == 0:
                self.iprintf(f":SOUR1:TOUT:SIGN EXT{pin}")
                self.iprintf(f":SOUR2:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iprintf(f":SOUR1:TOUT:STAT {stat}")
                    self.iprintf(f":SOUR2:TOUT:STAT {stat}")

    def action_layer_trigger_output_query(self):
        """
        Query the settings for action layer trigger output.

        :return:
        """
        res = dict()
        res['CH1'], res['CH2'] = dict(), dict()

        res['CH1']['sour.output'] = self.ipromptf(":SOUR1:TOUT:SIGN?")
        res['CH2']['sour.output'] = self.ipromptf(":SOUR2:TOUT:SIGN?")
        res['CH1']['sour.stat'] = self.ipromptf(":SOUR1:TOUT:STAT?")
        res['CH2']['sour.stat'] = self.ipromptf(":SOUR2:TOUT:STAT?")
        res['CH1']['sens.output'] = self.ipromptf(":SENS1:TOUT:SIGN?")
        res['CH2']['sens.output'] = self.ipromptf(":SENS2:TOUT:SIGN?")
        res['CH1']['sens.stat'] = self.ipromptf(":SENS1:TOUT:STAT?")
        res['CH2']['sens.stat'] = self.ipromptf(":SENS2:TOUT:STAT?")

        return self.INST_ADDRESS, res

    @_error_decorator
    def set_pulse_mode(self, puls_on=True, mode="CURR", channel=1, peak=0, base=0, puls_width=5E-5, puls_del=0, comp=None):
        """Set the pulse mode settings, including pulse mode, pulse width and pulse delay

        :param puls_on: True/False
        :param mode: CURR/VOLT
        :param channel: 0/1/2, 0 means use channel 1,2
        :param peak : peak value of pulse
        :param base : base force voltage/current value
        :param puls_width: 5E-5 to 100000 seconds
        :param puls_del: 0.0 to 99999.9, in seconds
        :param comp: compliance value of current or voltage
        :return:
        """
        if channel > 0:
            if puls_on:
                self.iprintf(f":SOUR{channel}:FUNC:SHAP PULS")
                self.iprintf(f":SOUR{channel}:{mode}:TRIG {peak}")
                self.iprintf(f":SOUR{channel}:{mode} {base}")
                if comp:
                    if mode == "CURR":
                        self.iprintf(f":SENS{channel}:VOLT:PROT {comp}")
                    elif mode == "VOLT":
                        self.iprintf(f":SENS{channel}:CURR:PROT {comp}")
            else:
                self.iprintf(f":SOUR{channel}:FUNC:SHAP DC")
            self.iprintf(f":SOUR{channel}:FUNC:MODE {mode}")
            self.iprintf(f":SOUR{channel}:PULS:DEL {puls_del}")
            self.iprintf(f":SOUR{channel}:PULS:WIDT {puls_width}")
        elif channel == 0:
            if puls_on:
                self.iprintf(":SOUR1:FUNC:SHAP PULS")
                self.iprintf(f":SOUR1:{mode}:TRIG {peak}")
                self.iprintf(f":SOUR1:{mode} {base}")
                self.iprintf(":SOUR2:FUNC:SHAP PULS")
                self.iprintf(f":SOUR2:{mode}:TRIG {peak}")
                self.iprintf(f":SOUR2:{mode} {base}")
                if comp:
                    if mode == "CURR":
                        self.iprintf(f":SENS1:VOLT:PROT {comp}")
                        self.iprintf(f":SENS2:VOLT:PROT {comp}")
                    elif mode == "VOLT":
                        self.iprintf(f":SENS1:CURR:PROT {comp}")
                        self.iprintf(f":SENS2:CURR:PROT {comp}")
            else:
                self.iprintf(":SOUR1:FUNC:SHAP DC")
                self.iprintf(":SOUR2:FUNC:SHAP DC")
            self.iprintf(f":SOUR1:FUNC:MODE {mode}")
            self.iprintf(f":SOUR1:PULS:DEL {puls_del}")
            self.iprintf(f":SOUR1:PULS:WIDT {puls_width}")
            self.iprintf(f":SOUR2:FUNC:MODE {mode}")
            self.iprintf(f":SOUR2:PULS:DEL {puls_del}")
            self.iprintf(f":SOUR2:PULS:WIDT {puls_width}")

    def pulse_mode_query(self):
        """query pulse settings

        :return:
        """
        res = dict()
        res['CH1'], res['CH2'] = dict(), dict()

        res['CH1']['shape'] = self.ipromptf(":SOUR1:FUNC:SHAP?")
        res['CH2']['shape'] = self.ipromptf(":SOUR2:FUNC:SHAP?")
        res['CH1']['mode'] = self.ipromptf(":SOUR1:FUNC:MODE?")
        res['CH2']['mode'] = self.ipromptf(":SOUR2:FUNC:MODE?")
        res['CH1']['delay'] = self.ipromptf(":SOUR1:PULS:DEL?")
        res['CH2']['delay'] = self.ipromptf(":SOUR2:PULS:DEL?")
        res['CH1']['width'] = self.ipromptf(":SOUR1:PULS:WIDT?")
        res['CH2']['width'] = self.ipromptf(":SOUR2:PULS:WIDT?")

        return self.INST_ADDRESS, res


class OfflineB2900(OnlineB2900):
    """ Online 2900 """

    # offline parameter
    ivmode2900 = 1
    # if return 9527, parameter no setting
    # setting in force v
    offline_forcev_smu_voltage = 9527.0
    # setting in force i
    offline_forcei_smu_current = 9527.0
    # setting in set_iv
    offline_setiv_step = 11

    def __init__(self, inst_addr, newline='\n'):
        """
        "ERR?" => clean memory
        "CN"   => connect SMU output all : DebugTool and test case can used.
        :param inst_addr:
        :param newline:
        """
        # self.inst = sicl.SICL(inst_addr, newline)
        self.INST_ADDRESS = inst_addr
        # self.ipromptf("ERR?")
        # self.iprintf("CN")
        # for select case in sweep iv from set_iv

    # GPIB read / write
    def iprintf(self, command):
        # return self.inst.iprintf(command)
        # self.inst.iprintf(command)
        # self.check_error_code_debug()
        return "Offline_iprintf"

    def ipromptf(self, command):
        # result = self.inst.ipromptf(command)
        # # self.check_error_code_debug()
        # return result.replace("\n", "")
        return "Offline_ipromptf"

    def ipromptf_largebuff(self, command):
        # result = self.inst.ipromptf_largebuff(command)
        # # self.check_error_code_debug()
        # return result.replace("\n", "")
        return "Offline_ipromptf_largebuff"

    def iread(self):
        # result = self.inst.iread_byte()
        # self.check_error_code_debug()
        # return result.replace("\n", "")
        return "Offline_iread"

    def log_level(self, level):
        self.logger.setLevel(level)

    # GPIB read / write

    # check
    def check_error_code(self):
        """"
            This Subprogram check ERROR code.
            for B2900, Send "ERRX?" can clear B2900 error buffer.
        :return: No return
        """
        # error_message = self.ipromptf(":SYST:ERR:ALL?")
        error_message = self.ipromptf(":SYST:ERR?")
        error_message_output = error_message.rstrip().split(",")
        # error_message_output = []
        # error_message_output.append("+0")
        if error_message_output[0] != "Offline_ipromptf":
            self.logger.warning(
                "*** ERROR : B2900 Instrument Error : "
                " ADDRESS : {}".format(error_message_output, self.INST_ADDRESS))
            raise TISError("*** ERROR : B2900 Instrument Error : "
                           " ADDRESS : {}".format(error_message_output, self.INST_ADDRESS))

        # return self.INST_ADDRESS

    def check_error_code_debug(self):
        """"
            This Subprogram check ERROR code in send any command
            for B2900,send ERRX? Can clear B2900 error buffer.
            When debug,change debug_mode = 1.
            In release,change debug_mode = 0.
        :return: No return
        """
        # pass
        if TIS_driver_debug_mode:
            error_message = self.ipromptf(":SYST:ERR?")
            error_message_output = error_message.rstrip().split(",")
            if error_message_output[0] != "Offline_ipromptf":
                self.logger.warning(
                    "*** ERROR : B2900 Instrument Error : "
                    " ADDRESS : {}".format(error_message_output, self.INST_ADDRESS))
                raise TISError("*** ERROR : B2900 Instrument Error : "
                               " ADDRESS : {}".format(error_message_output, self.INST_ADDRESS))
            else:
                return
        else:
            return

    def check_smu_input_module(self, smu_input_module):
        """"
            This Subprogram check ERROR code in send any command
            follow b1500 structure.
            When debug,change debug_mode = 1.
            In release,change debug_mode = 0.
            Using in:
            1. force_v_smu_2900
            2. force_i_smu_2900
            3. measure_i_smu_2900
            4. force_v_cmu_2900

        :param smu_input_module: input port with module;
        0: MPSMU, 1: HRSMU, 2: HPSMU, 3: MFCMU, 4: HVSPGU, 5: WGFMU, 6: MCSMU, 7: HCSMU, 8: HVSMU,
        9: HRSMU and MPSMU with ASU, 10: SCUU
        :return: No return
        """
        if 0 <= smu_input_module <= 10:
            smu_output_module = smu_input_module
        else:
            self.logger.warning(
                "*** ERROR : Incorrect Input SMU Module is not correct."
                " ADDRESS : {}".format(smu_input_module, self.INST_ADDRESS))
            raise TISError("*** ERROR : Incorrect Input SMU Module is not correct."
                           " ADDRESS : {}".format(smu_input_module, self.INST_ADDRESS))
        return smu_output_module

    def check_fv_calc_current_compliance(self, channel, dv_input_voltage, force_v_smu_compliance, output_status):
        """"
            This Subprogram check Force_v Current compliance. follow VSPEC.
            calc_curr_comp_list [0: input compliance,
                                 1: channel1:on/off mode,
                                 2: channel1: input_voltage,
                                 3: channel1:transfer compliance,
                                 4: channel2:on/off mode,
                                 5: channel2: input_voltage,
                                 6: channel2:transfer compliance]
            Using in:
            1. force_v_smu_2900

        :param channel: input voltage,from myself command setting.
        :param dv_input_voltage: input compliance value.
        :param force_v_smu_compliance: input compliance value.
        :param output_status: Output on/off status.
        :return: B2900_calc_curr_comp_list
        """
        calc_curr_comp_list = [0, 0, 0, 0, 0, 0, 0]
        calc_curr_comp_list[0] = force_v_smu_compliance

        if channel == 1:
            calc_curr_comp_list[2] = dv_input_voltage
            calc_curr_comp_list[3] = 0.105
            self.fv_channel1_voltage = dv_input_voltage
        elif channel == 2:
            calc_curr_comp_list[5] = dv_input_voltage
            calc_curr_comp_list[6] = 0.105
            self.fv_channel2_voltage = dv_input_voltage

        # if channel 1 using or channel 2 not using
        if output_status == 0:
            if dv_input_voltage <= 21:
                calc_curr_comp_list[3] = 1.515
                calc_curr_comp_list[6] = 1.515
            if dv_input_voltage <= 6:
                calc_curr_comp_list[3] = 3.03
                calc_curr_comp_list[6] = 3.03
        else:
            # if channel 1 or channel 2 using
            if 0 < self.fv_channel1_voltage <= 6:
                if 0 < self.fv_channel2_voltage <= 6:
                    if channel == 1:
                        calc_curr_comp_list[3] = 4 - int(calc_curr_comp_list[6])
                    elif channel == 2:
                        calc_curr_comp_list[6] = 4 - int(calc_curr_comp_list[3])
                elif 6 < self.fv_channel2_voltage <= 21:
                    if channel == 1:
                        calc_curr_comp_list[3] = 4 - (int(calc_curr_comp_list[6]) * 1.6)
                    elif channel == 2:
                        calc_curr_comp_list[6] = (4 - int(calc_curr_comp_list[3])) / 4
                elif 21 < self.fv_channel2_voltage <= 210:
                    if channel == 1:
                        calc_curr_comp_list[3] = 3.03
                    elif channel == 2:
                        calc_curr_comp_list[6] = 0.105
            elif 6 < self.fv_channel1_voltage <= 21:
                if 0 < self.fv_channel2_voltage <= 6:
                    if channel == 1:
                        calc_curr_comp_list[3] = 2.5 - (int(calc_curr_comp_list[6]) * 0.625)
                    elif channel == 2:
                        calc_curr_comp_list[6] = (2.5 - int(calc_curr_comp_list[3])) / 0.625
                elif 6 < self.fv_channel2_voltage <= 21:
                    if channel == 1:
                        calc_curr_comp_list[3] = 2.5 - int(calc_curr_comp_list[6])
                    elif channel == 2:
                        calc_curr_comp_list[6] = 2.5 - int(calc_curr_comp_list[3])
                elif 21 < self.fv_channel2_voltage <= 210:
                    if channel == 1:
                        calc_curr_comp_list[3] = 1.515
                    elif channel == 2:
                        calc_curr_comp_list[6] = 0.105
            elif 21 < self.fv_channel1_voltage <= 210:
                if 0 < self.fv_channel2_voltage <= 6:
                    if channel == 1:
                        calc_curr_comp_list[3] = 0.105
                    elif channel == 2:
                        calc_curr_comp_list[6] = 3.03
                elif 6 < self.fv_channel2_voltage <= 21:
                    if channel == 1:
                        calc_curr_comp_list[3] = 0.105
                    elif channel == 2:
                        calc_curr_comp_list[6] = 1.515
                elif 21 < self.fv_channel2_voltage <= 210:
                    if channel == 1:
                        calc_curr_comp_list[3] = 0.105
                    elif channel == 2:
                        calc_curr_comp_list[6] = 0.105

        return calc_curr_comp_list

    def check_voltage_range(self, input_range, input_module):
        """"
            check and transfer range from itis check2900_voltage_range
            Using in:
            1. force_v_smu_2900
            2. measure_v_smu_2900
            3. set_iv_2900
            4. sweep_iv_2900

        :param : input_range: input range
        :param : input_module: input port with module; 0: SMU
        :return:
        """

        input_range = abs(input_range)
        if input_range == 0:
            output_range = 0
        elif input_range <= 0.2:
            output_range = 0.2
        elif 0.2 < abs(input_range) <= 2:
            output_range = 2
        elif 2 < abs(input_range) <= 20:
            output_range = 20
        elif 20 < abs(input_range) <= 200:
            output_range = 200
        else:
            output_range = -54088
            self.logger.warning(
                "*** ERROR : Incorrect Input SMU Voltage Range : {} is not correct."
                " ADDRESS : {}".format(output_range, self.INST_ADDRESS))
            raise TISError("*** ERROR : Incorrect Input SMU Voltage Range : {} is not correct."
                           " ADDRESS : {}".format(output_range, self.INST_ADDRESS))
        return output_range

    def check_current_range(self, input_range, input_module):
        """"
            check and transfer range from itis check2900_current_range
            Using in:
            1. force_i_smu_2900
            2. measure_i_smu_2900
            3. sweep_iv_2900
            05/16
        :param : input_range: input range
        :param : input_module: input port with module; 0: SMU
        :return:
        """

        if abs(input_range) == 0:
            output_range = 0
        elif abs(input_range) <= 10e-9:
            output_range = 10e-9
        elif 10e-9 < abs(input_range) <= 100e-9:
            output_range = 100e-9
        elif 100e-9 < abs(input_range) <= 1e-6:
            output_range = 1e-6
        elif 1e-6 < abs(input_range) <= 10e-6:
            output_range = 10e-6
        elif 10e-6 < abs(input_range) <= 100e-6:
            output_range = 100e-6
        elif 100e-6 < abs(input_range) <= 1e-3:
            output_range = 1e-3
        elif 1e-3 < abs(input_range) <= 10e-3:
            output_range = 10e-3
        elif 10e-3 < abs(input_range) <= 100e-3:
            output_range = 100e-3
        elif 100e-3 < abs(input_range) <= 1:
            output_range = 1
        elif 1 < abs(input_range) <= 1.5:
            output_range = 1.5
        elif 1.5 < abs(input_range) <= 3:
            output_range = 3
        elif 3 < abs(input_range) <= 10:
            output_range = 10
        else:
            output_range = -54088
            self.logger.warning(
                "*** ERROR : Incorrect Input SMU Current Range is not correct."
                " ADDRESS : {}".format(output_range, self.INST_ADDRESS))
            raise TISError("*** ERROR : Incorrect Input SMU Current Range is not correct."
                           " ADDRESS : {}".format(output_range, self.INST_ADDRESS))

        return output_range

    @staticmethod
    def check_measure_status(input_val):
        """"
            This Subprogram check measure status. follow VSPEC.
            if return 9999 is not define status.
            Using in:
            1. measure_i_smu_2900
            2. measure_v_smu_2900
            3. sweep_iv_2900
            4. measure_cmu_2900

        :param input_val: input value,from myself command setting.
        :return: output_val
        """
        input_status = input_val[0]
        if input_status == "N":
            output_val = 0
        elif input_status == "T":
            output_val = 1
        elif input_status == "C":
            output_val = 2
        elif input_status == "V":
            output_val = 3
        elif input_status == "X":
            output_val = 4
        elif input_status == "G":
            output_val = 5
        elif input_status == "S":
            output_val = 6
        elif input_status == "U":
            output_val = 7
        elif input_status == "D":
            output_val = 8
        else:
            output_val = 9999

        return output_val

    def check_setiv_sweepmode(self, set_iv_sweep_mode):
        """"
            This Subprogram check sweep mode. follow VSPEC.
            Using in:
            1. set_iv_2900

        :param set_iv_sweep_mode: input sweep mode. 1,3,-1,-3: V mode; 2, 4, -2, -4: I mode.
        :return: output_val
        """
        if set_iv_sweep_mode == 1 or set_iv_sweep_mode == 2 or set_iv_sweep_mode == 3 or set_iv_sweep_mode == 4 or \
                set_iv_sweep_mode == -1 or set_iv_sweep_mode == -2 or set_iv_sweep_mode == -3 or set_iv_sweep_mode == -4:
            set_iv_sweep_mode = set_iv_sweep_mode
        else:
            self.logger.warning(
                "*** ERROR : Sweep mode selected wrongly."
                "Using +-1、+-2、+-3、+-4. ADDRESS : {}".format(set_iv_sweep_mode, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : Sweep mode selected wrongly."
                "Using +-1、+-2、+-3、+-4. ADDRESS : {}".format(set_iv_sweep_mode, self.INST_ADDRESS))

        return set_iv_sweep_mode

    def check_setiv_other(self, set_iv_2900_step, set_iv_hold_t, set_iv_delay_t, set_iv_power_compliance,
                          set_iv_stop_mode):
        """"
            This Subprogram check sweep mode. follow VSPEC.
            Using in:
            1. set_iv_2900

        :param set_iv_2900_step:        點數
        :param set_iv_hold_t:           起始點停頓ms
        :param set_iv_delay_t:          每個點的停頓ms
        :param set_iv_stop_mode:        stop_mode
        :param set_iv_power_compliance:        stop_mode
        :return: Massage
        """
        # check step
        if set_iv_2900_step < 2 or set_iv_2900_step > 1001:
            self.logger.warning(
                "*** ERROR : set_iv_2900_step must be 2 to 1001. ADDRESS : {}".format(set_iv_2900_step,
                                                                                      self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : set_iv_2900_step must be 2 to 1001. ADDRESS : {}".format(set_iv_2900_step,
                                                                                      self.INST_ADDRESS))
        # check hold time
        if set_iv_hold_t < 0 or set_iv_hold_t > 655.35:
            self.logger.warning(
                "*** ERROR : set_iv_hold_t must be 0 to 655.35. ADDRESS : {}".format(set_iv_hold_t, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : set_iv_hold_t must be 0 to 655.35. ADDRESS : {}".format(set_iv_hold_t, self.INST_ADDRESS))
        # check delay time
        if set_iv_delay_t < 0 or set_iv_delay_t > 100:
            self.logger.warning(
                "*** ERROR : set_iv_delay_t must be 0 to 100 when using B2900 Series. ADDRESS : {}".format(
                    set_iv_delay_t, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : set_iv_delay_t must be 0 to 100 when using B2900 Series. ADDRESS : {}".format(
                    set_iv_delay_t, self.INST_ADDRESS))
        # check power compliance
        if set_iv_power_compliance != 0:
            self.logger.warning(
                "*** ERROR : P_compliance must be 0 when using B2900 Series. ADDRESS : {}".format(
                    set_iv_delay_t, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : P_compliance must be 0 when using B2900 Series. ADDRESS : {}".format(
                    set_iv_delay_t, self.INST_ADDRESS))
        # check stop mode
        if set_iv_stop_mode == 0 or set_iv_stop_mode == 1:
            pass
        else:
            self.logger.warning(
                "*** ERROR : set_iv_stop_mode must be 0 or 1. ADDRESS : {}".format(set_iv_stop_mode, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : set_iv_stop_mode must be 0 or 1. ADDRESS : {}".format(set_iv_stop_mode, self.INST_ADDRESS))

        return "check step、hold time、delay time and stop mode : PASS"

    def check_sweepiv_transfer_ivmode(self, sweep_iv_mode):
        """"
            This Subprogram check and transfer sweep iv mode. follow VSPEC.
            Using in:
            1. sweep_iv_2900

        :param sweep_iv_mode:  iv mode
        :return: sweep_iv_mode_transfer
        """
        if sweep_iv_mode == 1:
            # sweep_iv_mode_case for select Voltage or Current mode
            sweep_iv_mode_transfer = 2
        elif sweep_iv_mode == 2:
            sweep_iv_mode_transfer = 1
        else:
            self.logger.warning(
                "*** ERROR : sweep_iv_mode must be 1 or 2. ADDRESS : {}".format(sweep_iv_mode, self.INST_ADDRESS))
            raise TISError(
                "*** ERROR : sweep_iv_mode must be 1 or 2. ADDRESS : {}".format(sweep_iv_mode, self.INST_ADDRESS))

        return sweep_iv_mode_transfer
    # check

    # Command
    # internal Command
    # internal Command

    # system
    def init_system(self):
        """
        initial system
        "*RST"  => Reset system to default.
        "*IDN?" => send HELLO command. Can check GPIB work.
        :return:
        """
        self.iprintf("*RST")
        idn = self.ipromptf("*IDN?")
        self.check_error_code_debug()

        idn = "OfflineB2900: init_system => PASS"
        return idn

    def disable_port_2900_rst(self):
        """
        Disable Port.
        ":SYST:ERR:ALL?"    => Reset 2900 error buffer.
        "*RST"              => Reset system to default.
        目前在初始化也有做消除ERROR Buffer的動作，可防止上一次的ERROR MESSAGE沒有消除，被判定成錯誤而跳出程式
        因為Debug，所以先不在Disable的時候刪除，穩定後可以考慮使用
        :return: GPIB address, System Error
        """
        syst_err = self.ipromptf(":SYST:ERR:ALL?")
        self.iprintf("*RST")
        # self.ipromptf("ERR?")

        syst_err = "OfflineB2900: disable_port_rst => PASS"

        return self.INST_ADDRESS, syst_err
    # system

    # SMU spot
    def force_v_smu_2900(self, force_v_smu_port, force_v_smu_module, force_v_smu_voltage, force_v_smu_range=0,
                         force_v_smu_compliance=0):
        """
        This Subprogram conduct Voltage on the SMU or Pin
        self.check_smu_input_module:                check input module
        self.check_fv_calc_current_compliance:      transfer compliance.
        self.check_voltage_range:                   check voltage range

        :param force_v_smu_port: input port: 1 or 2
        :param force_v_smu_module: input port with module; 0: SMU
        :param force_v_smu_voltage: force voltage
        :param force_v_smu_range: force range
        :param force_v_smu_compliance: I compliance
        :return:
        """
        # check input module
        force_v_smu_module = self.check_smu_input_module(force_v_smu_module)

        # check and transfer compliance
        output_status = 0
        if force_v_smu_compliance == 0:
            output1_onoff_status = self.iprintf("OUTP1:STAT?")
            output2_onoff_status = self.iprintf("OUTP2:STAT?")
            output_status = int(output1_onoff_status) + int(output2_onoff_status)
        fv_transfer_compliance = self.check_fv_calc_current_compliance(force_v_smu_port, force_v_smu_voltage,
                                                                       force_v_smu_compliance, output_status)
        if force_v_smu_port == 1:
            dv_input_compliance = fv_transfer_compliance[3]
        elif force_v_smu_port == 2:
            dv_input_compliance = fv_transfer_compliance[6]
        else:
            dv_input_compliance = force_v_smu_compliance

        # check range
        dv_input_range = self.check_voltage_range(force_v_smu_range, force_v_smu_module)

        # force V
        # Setting the Source Output Mode
        self.iprintf(":SOUR" + str(force_v_smu_port) + ":FUNC:MODE VOLT")
        self.check_error_code()
        # Setting the Output Range
        if force_v_smu_range == 0:
            self.iprintf(":SOUR" + str(force_v_smu_port) + ":VOLT:RANG:AUTO ON")
        else:
            self.iprintf(":SOUR" + str(force_v_smu_port) + ":VOLT:RANG " + str(dv_input_range))
        self.check_error_code()
        # Setting the Limit/Compliance Value
        self.iprintf(":SENS" + str(force_v_smu_port) + ":CURR:PROT " + str(dv_input_compliance))
        self.check_error_code()
        # Applying the DC Voltage
        self.iprintf(":SOUR" + str(force_v_smu_port) + ":VOLT " + str(force_v_smu_voltage))
        self.check_error_code()
        # Enabling the Source Output
        self.iprintf(":OUTP" + str(force_v_smu_port) + " ON")

        # check SMU error code
        self.check_error_code()

        self.offline_forcev_smu_voltage = float(force_v_smu_voltage)
        syst_err = "OfflineB2900: force_v_smu_2900 => PASS"

        return self.INST_ADDRESS, syst_err

    def force_i_smu_2900(self, force_i_smu_port, force_i_smu_module, force_i_smu_current, force_i_smu_range,
                         force_i_smu_compliance):
        """
        This Subprogram conduct Voltage on the SMU or Pin
        self.check_smu_input_module: check input module
        self.check_current_range:    check and transfer range from itis check2900_current_mrange.

        :param force_i_smu_port: input port: 1 or 2
        :param force_i_smu_module: input port with module; 0: SMU
        :param force_i_smu_current: force voltage
        :param force_i_smu_range: force range
        :param force_i_smu_compliance: I compliance
        :return:
        """
        # check input module
        fi_smu_module = self.check_smu_input_module(force_i_smu_module)

        # check range
        fi_input_range = self.check_current_range(force_i_smu_range, fi_smu_module)

        # check and transfer compliance
        fi_input_compliance = force_i_smu_compliance
        if force_i_smu_compliance == 0:
            fi_input_compliance = 6
            if force_i_smu_current <= 1.515:
                fi_input_compliance = 21
            if force_i_smu_current <= 0.105:
                fi_input_compliance = 210

        # force i
        # Setting the Source Output Mode
        self.iprintf(":SOUR" + str(force_i_smu_port) + ":FUNC:MODE CURR")
        # Setting the Output Range
        if force_i_smu_range == 0:
            # AUTO range
            self.iprintf(":SOUR" + str(force_i_smu_port) + ":CURR:RANG:AUTO ON")
        else:
            # fixed range
            self.iprintf(":SOUR" + str(force_i_smu_port) + ":CURR:RANG " + str(fi_input_range))
        # Setting the Limit/Compliance Value
        self.iprintf(":SENS" + str(force_i_smu_port) + ":VOLT:PROT " + str(fi_input_compliance))
        # Applying the DC Voltage
        self.iprintf(":SOUR" + str(force_i_smu_port) + ":CURR " + str(force_i_smu_current))
        # Enabling the Source Output
        self.iprintf(":OUTP" + str(force_i_smu_port) + " ON")

        # check SMU error code
        self.check_error_code()

        self.offline_forcei_smu_current = float(force_i_smu_current)
        syst_err = "OfflineB2900: force_i_smu_2900 => PASS"

        return self.INST_ADDRESS, syst_err

    def measure_i_smu_2900(self, measure_i_smu_port, measure_i_smu_module, measure_i_smu_range):
        """
        This Subprogram conduct measure the current on the SMU or Pin
        self.check_smu_input_module:        check input module
        self.check_current_range:           check and transfer range from itis check2900_current_mrange
                +----------------------------------------+
        '       | VOLT | CURR | RES | TIME | STAT | SOUR |
        '       +----------------------------------------+
        'Index      0      1     2      3      4     5
        :param measure_i_smu_port: Measurement Port
        :param measure_i_smu_module: input port with module; 0: SMU
        :param measure_i_smu_range: Range
        :return: Current ,  measure_i_status
        """
        measure_i_smu_module = self.check_smu_input_module(measure_i_smu_module)
        # Check and transfer the range
        mi_transfer_range = self.check_current_range(measure_i_smu_range, measure_i_smu_module)

        # Peform measurement now
        # Sets measurement data output format
        self.iprintf(":FORM:DATA ASC")
        self.check_error_code()
        # Sets SMU measurement operation mode
        self.iprintf(":SENS" + str(measure_i_smu_port) + ":FUNC \"CURR\"")
        self.check_error_code()
        # Sets current measurement range
        if mi_transfer_range == 0:
            self.iprintf(":SENS" + str(measure_i_smu_port) + ":CURR:RANG:AUTO ON")
            self.check_error_code()
        else:
            self.iprintf(":SENS" + str(measure_i_smu_port) + ":CURR:RANG:AUTO OFF")
            self.check_error_code()
            self.iprintf(":SENS" + str(measure_i_smu_port) + ":CURR:RANG " + str(mi_transfer_range))
            self.check_error_code()
        self.iprintf(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")
        self.check_error_code()
        return_val_array = self.ipromptf(":MEAS? (@" + str(measure_i_smu_port) + ")")
        # only read current
        # return_val_array = self.ipromptf(":MEAS:CURR?")
        self.check_error_code()

        # trim data
        measure_current = float(self.offline_forcei_smu_current)
        measure_i_status = "OfflineB2900: measure_i_smu_2900 => PASS"

        return self.INST_ADDRESS, measure_current, measure_i_status

    def measure_v_smu_2900(self, measure_v_smu_port, measure_v_smu_module, measure_v_smu_range):
        """
        This Subprogram conduct measure the voltage on the SMU or Pin
        self.check_smu_input_module:        check input module
        self.check_voltage_range:           check and transfer range from itis Check2900_voltage_mrange
        self.check_measure_status:          trim error code(status)
        time.sleep(0.1):                    for multiple.Time may be shortened again.
        05/16
                +----------------------------------------+
        '       | VOLT | CURR | RES | TIME | STAT | SOUR |
        '       +----------------------------------------+
        'Index      0      1     2      3      4     5
        :param measure_v_smu_port: Measurement Port
        :param measure_v_smu_module: input port with module;
        0: MPSMU, 1: HRSMU, 2: HPSMU, 3: MFCMU, 4: HVSPGU, 5: WGFMU, 6: MCSMU, 7: HCSMU, 8: HVSMU,
        9: HRSMU and MPSMU with ASU, 10: SCUU
        :param measure_v_smu_range: Range
        :return: Current ,  measure_i_status
        """
        measure_v_smu_module = self.check_smu_input_module(measure_v_smu_module)
        # Check and transfer the range
        mv_transfer_range = self.check_voltage_range(measure_v_smu_range, measure_v_smu_module)

        # Peform measurement now
        # Sets measurement data output format
        self.iprintf(":FORM:DATA ASC")
        # Sets SMU measurement operation mode
        self.iprintf(":SENS" + str(measure_v_smu_port) + ":FUNC \"VOLT\"")
        # Sets current measurement range
        if mv_transfer_range == 0:
            self.iprintf(":SENS" + str(measure_v_smu_port) + ":VOLT:RANG:AUTO ON")
        else:
            self.iprintf(":SENS" + str(measure_v_smu_port) + ":VOLT:RANG:AUTO OFF")
            self.iprintf(":SENS" + str(measure_v_smu_port) + ":VOLT:RANG " + str(mv_transfer_range))
        self.iprintf(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")
        return_val_array = self.ipromptf(":MEAS? (@" + str(measure_v_smu_port) + ")")
        # only read current
        # return_val_array = self.ipromptf(":MEAS:CURR?")

        # trim data
        measure_voltage = float(self.offline_forcev_smu_voltage)
        measure_v_status = "OfflineB2900: measure_v_smu_2900 => PASS"

        return self.INST_ADDRESS, measure_voltage, measure_v_status
    # SMU spot

    # SMU sweep
    def set_iv_2900(self, set_iv_port, set_iv_smu_module, set_iv_sweep_mode, set_iv_range, set_iv_start_v,
                    set_iv_stop_v, set_iv_step, set_iv_hold_t, set_iv_delay_t, set_iv_compliance,
                    set_iv_power_compliance, set_iv_stop_mode):
        """
        This Subprogram conduct setup parameters for staircase sweep measurement.
        self.check_setiv_sweepmode:                  check sweep mode.
        self.check_fv_calc_current_compliance:       check and transfer compliance..
        self.check_setiv_other:                      check step、hold time、delay time and stop mode.

        fv_transfer_compliance  [0: input compliance,
                                 1: channel1:on/off mode,
                                 2: channel1: input_voltage,
                                 3: channel1:transfer compliance,
                                 4: channel2:on/off mode,
                                 5: channel2: input_voltage,
                                 6: channel2:transfer compliance
                                 ]
        :param set_iv_port:             input port (101,201,301...by slot)
        :param set_iv_smu_module:       input port with module; 0: SMU
        :param set_iv_sweep_mode:       input sweep mode; V mode: 1,3,-1,-3. I mode: 2, 4, -2, -4
        :param set_iv_range:            input range
        :param set_iv_start_v:          start value
        :param set_iv_stop_v:           stop value
        :param set_iv_step:             點數
        :param set_iv_hold_t:           起始點停頓ms
        :param set_iv_delay_t:          每個點的停頓ms
        :param set_iv_compliance:       compliance
        :param set_iv_power_compliance: power_compliance
        :param set_iv_stop_mode:        stop_mode
        :return:
        """
        # check sweep mode
        set_iv_sweep_mode = self.check_setiv_sweepmode(set_iv_sweep_mode)

        # Calculate compliance value if set by default
        dv_input_compliance = set_iv_compliance
        if set_iv_compliance == 0:
            if set_iv_sweep_mode % 2 == 1:
                output_status = 0
                if set_iv_compliance == 0:
                    output1_onoff_status = self.iprintf("OUTP1:STAT?")
                    output2_onoff_status = self.iprintf("OUTP2:STAT?")
                    output_status = int(output1_onoff_status) + int(output2_onoff_status)
                fv_transfer_compliance = self.check_fv_calc_current_compliance(set_iv_port, set_iv_stop_v,
                                                                               set_iv_compliance, output_status)
                if set_iv_port == 1:
                    dv_input_compliance = fv_transfer_compliance[3]
                if set_iv_port == 2:
                    dv_input_compliance = fv_transfer_compliance[6]
            else:
                dv_input_compliance = 6 # voltage complinace
                if set_iv_stop_v < 1.515:
                    dv_input_compliance = 21
                if set_iv_stop_v < 0.105:
                    dv_input_compliance = 210
        self.ivmode2900 = 1
        # # check Start、Stop and watts Voltage
        # set_iv_start_v, set_iv_stop_v = self.check_setiv_voltage(set_iv_smu_module, set_iv_start_v, set_iv_stop_v,
        #                                                          set_iv_compliance)
        # check step、hold time、delay time and stop mode
        self.check_setiv_other(set_iv_step, set_iv_hold_t, set_iv_delay_t, set_iv_power_compliance, set_iv_stop_mode)

        # command
        # set hold and delay time
        self.iprintf(":ARM" + str(set_iv_port) + ":TRAN:DEL 0")
        self.iprintf(":ARM" + str(set_iv_port) + ":ACQ:DEL " + str(set_iv_hold_t))
        self.iprintf(":TRIG" + str(set_iv_port) + ":TRAN:DEL 0")
        self.iprintf(":TRIG" + str(set_iv_port) + ":ACQ:DEL " + str(set_iv_delay_t))

        # check range
        set_iv_voltage_range = self.check_voltage_range(set_iv_range, set_iv_smu_module)
        set_iv_current_range = self.check_current_range(set_iv_range, set_iv_smu_module)
        # set iv
        setiv_sweepmode_dict = {
            1: ["VOLT", "LIN", "SING", "CURR"],
            2: ["CURR", "LIN", "SING", "VOLT"],
            3: ["VOLT", "LIN", "DOUB", "CURR"],
            4: ["CURR", "LIN", "DOUB", "VOLT"],
            -1: ["VOLT", "LOG", "SING", "CURR"],
            -2: ["CURR", "LOG", "SING", "VOLT"],
            -3: ["VOLT", "LOG", "DOUB", "CURR"],
            -4: ["CURR", "LOG", "DOUB", "VOLT"]
        }
        self.iprintf(":SOUR" + str(set_iv_port) + ":FUNC:MODE " + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]))
        self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":MODE SWE")
        self.iprintf(":SOUR" + str(set_iv_port) + ":SWE:SPAC " + str(setiv_sweepmode_dict[set_iv_sweep_mode][1]))
        self.iprintf(":SOUR" + str(set_iv_port) + ":SWE:STA " + str(setiv_sweepmode_dict[set_iv_sweep_mode][2]))
        if set_iv_sweep_mode % 2 == 1:
            self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":RANG " +
                         str(set_iv_voltage_range))
        else:
            self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":RANG " +
                         str(set_iv_current_range))
        self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":STAR " +
                     str(set_iv_start_v))
        self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":STOP " +
                     str(set_iv_stop_v))
        self.iprintf(":SOUR" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][0]) + ":POIN " +
                     str(set_iv_step))
        self.iprintf(":SENS" + str(set_iv_port) + ":" + str(setiv_sweepmode_dict[set_iv_sweep_mode][3]) + ":PROT " +
                     str(dv_input_compliance))
        self.iprintf(":TRIG" + str(set_iv_port) + ":ALL:SOUR AINT")
        self.iprintf(":TRIG" + str(set_iv_port) + ":ALL:COUN " + str(set_iv_step))

        # check SMU error code
        self.check_error_code()
        syst_err = "OfflineB2900: set_iv_2900 => PASS"

        return self.INST_ADDRESS, syst_err

    def sweep_iv_2900(self, sweep_iv_port, sweep_iv_module, sweep_iv_mode, sweep_iv_range):
        """
        This Subprogram conduct perform staircase sweep measurement.
        self.check_setiv_sweepmode:                  check and transfer iv mode.
        self.check_current_range:                    check current range.
        self.check_setiv_sweepmode:                  check voltage range.
                +----------------------------------------+
        '       | VOLT | CURR | RES | TIME | STAT | SOUR |
        '       +----------------------------------------+
        'Index      0      1     2      3      4     5
        :param sweep_iv_port: channel (SLOT1, SLOT2, ...SLOT10)
        :param sweep_iv_module: input port with module. 0: SMU
        :param sweep_iv_mode: 1: V mode; 2: I mode
        :param sweep_iv_range: Range
        :return: self.INST_ADDRESS, sweep_iv_outputdata, sweep_iv_source, sweep_iv_status
        """
        # transfer sweep iv mode
        sweep_iv_mode_transfer = self.check_sweepiv_transfer_ivmode(sweep_iv_mode)
        # # transfer MM command
        # sweep_iv_mode_mmcommand = self.check_sweepiv_mmcommand(self.ivmode2900, self.pbias2900)

        # command
        # Sets measurement data output format
        self.iprintf(":FORM:DATA ASC")
        self.check_error_code()
        self.iprintf(":FORM:ELEM:SENS VOLT,CURR,RES,TIME,STAT,SOUR")
        self.check_error_code()
        sweep_iv_transfer_range = 0
        mode_str = "CURR"
        if sweep_iv_mode_transfer == 1:
            # check and transfer range
            sweep_iv_transfer_range = self.check_current_range(sweep_iv_range, sweep_iv_module)
            mode_str = "CURR"
        if sweep_iv_mode_transfer == 2:
            # check and transfer range
            sweep_iv_transfer_range = self.check_voltage_range(sweep_iv_range, sweep_iv_module)
            mode_str = "VOLT"

        self.iprintf(":SENS" + str(sweep_iv_port) + ":FUNC \"" + mode_str + "\"")
        self.check_error_code()
        if sweep_iv_transfer_range == 0:
            self.iprintf(":SENS" + str(sweep_iv_port) + ":" + mode_str + ":RANG:AUTO ON")
            self.check_error_code()
        else:
            self.iprintf(":SENS" + str(sweep_iv_port) + ":" + mode_str + ":RANG:AUTO OFF")
            self.check_error_code()
            self.iprintf(":SENS" + str(sweep_iv_port) + ":" + mode_str + ":RANG " + str(sweep_iv_transfer_range))
            self.check_error_code()
        # Enabling the Source Output
        self.iprintf(":OUTP" + str(sweep_iv_port) + " ON")
        self.check_error_code()
        # Initiate transition and acquire
        self.iprintf(":INIT (@" + str(sweep_iv_port) + ")")
        self.check_error_code()
        return_val_array = []
        return_val_array.append(self.ipromptf(":FETC:ARR:VOLT? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf(":FETC:ARR:CURR? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf(":FETC:ARR:RES? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf(":FETC:ARR:TIME? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf(":FETC:ARR:STAT? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()
        return_val_array.append(self.ipromptf(":FETC:ARR:SOUR? (@" + str(sweep_iv_port) + ")"))
        self.check_error_code()

        measure_point = self.ipromptf(":SYST:DATA:QUAN? (@" + str(sweep_iv_port) + ")")
        # check SMU error code
        self.check_error_code()

        # define offline data
        offline_iv_outputdata_list = []
        offline_iv_source_list = []
        offline_data_add = 0
        for a in range(self.offline_setiv_step):
            offline_iv_source_list.append(a)
            if sweep_iv_mode_transfer == 1:
                offline_data = 1e-12
            else:
                offline_data = 1
            if a == 0:
                offline_iv_outputdata_list.append(offline_data)
            else:
                offline_data_add = offline_data_add + offline_data
                offline_iv_outputdata_list.append(offline_data_add)

        sweep_iv_output_data = offline_iv_outputdata_list
        sweep_iv_source = offline_iv_source_list
        sweep_iv_status = "OfflineB2900: sweep_iv_2900 => PASS"

        return self.INST_ADDRESS, sweep_iv_output_data, sweep_iv_source, sweep_iv_status
    # SMU sweep
    # Command
    def _error_decorator(func):
        """
        Check error code after sending command
        :param func:
        :return:
        """

        @wraps(func)
        def wrapper(*args, **kwargs):
            self = args[0]  # args[0] is object itself
            func(*args, **kwargs)
            self.check_error_code()

            return self.INST_ADDRESS, func.__name__

        return wrapper


    @_error_decorator
    def set_trigger_subsystem_settings(self, layer='ALL', action='ALL', channel=0, bypass=None, count=None, delay=None, sour=None, timer=None,
                                       trg_out=None, trg_mode=None):
        """

        :param layer: ARM/TRIG/ALL
        :param action: ACQ/TRAN/ALL
        :param channel: 0/1/2, 0 means use channel 1,2
        :param bypass: True: ONCE, False: OFF
        :return:
        """
        pass

    def trigger_subsystem_settings_query(self, func, layer="ALL"):
        """

        :param func: BYP/COUN/DEL/SOUR/TIM/TOUT:SIGN/TOUT:STAT
        :param layer: ALL/TRIG/ARM
        :return: dict
        """
        res = dict()
        layer = layer.upper()
        res['ARM1'], res['ARM2'] = dict(), dict()
        res['TRIG1'], res['TRIG2'] = dict(), dict()
        value = {"BYP": "OFF",
                 "COUN": 1,
                 "DEL": 0,
                 "SOUR": "AINT",
                 "TIM": 2e-5,
                 "TOUT:SIGN": "EXT1",
                 "TOUT:STAT": "OFF"}

        if layer == 'ARM' or layer == 'ALL':
            res['ARM1']['ACQ'] = value[func]
            res['ARM2']['ACQ'] = value[func]
            res['ARM1']['TRAN'] = value[func]
            res['ARM2']['TRAN'] = value[func]

        if layer == 'TRIG' or layer == 'ALL':
            res['TRIG1']['ACQ'] = value[func]
            res['TRIG2']['ACQ'] = value[func]
            res['TRIG1']['TRAN'] = value[func]
            res['TRIG2']['TRAN'] = value[func]

        return res

    @_error_decorator
    def send_imm_trigger(self, layer='ALL', action='ALL', channel=0):
        """
                Send immediate trigger for the specified device action to the specified channel.

                :param layer: ALL/ARM/TRIG
                :param action: ALL/TRIG/ARM
                :param channel: 0/1/2, 0 means use channel 1,2
                :return:
                """
        pass

    @_error_decorator
    def initiates_trigger(self, channel=1, action='ALL'):
        """
        Initiates the specified device action for the specified channel. Trigger status is changed from idle to initiated.

        :param channel: 0/1/2, 0 means use channel 1,2
        :param action: ALL/TRIG/ARM
        :return:
        """
        pass

    def device_action_status(self, channel=0):
        """
        Checks the status of the specified device action for the specified channel, and waits until the status is changed to idle.
        response returns 1 if the specified device action is in the idle state.

        :param channel: 0/1/2, 0 means use channel 1,2
        :param action: ALL/TRIG/ARM
        :return:
        """
        res = dict()
        res['CH1'], res['CH2'] = dict(), dict()
        if channel > 0:
            res[f"CH{channel}"]["ACQ"] = 1
            res[f"CH{channel}"]["TRAN"] = 1
        elif channel == 0:
            res[f"CH1"]["ACQ"] = 1
            res[f"CH1"]["TRAN"] = 1
            res[f"CH2"]["ACQ"] = 1
            res[f"CH2"]["TRAN"] = 1

        return res

    @_error_decorator
    def set_digital_io_settings(self, pin=1, func=None, polarity=None, timing=None, pul_width=None, trg_type=None):
        """
        Set digital io (EXT1 ~ EXT14) settings, including function, polarity, trigger timing, pulse width, trigger type.

        :param pin: 1 ~ 14
        :param func: DIO/DINP/HVOL/TINP/TOUT
        :param polarity: NEG/POS
        :param timing: BEFORE/AFTER/BOTH
        :param pul_width: default is 1e-4
        :param trg_type: EDGE/LEVEL
        :return:
        """
        pass

    def digital_io_settings_query(self, pin=1):
        """
        Read digital o setting from b2900

        :param pin: 1 ~ 14
        :return:
        """
        res = dict()
        res[f'EXT{pin}'] = dict()

        res[f'EXT{pin}']["function"] = 'TOUT'
        res[f'EXT{pin}']["polarity"] = 'POS'
        res[f'EXT{pin}']["timing"] = 'BOTH'
        res[f'EXT{pin}']["width"] = '+1.00000000E-004'
        res[f'EXT{pin}']["type"] = 'EDGE'

        return res

    @_error_decorator
    def set_action_layer_trigger_output(self, pin=1, stat=None, action=None, channel=0):
        """
        Set the trigger output for the status change between the trigger layer and the transient/acquire device action.

        :param pin: 1 ~ 14
        :param stat: ON/OFF
        :param action: ALL/TRAN/ACQ
        :param channel: 0/1/2, 0 means use channel 1,2
        :return:
        """
        if not action:
            return

        action = action.upper()
        if action == "ACQ" or action == "ALL":
            if channel > 0:
                self.iprintf(f":SENS{channel}:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iprintf(f":SENS{channel}:TOUT:STAT {stat}")
            elif channel == 0:
                self.iprintf(f":SENS1:TOUT:SIGN EXT{pin}")
                self.iprintf(f":SENS2:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iprintf(f":SENS1:TOUT:STAT {stat}")
                    self.iprintf(f":SENS2:TOUT:STAT {stat}")

        if action == "TRAN" or action == "ALL":
            if channel > 0:
                self.iprintf(f":SOUR{channel}:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iprintf(f":SOUR{channel}:TOUT:STAT {stat}")
            elif channel == 0:
                self.iprintf(f":SOUR1:TOUT:SIGN EXT{pin}")
                self.iprintf(f":SOUR2:TOUT:SIGN EXT{pin}")
                if stat:
                    self.iprintf(f":SOUR1:TOUT:STAT {stat}")
                    self.iprintf(f":SOUR2:TOUT:STAT {stat}")

    def action_layer_trigger_output_query(self):
        """
        Query the settings for action layer trigger output.

        :return:
        """
        res = dict()
        res['CH1'], res['CH2'] = dict(), dict()

        res['CH1']['sour.output'] = self.ipromptf(":SOUR1:TOUT:SIGN?")
        res['CH2']['sour.output'] = self.ipromptf(":SOUR2:TOUT:SIGN?")
        res['CH1']['sour.stat'] = self.ipromptf(":SOUR1:TOUT:STAT?")
        res['CH2']['sour.stat'] = self.ipromptf(":SOUR2:TOUT:STAT?")
        res['CH1']['sens.output'] = self.ipromptf(":SENS1:TOUT:SIGN?")
        res['CH2']['sens.output'] = self.ipromptf(":SENS2:TOUT:SIGN?")
        res['CH1']['sens.stat'] = self.ipromptf(":SENS1:TOUT:STAT?")
        res['CH2']['sens.stat'] = self.ipromptf(":SENS2:TOUT:STAT?")

        return self.INST_ADDRESS, res