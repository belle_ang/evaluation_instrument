import pyvisa
import time

track_time = {}

class HiokiDM7276:
    def __init__(self):
        self.dvm = pyvisa.ResourceManager().open_resource("ASRL3::INSTR")
        self.instrument_init()

    def iwrite(self, cmd):
        time1 = time.perf_counter()
        self.iwrite(cmd)
        track_time[cmd] = time.perf_counter() - time1

    def iread(self, cmd):
        time1 = time.perf_counter()
        msg = self.iread(cmd)
        track_time[cmd] = time.perf_counter() - time1
        return msg

    def instrument_init(self):
        self.iwrite("*RST")
        idn = self.iread("*IDN?")
        # self.iwrite(":INITIATE:CONTINUOUS OFF") # todo : why use initiate continous off
        # self.iwrite("SAMP:COUN 4") # todo
        return idn != ""

    def measure_v3458(self):
        data = self.iread("MEAS:DC?MAX")
        return data

    def __resolution_round_off(self, resolution):
        pass

    def __check_integ_value(self, integ_value_):
        if integ_value_ == 0 or 0.5e-6 <= integ_value_ <= 999999.9e-6:
            # resolution " 0.1E6 [s] for 3458A
            return f"{integ_value_:.7f}"
        elif 1 <= integ_value_ < 10:
            return f"{integ_value_:.0f}"
        elif 10 <= integ_value_ <= 100:
            return integ_value_

    def set_adc3458(self, integ_value=0.5E-6, autozero=1):
        self.iwrite(':VOLT:APER:ENAB ON') # todo :check the different , if init is enable strip it

        if 1 < float(integ_value) < 10:
            self.iwrite(f':VOLT:NPLC {integ_value}') # todo : why use the NPLC or APER
        elif 10 < float(integ_value) < 100:
            self.iwrite(f':VOLT:NPLC {integ_value}')
        elif float(integ_value) == 0 or float(integ_value) < 1:
            self.iwrite(f':VOLT:APER {integ_value}')

        on_off = ['OFF', 'ON']
        try:
            self.iwrite(f'VOLT:DC:NULL {on_off[autozero]}')
        except IndexError:
            print(f"{autozero} not illegal autozero[0/1]")



