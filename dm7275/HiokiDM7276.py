import pyvisa


class HiokiDM7276():
    def __init__(self):
        self.dvm = pyvisa.ResourceManager.open_resource("")
        self.check()

    def write(self, cmd):
        self.dvm.write(cmd)

    def prompt(self, cmd):
        return self.dvm.query(cmd)

    def check(self):
        self.write("*RST")
        idn = self.prompt("*IDN?")
        self.write("VOLT:DC:RANG:AUTO ON")
        self.write(":INITIATE:CONTINUOUS OFF")
        self.write("SAMP:COUN 4")
        return idn != ""

    def measure_v3458(self, voltage_value_name=" "):
        # self.write("INIT;*TRG")
        data = self.prompt("READ?")
        # if  data.find(",") != -1:
        #     result = data.split(",")[2].split("\n")[0]
        # else:
        #     result = data

        return 0, float(data)

    def set_adc3458(self, integ_value="0.0000005", auto_zero="ON"):
        if 1 < float(integ_value) < 10:
            print('SCPI: :VOLT:NPLC:ENAB ON')
            print('SCPI: :VOLT:NPLC ' + integ_value)
            print('SCPI: VOLT:DC:NULL ' + auto_zero + "\n")

            self.write(':VOLT:NPLC:ENAB ON')
            self.write(':VOLT:NPLC ' + integ_value)
            if auto_zero == 0:
                self.write('VOLT:DC:NULL OFF')
            else:
                self.write('VOLT:DC:NULL ON')
        elif 10 < float(integ_value) < 100:
            print(':VOLT:NPLC:ENAB ON')
            print(':VOLT:NPLC ' + str(integ_value))
            print('VOLT:DC:NULL ' + str(auto_zero) + "\n")

            self.write(':VOLT:NPLC:ENAB ON')
            self.write(':VOLT:NPLC ' + str(integ_value))
            if auto_zero == 0:
                self.write('VOLT:DC:NULL OFF')
            else:
                self.write('VOLT:DC:NULL ON')
        elif float(integ_value) == 0 or float(integ_value) < 1:
            print(':VOLT:APER:ENAB ON')
            print(':VOLT:APER ' + str(integ_value))
            print('VOLT:DC:NULL ' + str(auto_zero) + "\n")

            self.write(':VOLT:APER:ENAB ON')
            self.write(':VOLT:APER ' + str(integ_value))
            if auto_zero == 0:
                self.write('VOLT:DC:NULL OFF')
            else:
                self.write('VOLT:DC:NULL ON')
