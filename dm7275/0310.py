import pyvisa
import time

timetrack = []

dvm = pyvisa.ResourceManager().open_resource("ASRL3::INSTR")
def iwrite(cmd):
    time1 = time.perf_counter()
    dvm.write(cmd)
    timetrack.append([cmd, time.perf_counter() - time1])

def iquery(cmd):
    time1 = time.perf_counter()
    res = dvm.query(cmd)
    timetrack.append([cmd, time.perf_counter() - time1])
    return res

iwrite("*RST")
# iwrite(f':VOLT:APER:ENAB ON')

# iwrite(":INITIATE:CONTINUOUS OFF")

# iwrite(f':VOLT:NPLC MIN')
iwrite(":VOLT:APER:ENAB ON;:VOLT:APER MIN")


time1 = time.perf_counter()
# meas = dvm.query("READ?")
meas = iquery("FETCH?")
# iwrite(":VOLT:APER:ENAB ON;:VOLT:APER MIN")

print(f"time: {time.perf_counter() - time1:.5f}s, data:{meas}")
# iwrite(f':VOLT:NPLC MIN')
# iwrite(f':VOLT:NPLC MIN')

import pprint

for i in timetrack:
    print (i)

