def compen_5250(self, freq_val, l_hptrx, l_usrhptrx_h, l_usrhptrx_l, l_usrhpcoax_h, l_usrhpcoax_l, raw_cap, raw_g):
    """

    :param freq_val: Measured frequency [Hz]
    :param l_hptrx: HP Triax Cable Length [m]
    :param l_usrhptrx_h: User Triax Cable Length (High) [m]
    :param l_usrhptrx_l: User Triax Cable Length (Low) [m]
    :param l_usrhpcoax_h: User Coax Cable Length (High) [m]
    :param l_usrhpcoax_l: User Coax Cable Length (Low) [m]
    :param raw_cap: R_c. Raw Capacitance Data
    :param raw_g: R_g. Raw Conductance Data
    :return: c_c: Compensated Capacitance Data; c_g: Compensated Conductance Data; sta: status
    """
    # Rlc_data:
    #               R [ohm]       L [H]         C [F]
    #       DATA1   74.65E-3,  140.00E-9,   58.44E-12  ! Frame Path 1
    #       DATA2   75.41E-3,   90.00E-9,   67.13E-12  ! Frame Path 2
    #       DATA3  231.41E-3,  450.00E-9,  178.85E-12  ! Card Path High
    #       DATA4  177.56E-3,  390.00E-9,  135.45E-12  ! Card Path Low
    #       DATA5  100.70E-3,  400.00E-9,   80.00E-12  ! HP Triax Cable   [/m]
    #       DATA6  100.70E-3,  400.00E-9,   80.00E-12  ! User Triax Cbl H [/m]
    #       DATA7  100.70E-3,  400.00E-9,   80.00E-12  ! User Triax Cbl L [/m]
    #       DATA8  114.00E-3,  544.00E-9,  130.00E-12  ! User Coax Cbl H  [/m]
    #       DATA9  114.00E-3,  544.00E-9,  130.00E-12  ! User Coax Cbl L  [/m]
    #       DATA10   0.00E-3,    0.00E-9,    1.20E-12  ! Stray Capacitance

    # Adjust Factor Value
    data1 = [0, 0.07465, 0.00000014, 0.00000000005844]
    data2 = [0, 0.07541, 0.00000009, 0.00000000006713]
    data3 = [0, 0.23141, 0.00000045, 0.00000000017885]
    data4 = [0, 0.17756, 0.00000039, 0.00000000013545]
    data5 = [0, 0.1007, 0.0000004, 0.00000000008] * l_hptrx
    data6 = [0, 0.1007, 0.0000004, 0.00000000008] * l_usrhptrx_h
    data7 = [0, 0.1007, 0.0000004, 0.00000000008] * l_usrhptrx_l
    data8 = [0, 0.114, 0.000000544, 0.00000000013] * l_usrhpcoax_h
    data9 = [0, 0.114, 0.000000544, 0.00000000013] * l_usrhpcoax_l
    data10 = [0, 0, 0, 0.0000000000012]

    pi = 3.14159265359

    if l_hptrx < 0 or l_usrhptrx_h < 0 or l_usrhptrx_l < 0 or l_usrhpcoax_h < 0 or l_usrhpcoax_l < 0:
        self.logger.warning("*** Incorrect Input Parameter is not correct.".format(self.INST_ADDRESS))

    idx_resistance, idx_inductance, idx_capacitance = 1, 2, 3
    # Calculate Coef Value
    tmp1 = 2 * data1[3] + 4 * data2[3] + 2 * data3[3] + 2 * data4[3] + 4 * data5[3] + 2 * data6[3] \
           + 2 * data7[3] + 2 * data8[3] + 2 * data9[3]
    tmp2 = 2 * data2[3] + 2 * data3[3] + 2 * data4[3] + 4 * data5[3] + 2 * data6[3] + 2 * data7[3] \
           + 2 * data8[3] + 2 * data9[3]
    tmp3 = 1 * data3[3] + 2 * data5[3] + 2 * data6[3] + 2 * data8[3]
    tmp4 = 1 * data4[3] + 2 * data5[3] + 2 * data7[3] + 2 * data9[3]
    tmp5 = 2 * data5[3] + 2 * data6[3] + 2 * data7[3] + 2 * data8[3] + 2 * data9[3]
    tmp6 = 1 * data6[3] + 2 * data8[3]
    tmp7 = 1 * data7[3] + 2 * data9[3]
    tmp8 = 1 * data8[3] * data8[2] + 1 * data9[3] * data9[2]
    coef = [54088, 54088, 54088, 54088, 54088]
    coef[0] = -2 * pi * pi * (
            tmp1 * data1[2] + tmp2 * data2[2] + tmp3 * data3[2] + tmp4 * data4[2] + tmp5 * data5[2] +
            tmp6 * data6[2] + tmp7 * data7[2] + tmp8)
    tmp8 = 1 * data8[3] * data8[1] + 1 * data9[3] * data9[1]
    coef[1] = pi * (
            tmp1 * data1[1] + tmp2 * data2[1] + tmp3 * data3[1] + tmp4 * data4[1] + tmp5 * data5[1] +
            tmp6 * data6[1] + tmp7 * data7[1] + tmp8)
    coef[2] = 2 * data1[1] + 2 * data2[1] + 1 * data3[1] + 1 * data4[1] + 2 * data5[1] + 1 * data6[1] + \
              1 * data7[1] + 1 * data8[1] + 1 * data9[1]
    coef[3] = pi * (
            4 * data1[2] + 4 * data2[2] + 2 * data3[2] + 2 * data4[2] + 4 * data5[2] +
            2 * data6[2] + 2 * data7[2] + 2 * data8[2] + 2 * data9[2])
    coef[4] = data10[3]

    # Setup Complex Value
    y0_r = 0
    y0_i = 2 * pi * freq_val * coef[4]

    coef1_r = coef[0] * freq_val * freq_val + 1
    coef1_i = coef[1] * freq_val
    coef2_r = coef[2]
    coef2_i = coef[3] * freq_val

    # Convert : Cp-G to G-B[Ym]
    ym_r = raw_g
    ym_i = 2 * pi * freq_val * raw_cap

    # Compen : Ym to Yt
    ynof_r = ym_r - y0_r
    ynof_i = ym_i - y0_i

    nume_r = ynof_r * coef1_r - ynof_i * coef1_i
    nume_i = ynof_r * coef1_i + ynof_i * coef1_r
    deno_r = 1 - (ynof_r * coef2_r - ynof_i * coef2_i)
    deno_i = -1 * (ynof_r * coef2_i + ynof_i * coef2_r)
    yt_r = (nume_r * deno_r + nume_i * deno_i) / (deno_r * deno_r + deno_i * deno_i)
    yt_i = (nume_i * deno_r - nume_r * deno_i) / (deno_r * deno_r + deno_i * deno_i)

    # Convert : G-B[Yt] to Cp-G
    c_g = yt_r
    c_c = yt_i / (2 * pi * freq_val)

    return c_c, c_g
