# from pytic.tdriver.HiokiBoxIm3570_1 import *
# from pyrun.library import *
from datetime import datetime
from IM3570 import *

"""
1. 把想要測的參數加到 # PARAMETER 的 lst. 
2. 越內層的 for 參數跳越快
"""

freq_lst = [1e4, 1e5, 1e6]
speed_lst = [0, 1, 2]  # 0:FAST, 1:MED, 2:SLOW, 3:SLOW2
step_lst = [41, 71, 101, 201]
# step_lst = [11]
ac_swp_ = ac_spot_ = 0.05  # Assume use same ac in spot and sweep
v1_, v2_ = 0, 40
hold_ = delay_ = 0
testcases = []

for f_ in freq_lst:
    for sp_ in speed_lst:
        for sn_ in step_lst:
            testcases.append([f_, ac_swp_, sp_, v1_, v2_, sn_, hold_, delay_, ac_spot_])

def write_report(report_context, csv_file =datetime.now().strftime("%Y%m%d_%H%M")):
    with open(csv_file + ".csv", "w", encoding="utf-8") as f:
        f.write(report_context)


spot_header = "\nfreq, test_signal, speed, dc, step_number, hold, delay, status, capacitance, conductance, freq_time, cmu_time, force_time, meas_time"
sweep_header = "\nfreq, test_signal, speed, signal_level, start_voltage, stop_voltage, step_number, hold, delay, setfreq_time, setcmu_time,setcv_time,sweep_time"


class DataCorrection:
    """
    INPUT:
    spot: freq, tc_swp_signal_lev, speed, delay_t
    sweep: frequency, tc_swp_signal_lev, speed, start_v, stop_v, step_num, hold_t, delay_t
    spot and sweep return format : [1. setting time], [2. measure time], [3. measure result]
    """

    def __init__(self):
        # self.fieldBeeConnect()
        self.lcr = OnlineHiokiIM3570()

    # def fieldBeeConnect(self):
    #     start_library("./pytic/tdriver/ROSE_INST_CONFIG.csv")
    #     a_port = FN_port(1, 2)
    #     self.lcr = a_port.driver

    def spot(self, spot_case):
        [frequency_, signal_level_, integration_, _, _, _, hold_t_, delay_t_, output_voltage_] = spot_case
        freq_time = self.lcr.set_freq_3570(frequency_)
        cmu_time = self.lcr.set_cmu84_3570(integration_, signal_level_)
        force_time = self.lcr.force_v_3570(output_voltage_)
        meas_time, spot_res = self.lcr.measure_cmu84_3570()
        # spot_result = (
        #     frequency_
        #     + signal_level_
        #     + integration_
        #     + hold_t_
        #     + delay_
        #     + output_voltage_
        #     + spot_res
        #     + freq_time
        #     + cmu_time
        #     + force_time
        #     + meas_time
        # ) # todo: check&cancel
        return f"{frequency_} , {signal_level_}, {integration_}, {hold_t_}, {delay_}, {output_voltage_}" + spot_res + f"{freq_time}, {cmu_time}, {force_time}, {meas_time}"
        # spot_result = (f"{frequency_} , {signal_level_}, {integration_}, {hold_t_}, {delay_}, {output_voltage_}" +
        #                spot_res + f"{freq_time}, {cmu_time}, {force_time}, {meas_time}")
        # return spot_result

    def sweep(self, swp_case):
        [
            frequency_,
            signal_level_,
            integration_,
            start_voltage_,
            stop_voltage_,
            step_number_,
            hold_t_,
            delay_t_,
            _,
        ] = swp_case

        freq_time = self.lcr.set_freq_3570(frequency_)
        cmu_time = self.lcr.set_cmu84_3570(integration_, signal_level_)
        cv_time = self.lcr.set_cv84_3570(start_voltage_, stop_voltage_, step_number_, hold_t_, delay_t_)
        status, measure_time, swp_res = self.lcr.sweep_cv84_3570()

        sweep_header = "\nfreq, test_signal, speed, signal_level, start_voltage, stop_voltage, step_number, hold, delay, setfreq_time, setcmu_time,setcv_time,sweep_time"
        swp_para = f"\n{frequency_}, {signal_level_}, {integration_}, {signal_level_}, {start_voltage_} ,{stop_voltage_}, {step_number_}, {hold_t_}, {delay_t_}, {freq_time}, {cmu_time}, {cv_time}, {measure_time}\n"
        return sweep_header + swp_para + swp_res + "\n"


def collect_spot_sweep_data():

    next2line = "\n" * 2
    underline = "_" * 50

    report_str = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    spot_para = "\nfreq,ac,speed,dc,delay,hold,capacitance, conductance, return time, setting time, measure time, total"  # todo:check & cancel
    spot_str = next2line + underline + "SPOT" + underline + next2line + spot_para
    sweep_str = next2line + underline + "SWEEP" + underline + next2line + spot_para

    for testcase_ in testcases:
        sweep_str += DataCorrection().sweep(testcase_)
    report_str = report_str + sweep_str

    write_report(report_str)


if __name__ == "__main__":
    collect_spot_sweep_data()
