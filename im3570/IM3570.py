import sys
import time
import numpy as np
import pyvisa
from im3570.compensation_component import *


Agilent_Triax_Cable = 3


def wait_precise(wait_time):
    wait_time_now = time.perf_counter() + wait_time
    while time.perf_counter() < wait_time_now:
        pass
    return


def time_track(func):
    def timed(*args):
        start_time = time.perf_counter()
        func(*args)
        consume_time = time.perf_counter() - start_time
        return consume_time

    return timed


class OnlineHiokiIM3570:
    def __init__(self):
        self.lcr = pyvisa.ResourceManager().open_resource("GPIB0::2::INSTR")
        self.czz = CComp()

        self.time_tracking = {}
        self.init_instrument()
        # self.dc = ExternalDCSupply()
        self.measurement_range = 0

    def iprompt(self, command):
        start_time = time.perf_counter()
        query_read = self.lcr.query(command)
        self.time_tracking[command] = time.perf_counter() - start_time
        return query_read

    def iwrite(self, command):
        start_time = time.perf_counter()
        self.lcr.write(command)
        self.time_tracking[command] = time.perf_counter() - start_time

    def is_connect(self):
        return True if self.iprompt("*IDN?") != "" else False

    def init_instrument(self):
        self.iwrite("*RST")
        self.iwrite(":TRIG EXT")
        self.iwrite(":LEV CV")
        self.iwrite(":PAR1 CP;PAR3 G")
        self.iwrite(":MEAS:VAL 18")
        self.iwrite(":DCB ON;DCB:LEV 0")


    @time_track
    def set_freq_3570(self, frequency=9999999):
        self.frequency = self.__check_frequency(frequency)
        self.iwrite(f":FREQ {frequency}")

    @time_track
    def set_cmu84_3570(self, integration_time: int, signal_level=0.03):
        integration_time = self.__check_integration_time(integration_time)
        signal_level = self.__check_signal_level(signal_level)
        self.iwrite(f":SPEE {integration_time}")
        self.iwrite(f":LEV:CVOLT {signal_level}")

    @time_track
    def force_v_3570(self, output_voltage):
        if self.dc.is_connect() is True:
            self.dc.force_v(output_voltage)
        else:
            if abs(float(output_voltage)) < 2.5:
                self.iwrite(f":DCB ON;DCB:LEV {output_voltage}")
            else:
                sys.exit("internal dc only support 2.5V")

    # @time_track
    # def force_v_3570(self, output_voltage):
    #     self.dc.force_v(output_voltage)

    @time_track
    def set_cv84_3570(self, start_voltage, stop_voltage, steps_number, hold_time=0, delay_time=0):
        self.__check_voltage(start_voltage)
        self.__check_voltage(stop_voltage)
        # self.__check_voltage(start_voltage, stop_voltage)
        # self.__check_time(hold_time, delay_time)
        self.bias_value = self.__step_aperture(start_voltage, stop_voltage, steps_number)  # convert to step aperture
        self.hold_time = float(hold_time)
        self.delay_time = delay_time

    def measure_cmu84_3570(self, measurement_range=0):
        self.__check_measurement_range(measurement_range)
        start_time = time.perf_counter()
        self.iwrite("*TRG")
        status_, capacitance, conductance = self.lcr.query(":MEAS?").split(",")
        capacitance, conductance = float(capacitance), float(conductance)

        print(f"original capacitance :{capacitance},  conductance: {conductance}")
        # compensation

        l_hptrx, l_usrhptrx_h, l_usrhptrx_l, l_usrhpcoax_h, l_usrhpcoax_l = 3, 0, 0, 4,4
        capacitance, conductance = self.compen_5250(self.frequency, l_hptrx, l_usrhptrx_h, l_usrhptrx_l, l_usrhpcoax_h,
                                                    l_usrhpcoax_l, capacitance, conductance)
        print(f"COMPENSATION capacitance :{capacitance},  conductance: {conductance}")

        time_stamp = time.perf_counter() - start_time
        status = 0

        return status, time_stamp, float(capacitance), float(conductance)

    def sweep_cv84_3570(self, measurement_range=0):
        time_start = time.perf_counter()
        self.__check_measurement_range(measurement_range)
        wait_precise(self.hold_time)
        capacitance_name, conductance_name, status_name = [], [], []
        sweep_result = "\nNO., DC ,  CAP [F]  ,  COND [S]  ,  FORCE V TIME [s] , MEAS TIME [s]\n"  # todo : strip out from method
        for pts, volt_i in enumerate(self.bias_value):
            force_v_time = self.force_v_3570(volt_i)
            wait_precise(self.delay_time)
            meas_status, meas_time, capacitance, conductance = self.measure_cmu84_3570(measurement_range)
            capacitance_name.append(capacitance)
            conductance_name.append(conductance)
            status_name.append(meas_status)
            _, spot_meas_time, cap, cond = self.measure_cmu84_3570(measurement_range)
            sweep_result += f"{pts + 1}, {volt_i} ,{cap}, {cond}, {force_v_time}, {spot_meas_time}\n"
        time_stamp = time.perf_counter() - time_start
        status = 0
        # 4770 : capacitance name, conductance name, bias value name, status name
        # return status, time_stamp, capacitance_name, conductance_name, self.bias_value, status_name

        return status, time_stamp, sweep_result

    def compen_5250(self, freq_val, l_hptrx, l_usrhptrx_h, l_usrhptrx_l, l_usrhpcoax_h, l_usrhpcoax_l, raw_cap, raw_g):
        """

        :param freq_val: Measured frequency [Hz]
        :param l_hptrx: HP Triax Cable Length [m]
        :param l_usrhptrx_h: User Triax Cable Length (High) [m]
        :param l_usrhptrx_l: User Triax Cable Length (Low) [m]
        :param l_usrhpcoax_h: User Coax Cable Length (High) [m]
        :param l_usrhpcoax_l: User Coax Cable Length (Low) [m]
        :param raw_cap: R_c. Raw Capacitance Data
        :param raw_g: R_g. Raw Conductance Data
        :return: c_c: Compensated Capacitance Data; c_g: Compensated Conductance Data; sta: status
        """
        # Rlc_data:
        #               R [ohm]       L [H]         C [F]
        #       DATA1   74.65E-3,  140.00E-9,   58.44E-12  ! Frame Path 1
        #       DATA2   75.41E-3,   90.00E-9,   67.13E-12  ! Frame Path 2
        #       DATA3  231.41E-3,  450.00E-9,  178.85E-12  ! Card Path High
        #       DATA4  177.56E-3,  390.00E-9,  135.45E-12  ! Card Path Low
        #       DATA5  100.70E-3,  400.00E-9,   80.00E-12  ! HP Triax Cable   [/m]
        #       DATA6  100.70E-3,  400.00E-9,   80.00E-12  ! User Triax Cbl H [/m]
        #       DATA7  100.70E-3,  400.00E-9,   80.00E-12  ! User Triax Cbl L [/m]
        #       DATA8  114.00E-3,  544.00E-9,  130.00E-12  ! User Coax Cbl H  [/m]
        #       DATA9  114.00E-3,  544.00E-9,  130.00E-12  ! User Coax Cbl L  [/m]
        #       DATA10   0.00E-3,    0.00E-9,    1.20E-12  ! Stray Capacitance


        data1 = [0, 0.07465, 0.00000014, 0.00000000005844]
        data2 = [0, 0.07541, 0.00000009, 0.00000000006713]
        data3 = [0, 0.23141, 0.00000045, 0.00000000017885]
        data4 = [0, 0.17756, 0.00000039, 0.00000000013545]
        data5 = [0, 0.1007, 0.0000004, 0.00000000008]
        data6 = [0, 0.1007, 0.0000004, 0.00000000008]
        data7 = [0, 0.1007, 0.0000004, 0.00000000008]
        data8 = [0, 0.114, 0.000000544, 0.00000000013]
        data9 = [0, 0.114, 0.000000544, 0.00000000013]
        data10 = [0, 0, 0, 0.0000000000012]

        pi = 3.14159265359

        if l_hptrx < 0 or l_usrhptrx_h < 0 or l_usrhptrx_l < 0 or l_usrhpcoax_h < 0 or l_usrhpcoax_l < 0:
            sys.exit("*** Incorrect Input Parameter is not correct.")

        # Adjust Factor Value
        for i in range(1, 4):
            data5[i] = data5[i] * l_hptrx
            data6[i] = data5[i] * l_usrhptrx_h
            data7[i] = data5[i] * l_usrhptrx_l
            data8[i] = data5[i] * l_usrhpcoax_h
            data9[i] = data5[i] * l_usrhpcoax_l

        # Calculate Coef Value
        tmp1 = 2 * data1[3] + 4 * data2[3] + 2 * data3[3] + 2 * data4[3] + 4 * data5[3] + 2 * data6[3] \
               + 2 * data7[3] + 2 * data8[3] + 2 * data9[3]


        # tmp = 2
        tmp2 = 2 * data2[3] + 2 * data3[3] + 2 * data4[3] + 4 * data5[3] + 2 * data6[3] + 2 * data7[3] \
               + 2 * data8[3] + 2 * data9[3]
        tmp3 = 1 * data3[3] + 2 * data5[3] + 2 * data6[3] + 2 * data8[3]
        tmp4 = 1 * data4[3] + 2 * data5[3] + 2 * data7[3] + 2 * data9[3]
        tmp5 = 2 * data5[3] + 2 * data6[3] + 2 * data7[3] + 2 * data8[3] + 2 * data9[3]
        tmp6 = 1 * data6[3] + 2 * data8[3]
        tmp7 = 1 * data7[3] + 2 * data9[3]
        tmp8 = 1 * data8[3] * data8[2] + 1 * data9[3] * data9[2]


        coef = [54088, 54088, 54088, 54088, 54088]
        coef[0] = -2 * pi * pi * (
                tmp1 * data1[2] + tmp2 * data2[2] + tmp3 * data3[2] + tmp4 * data4[2] + tmp5 * data5[2] +
                tmp6 * data6[2] + tmp7 * data7[2] + tmp8)

        tmp8 = 1 * data8[3] * data8[1] + 1 * data9[3] * data9[1]

        coef[1] = pi * (
                tmp1 * data1[1] + tmp2 * data2[1] + tmp3 * data3[1] + tmp4 * data4[1] + tmp5 * data5[1] +
                tmp6 * data6[1] + tmp7 * data7[1] + tmp8)
        coef[2] = 2 * data1[1] + 2 * data2[1] + 1 * data3[1] + 1 * data4[1] + 2 * data5[1] + 1 * data6[1] + \
                  1 * data7[1] + 1 * data8[1] + 1 * data9[1]
        coef[3] = pi * (
                4 * data1[2] + 4 * data2[2] + 2 * data3[2] + 2 * data4[2] + 4 * data5[2] +
                2 * data6[2] + 2 * data7[2] + 2 * data8[2] + 2 * data9[2])
        coef[4] = data10[3]

        # Setup Complex Value
        y0_r = 0
        y0_i = 2 * pi * freq_val * coef[4]

        coef1_r = coef[0] * freq_val * freq_val + 1
        coef1_i = coef[1] * freq_val
        coef2_r = coef[2]
        coef2_i = coef[3] * freq_val

        # Convert : Cp-G to G-B[Ym]
        ym_r = raw_g
        ym_i = 2 * pi * freq_val * raw_cap

        # Compen : Ym to Yt
        ynof_r = ym_r - y0_r
        ynof_i = ym_i - y0_i

        nume_r = ynof_r * coef1_r - ynof_i * coef1_i
        nume_i = ynof_r * coef1_i + ynof_i * coef1_r
        deno_r = 1 - (ynof_r * coef2_r - ynof_i * coef2_i)
        deno_i = -1 * (ynof_r * coef2_i + ynof_i * coef2_r)
        yt_r = (nume_r * deno_r + nume_i * deno_i) / (deno_r * deno_r + deno_i * deno_i)
        yt_i = (nume_i * deno_r - nume_r * deno_i) / (deno_r * deno_r + deno_i * deno_i)

        # Convert : G-B[Yt] to Cp-G
        c_g = yt_r
        c_c = yt_i / (2 * pi * freq_val)

        return c_c, c_g

    def compen_2201(self, freq_val, l_hptrx, l_usrhptrx_h, l_usrhptrx_l, l_usrhpcoax_h, l_usrhpcoax_l, raw_cap, raw_g):
        """

        :param freq_val: Measured frequency [Hz]
        :param l_hptrx: HP Triax Cable Length [m]: default: 3m
        :param l_usrhptrx_h: User Triax Cable Length (High) [m]: default: 0m
        :param l_usrhptrx_l: User Triax Cable Length (Low) [m]: default: 0m
        :param l_usrhpcoax_h: User Coax Cable Length (High) [m]: default: 4m
        :param l_usrhpcoax_l: User Coax Cable Length (Low) [m]: default: 4m
        :param raw_cap: R_c. Raw Capacitance Data
        :param raw_g: R_g. Raw Conductance Data
        :return: c_c: Compensated Capacitance Data; c_g: Compensated Conductance Data; sta: status
        """
        if l_hptrx < 0 or l_usrhptrx_h < 0 or l_usrhptrx_l < 0 or l_usrhpcoax_h < 0 or l_usrhpcoax_l < 0:
            sys.exit("*** Incorrect Input Parameter is not correct.")

        pi = 3.14159265358979323846
        omega = 2.0 * pi * freq_val
        #               R [ohm]       L [H]         C [F]         omega
        # \\ccdata\b2211a\cable\triax
        mother_b = [0.0E+00, 5.25E-08, 2.94E-11, omega]
        # matrix_h_2210A = [2.43E+00, 6.31E-07, 1.93E-10, omega]
        # matrix_l_2210A = [2.49E+00, 5.87E-07, 1.92E-10, omega]
        matrix_h_2211A = [2.380000e+00, 7.290000e-07, 2.020000e-10, omega]
        matrix_l_2211A = [2.420000e+00, 6.950000e-07, 2.010000e-10, omega]

        cable1_15m = [1.511000e-01, 6.000000e-07, 1.200000e-10, omega]
        cable1_3m = [6.300000e-01, 1.250000e-06, 1.600000e-10, omega]
        cable1_4m = [8.400000e-01, 1.660000e-06, 2.130000e-10, omega]

        cable2_h = [1.007000e-01, 4.000000e-07, 8.000000e-11, omega]
        cable2_l = [1.007000e-01, 4.000000e-07, 8.000000e-11, omega]
        cable3_h = [1.140000e-01, 5.440000e-07, 1.300000e-10, omega]
        cable3_l = [1.140000e-01, 5.440000e-07, 1.300000e-10, omega]

        if Agilent_Triax_Cable == 4:
            cable1 = cable1_4m
        elif Agilent_Triax_Cable == 1.5:
            cable1 = cable1_15m
        else:
            cable1 = cable1_3m

        zm = self.czz.ccomp_divided_ccomp(1, 0, float(raw_g), float(raw_cap) * float(omega))

        fmat_0 = C22.fmatrix(mother_b[0], mother_b[1], mother_b[2], omega)
        fmat_1 = C22.fmatrix(matrix_h_2211A[0], matrix_h_2211A[1], matrix_h_2211A[2], omega)
        fmat_2 = C22.fmatrix(cable1[0], cable1[1], cable1[2], omega)
        fmat_3 = C22.fmatrix(cable2_h[0], cable2_h[1], cable2_h[2], omega)
        fmat_4 = C22.fmatrix(cable3_h[0], cable3_h[1], cable3_h[2], omega)

        fmat_5 = C22.fmatrix(mother_b[0], mother_b[1], mother_b[2], omega) # duplicate fmat_0

        fmat_6 = C22.fmatrix(matrix_l_2211A[0], matrix_l_2211A[1], matrix_l_2211A[2], omega)
        fmat_7 = C22.fmatrix(cable1[0], cable1[1], cable1[2], omega) # duplicate fmat_7
        fmat_8 = C22.fmatrix(cable2_l[0], cable2_l[1], cable2_l[2], omega)
        fmat_9 = C22.fmatrix(cable3_l[0], cable3_l[1], cable3_l[2], omega)

        # fh = fmat_0 * fmat_1 * fmat_2 * fmat_3 * fmat_4
        # fl = fmat_9 * fmat_8 * fmat_7 * fmat_6 * fmat_5
        fh_1 = C22.cc22_multiply_c22(fmat_0[0], fmat_0[1], fmat_0[2], fmat_0[3],
                                     fmat_1[0], fmat_1[1], fmat_1[2], fmat_1[3])
        fh_2 = C22.cc22_multiply_c22(fh_1[0], fh_1[1], fh_1[2], fh_1[3],
                                     fmat_2[0], fmat_2[1], fmat_2[2], fmat_2[3])
        fh_3 = C22.cc22_multiply_c22(fh_2[0], fh_2[1], fh_2[2], fh_2[3],
                                     fmat_3[0], fmat_3[1], fmat_3[2], fmat_3[3])
        fh_4 = C22.cc22_multiply_c22(fh_3[0], fh_3[1], fh_3[2], fh_3[3],
                                     fmat_4[0], fmat_4[1], fmat_4[2], fmat_4[3])

        fl_1 = C22.cc22_multiply_c22(fmat_9[0], fmat_9[1], fmat_9[2], fmat_9[3],
                                     fmat_8[0], fmat_8[1], fmat_8[2], fmat_8[3])
        fl_2 = C22.cc22_multiply_c22(fl_1[0], fl_1[1], fl_1[2], fl_1[3],
                                     fmat_7[0], fmat_7[1], fmat_7[2], fmat_7[3])
        fl_3 = C22.cc22_multiply_c22(fl_2[0], fl_2[1], fl_2[2], fl_2[3],
                                     fmat_6[0], fmat_6[1], fmat_6[2], fmat_6[3])
        fl_4 = C22.cc22_multiply_c22(fl_3[0], fl_3[1], fl_3[2], fl_3[3],
                                     fmat_5[0], fmat_5[1], fmat_5[2], fmat_5[3])

        # Zt = (Fh.a * Fl.b + Fh.b * Fl.d + Zm) / (Fh.a * Fl.d)
        zt_1 = CComp.ccomp_multiply_ccomp(fh_4[0][0], fh_4[0][1], fl_4[1][0], fl_4[1][1])
        zt_2 = CComp.ccomp_multiply_ccomp(fh_4[1][0], fh_4[1][1], fl_4[3][0], fl_4[3][1])
        zt_3 = CComp.ccomp_plus_ccomp(zt_1[0], zt_1[1], zt_2[0], zt_2[1])
        zt_4 = CComp.ccomp_plus_ccomp(zt_3[0], zt_3[1], zm[0], zm[1])
        zt_5 = CComp.ccomp_multiply_ccomp(fh_4[0][0], fh_4[0][1], fl_4[3][0], fl_4[3][1])
        zt_6 = self.czz.ccomp_divided_ccomp(zt_4[0], zt_4[1], zt_5[0], zt_5[1])

        temp = self.czz.ccomp_divided_ccomp(1, 0, zt_6[0], zt_6[1])

        c_c = temp[1] / omega
        c_g = temp[0]
        print (f"temp1 : {temp[1] } /  {omega}")
        return c_c, c_g

    def diasble(self):
        self.lcr.close()

    def __check_signal_level(self, signal_level_):
        error_message = f"*** ErrorInput signal level '{signal_level_}' is wrong , set to default (30mV)."
        if signal_level_ < 0 or signal_level_ > 20:
            sys.exit(error_message)
        else:
            return str(signal_level_)

    def __check_integration_time(self, integration_time_):
        integration_modes = {0: "FAST", 1: "MED", 2: "SLOW", 3: "SLOW2"}  # 4770 : 1:SHOR , 2:MED , 3:LONG
        try:
            return integration_modes[integration_time_]
        except KeyError:
            error_message = f"*** ErrorInput Integration is wrong {integration_time_}."
            sys.exit(error_message)

    def __check_measurement_range(self, range_):

        if range_ != self.measurement_range:
            if range_ in range(0, 13):
                if range_ == 0:
                    self.iwrite(":RANG:AUTO ON")
                else:
                    self.iwrite(":RANG:AUTO OFF")
                    self.iwrite(f":RANG {range_}")
                self.measurement_range = range_
            else:
                sys.exit("***measurement range Error")

    def __check_frequency(self, frequency_):
        if 20 < frequency_ < 2e6:
            return frequency_
        else:
            sys.exit("frequency ErrorIncorrect Input Frequency Range:20Hz to 2MHz")

    def __check_voltage(self, volt_):
        if abs(volt_) > 41:
            error_message = f"*** ERROR : Voltage '{volt_}' out of range -40~40V"
            sys.exit(error_message)

    def __check_voltage_0(self, *volts_):
        # checking program of set_iv, force_v
        for volt_i in volts_:
            if abs(volt_i) > 41:
                error_message = "*** ERROR : Voltage '{}' out of range -40~40V".format(volt_i)
                sys.exit(error_message)
        return volts_

    def __check_time(self, time_):
        if time_ < 0 or time_ > 651:
            error_message = f"*** ERROR : Given time '{time_}' is out of range 0~650"
            sys.exit(error_message)

    def __check_time0(self, *times_):

        # Check the delay and hold time, obey by the device limit
        for time_i in list(*times_):
            print(time_i)
            if time_i < 0 or time_i > 651:
                error_message = "*** ERROR : Given time '{}' is out of range 0~650".format(time_i)
                sys.exit(error_message)

    def __step_aperture(self, start_volt_, stop_volt_, steps_number_: int):
        # @return: Dc bias string's list ["point1_v", "point2_v", ....]
        if steps_number_ not in range(2, 1001):
            sys.exit("*** ERROR :  Input Step_Number is out of range 2~1001.".format(steps_number_))
        steps_lst_ = abs((stop_volt_ - start_volt_) / (steps_number_ - 1))
        return ["{0:.3f}".format(x) for x in np.arange(start_volt_, stop_volt_ + steps_lst_, steps_lst_)]


class ExternalDCSupply:
    def __init__(self):
        rm = pyvisa.ResourceManager()
        self.dc_supply = rm.open_resource("GPIB0::4::INSTR")
        self.is_connect()

    def is_connect(self):
        # Check connection of external DC supply
        idn = self.dc_supply.query("*IDN?")

        if idn != "":  # todo : check IDN of b2902
            self.dc_supply.write("*RST")
            self.dc_supply.write(":SOUR1:FUNC:MODE VOLT")
            self.dc_supply.write(":SOUR1:VOLT:RANG:AUTO ON")
            self.dc_supply.write(":SENS1:CURR:PROT 0.001")
            return True  # Connected
        else:
            return False  # Connect failed

    def force_v(self, output_voltage):
        if abs(float(output_voltage)) < 41:
            self.dc_supply.write(f":SOUR1:VOLT {output_voltage}")
            self.dc_supply.write(":OUTP1 ON")
            # check the voltage is ready?
            ask = self.dc_supply.query(":SOUR1:VOLT?")
            set = float(ask)
            output_voltage = float(output_voltage)
            # print (f"type set:{type(set)}, {type(output_voltage)}, value: {set}, {output_voltage}")
            while set == output_voltage:
                # print (f"{set},{output_voltage} ")
                break
            # while ask == str(output_voltage):
            #     print (ask)
            #     pass
            # while True:
            #     if self.dc_supply.query(":SOUR1:VOLT?") != str(output_voltage):
            #         print ("HELLO")






class OfflineHiokiIM3570(OnlineHiokiIM3570):
    def __init__(self, lcr_visa_resource, lcr_visa_addr, dc_supply=None):
        pass
        # super().__init__(lcr_visa_resource, lcr_visa_addr, dc_supply)

    def iprompt(self, command):
        time_start = time.perf_counter()
        print(command)
        time_stamp = time.perf_counter() - time_start
        return time_stamp

    def iwrite(self, command):
        time_start = time.perf_counter()
        print(command)
        time_stamp = time.perf_counter() - time_start
        return time_stamp

    def is_connect(self):
        return super().is_connect()

    def init_instrument(self):
        print("system_init")

    def set_freq_3570(self, frequency=9999999):
        return super().set_freq_3570(frequency)

    def set_cmu84_3570(self, integration_time: int, signal_level=0.03):
        return super().set_cmu84_3570(integration_time, signal_level)

    def force_v_3570(self, output_voltage):
        return super().force_v_3570(output_voltage)

    def set_cv84_3570(self, start_voltage, stop_voltage, steps_number, hold_time=0, delay_time=0):
        return super().set_cv84_3570(start_voltage, stop_voltage, steps_number, hold_time, delay_time)

    def measure_cmu84_3570(self, measurement_range=0):
        return super().measure_cmu84_3570(measurement_range)

    def sweep_cv84_3570(self, measurement_range=0):
        return super().sweep_cv84_3570(measurement_range)



lcr = OnlineHiokiIM3570()
lcr.set_cmu84_3570(2,0.05)



# print ("\n1M" )
# lcr.set_freq_3570(1E6)
# a = lcr.measure_cmu84_3570()
# time.sleep(2)

print ("\n100k" )
lcr.set_freq_3570(1E5)
b = lcr.measure_cmu84_3570()
# time.sleep(2)
#
# print ("\n10K")
# lcr.set_freq_3570(1E4)
# c = lcr.measure_cmu84_3570()
#
# print ("\n1K")
# lcr.set_freq_3570(1E3)
# d = lcr.measure_cmu84_3570()
#
# print ("\n### descending ###\n")
#
#
# print ("\n1K")
# lcr.set_freq_3570(1E3)
# dd = lcr.measure_cmu84_3570()
#
#
# print ("\n10K")
# lcr.set_freq_3570(1E4)
# cc = lcr.measure_cmu84_3570()
#
#
# print ("\n100k" )
# lcr.set_freq_3570(1E5)
# bb = lcr.measure_cmu84_3570()
# time.sleep(2)
#
#
# print ("\n1M" )
# lcr.set_freq_3570(1E6)
# aa = lcr.measure_cmu84_3570()
# time.sleep(2)
#
lcr.diasble()
#
