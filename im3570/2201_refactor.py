import cmath


freq_val = 1E6

omega = 2.0 * cmath.pi * freq_val
#               R [ohm]       L [H]         C [F]         omega
# \\ccdata\b2211a\cable\triax
mother_b = [0.0E+00, 5.25E-08, 2.94E-11]
# matrix_h_2210A = [2.43E+00, 6.31E-07, 1.93E-10]
# matrix_l_2210A = [2.49E+00, 5.87E-07, 1.92E-10]
matrix_h_2211A = [2.380000e+00, 7.290000e-07, 2.020000e-10]
matrix_l_2211A = [2.420000e+00, 6.950000e-07, 2.010000e-10]

cable1_15m = [1.511000e-01, 6.000000e-07, 1.200000e-10]
cable1_3m = [6.300000e-01, 1.250000e-06, 1.600000e-10]
cable1_4m = [8.400000e-01, 1.660000e-06, 2.130000e-10]

cable2_h = [1.007000e-01, 4.000000e-07, 8.000000e-11]
cable2_l = [1.007000e-01, 4.000000e-07, 8.000000e-11]
cable3_h = [1.140000e-01, 5.440000e-07, 1.300000e-10]
cable3_l = [1.140000e-01, 5.440000e-07, 1.300000e-10]
raw_cap = complex(8.08173e-12, 8.140684e-06)
pure_real_one = complex(1,0)
zm = pure_real_one / (raw_cap * omega)

mat_mother_board = C22.fmatrix()
"""
if Agilent_Triax_Cable == 4:
    cable1 = cable1_4m
elif Agilent_Triax_Cable == 1.5:
    cable1 = cable1_15m
else:
    cable1 = cable1_3m

zm = self.czz.ccomp_divided_ccomp(1, 0, float(raw_g), float(raw_cap) * float(omega))

fmat_0 = C22.fmatrix(mother_b[0], mother_b[1], mother_b[2], omega)
fmat_1 = C22.fmatrix(matrix_h_2211A[0], matrix_h_2211A[1], matrix_h_2211A[2], omega)
fmat_2 = C22.fmatrix(cable1[0], cable1[1], cable1[2], omega)
fmat_3 = C22.fmatrix(cable2_h[0], cable2_h[1], cable2_h[2], omega)
fmat_4 = C22.fmatrix(cable3_h[0], cable3_h[1], cable3_h[2], omega)

fmat_5 = C22.fmatrix(mother_b[0], mother_b[1], mother_b[2], omega)  # duplicate fmat_0

fmat_6 = C22.fmatrix(matrix_l_2211A[0], matrix_l_2211A[1], matrix_l_2211A[2], omega)
fmat_7 = C22.fmatrix(cable1[0], cable1[1], cable1[2], omega)  # duplicate fmat_7
fmat_8 = C22.fmatrix(cable2_l[0], cable2_l[1], cable2_l[2], omega)
fmat_9 = C22.fmatrix(cable3_l[0], cable3_l[1], cable3_l[2], omega)

# fh = fmat_0 * fmat_1 * fmat_2 * fmat_3 * fmat_4
# fl = fmat_9 * fmat_8 * fmat_7 * fmat_6 * fmat_5
fh_1 = C22.cc22_multiply_c22(fmat_0[0], fmat_0[1], fmat_0[2], fmat_0[3],
                             fmat_1[0], fmat_1[1], fmat_1[2], fmat_1[3])
fh_2 = C22.cc22_multiply_c22(fh_1[0], fh_1[1], fh_1[2], fh_1[3],
                             fmat_2[0], fmat_2[1], fmat_2[2], fmat_2[3])
fh_3 = C22.cc22_multiply_c22(fh_2[0], fh_2[1], fh_2[2], fh_2[3],
                             fmat_3[0], fmat_3[1], fmat_3[2], fmat_3[3])
fh_4 = C22.cc22_multiply_c22(fh_3[0], fh_3[1], fh_3[2], fh_3[3],
                             fmat_4[0], fmat_4[1], fmat_4[2], fmat_4[3])

fl_1 = C22.cc22_multiply_c22(fmat_9[0], fmat_9[1], fmat_9[2], fmat_9[3],
                             fmat_8[0], fmat_8[1], fmat_8[2], fmat_8[3])
fl_2 = C22.cc22_multiply_c22(fl_1[0], fl_1[1], fl_1[2], fl_1[3],
                             fmat_7[0], fmat_7[1], fmat_7[2], fmat_7[3])
fl_3 = C22.cc22_multiply_c22(fl_2[0], fl_2[1], fl_2[2], fl_2[3],
                             fmat_6[0], fmat_6[1], fmat_6[2], fmat_6[3])
fl_4 = C22.cc22_multiply_c22(fl_3[0], fl_3[1], fl_3[2], fl_3[3],
                             fmat_5[0], fmat_5[1], fmat_5[2], fmat_5[3])

# Zt = (Fh.a * Fl.b + Fh.b * Fl.d + Zm) / (Fh.a * Fl.d)
zt_1 = CComp.ccomp_multiply_ccomp(fh_4[0][0], fh_4[0][1], fl_4[1][0], fl_4[1][1])
zt_2 = CComp.ccomp_multiply_ccomp(fh_4[1][0], fh_4[1][1], fl_4[3][0], fl_4[3][1])
zt_3 = CComp.ccomp_plus_ccomp(zt_1[0], zt_1[1], zt_2[0], zt_2[1])
zt_4 = CComp.ccomp_plus_ccomp(zt_3[0], zt_3[1], zm[0], zm[1])
zt_5 = CComp.ccomp_multiply_ccomp(fh_4[0][0], fh_4[0][1], fl_4[3][0], fl_4[3][1])
zt_6 = self.czz.ccomp_divided_ccomp(zt_4[0], zt_4[1], zt_5[0], zt_5[1])

temp = self.czz.ccomp_divided_ccomp(1, 0, zt_6[0], zt_6[1])

c_c = temp[1] / omega
c_g = temp[0]
print(f"temp1 : {temp[1]} /  {omega}")
return c_c, c_g
"""
