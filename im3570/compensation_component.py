class CComp:

    # def __init__(self, r, i):
    #     self.r = float(r)
    #     self.i = float(i)

    @staticmethod
    def ccomp_void(r, i):
        """

        :param r:
        :param i:
        :return:
        """
        return 0, 0

    @staticmethod
    def ccomp_double_real(r, i):
        """

        :param r:
        :param i:
        :return:
        """
        r = r
        i = i

        return r, i

    @staticmethod
    def ccomp_real(r, i):
        """

        :param r:
        :param i:
        :return:
        """
        r = r

        return r, 0

    @staticmethod
    def void_equals_ccomp(r, i):
        """
        等於
        :param r:
        :param i:
        :return:
        """
        r = r
        i = i

    @staticmethod
    def ccomp_multiply_real(r, i, f):
        """
        相乘
        :param r:
        :param i:
        :param f:
        :return:
        """
        r = r * f
        i = i * f

        return r, i

    @staticmethod
    def ccomp_divided_real(r, i, f):
        """
        相除
        :param r:
        :param i:
        :param f:
        :return:
        """
        r = r / f
        i = i / f

        return r, i

    @staticmethod
    def ccomp_multiply_ccomp(r1, i1, r2, i2):
        """
        相乘
        :param r1:
        :param i1:
        :param r2:
        :param i2:
        :return:
        """
        r = r1 * r2 - i1 * i2
        i = i1 * r2 + r1 * i2

        return r, i

    @staticmethod
    def ccomp_plus_ccomp(r1, i1, r2, i2):
        """
        相加
        :param r1:
        :param i1:
        :param r2:
        :param i2:
        :return:
        """
        r = r1 + r2
        i = i1 + i2

        return r, i

    @staticmethod
    def ccomp_minus_ccomp(r1, i1, r2, i2):
        """
        相減
        :param r1:
        :param i1:
        :param r2:
        :param i2:
        :return:
        """
        r = r1 - r2
        i = i1 - i2

        return r, i

    @staticmethod
    def real_abs2_ccomp(r, i):
        """
        friend
        :param r:
        :param i:
        :return:
        """
        abs2 = r * r + i * i

        return abs2

    @staticmethod
    def ccomp_conju_ccomp(r, i):
        """
        friend
        :param r:
        :param i:
        :return:
        """
        r = r
        i = -i

        return r, i

    def ccomp_divided_ccomp(self, r1, i1, r2, i2):
        """
        相除
        return((*this) * conju(f) / abs2(f));
        :param r1:
        :param i1:
        :param r2:
        :param i2:
        :return:
        """
        abs2 = self.real_abs2_ccomp(r2, i2)
        conju = self.ccomp_conju_ccomp(r2, i2)
        multiply = self.ccomp_multiply_ccomp(r1, i1, conju[0], conju[1])
        divided = self.ccomp_divided_real(multiply[0], multiply[1], abs2)
        r = divided[0]
        i = divided[1]

        return r, i


class C22:

    # def __init__(self, r, i):
    #     self.r = float(r)
    #     self.i = float(i)

    @staticmethod
    def cc22_multiply_c22(a1, b1, c1, d1, a2, b2, c2, d2):
        """
        相乘
        temp.a = a * f.a + b * f.c;
        temp.b = a * f.b + b * f.d;
        temp.c = c * f.a + d * f.c;
        temp.d = c * f.b + d * f.d;
        :param a1:
        :param b1:
        :param c1:
        :param d1:
        :param a2:
        :param b2:
        :param c2:
        :param d2:
        :return:
        """

        a_1 = CComp.ccomp_multiply_ccomp(a1[0], a1[1], a2[0], a2[1])
        a_2 = CComp.ccomp_multiply_ccomp(b1[0], b1[1], c2[0], c2[1])
        a_3 = CComp.ccomp_plus_ccomp(a_1[0], a_1[1], a_2[0], a_2[1])

        b_1 = CComp.ccomp_multiply_ccomp(a1[0], a1[1], b2[0], b2[1])
        b_2 = CComp.ccomp_multiply_ccomp(b1[0], b1[1], d2[0], d2[1])
        b_3 = CComp.ccomp_plus_ccomp(b_1[0], b_1[1], b_2[0], b_2[1])

        c_1 = CComp.ccomp_multiply_ccomp(c1[0], c1[1], a2[0], a2[1])
        c_2 = CComp.ccomp_multiply_ccomp(d1[0], d1[1], c2[0], c2[1])
        c_3 = CComp.ccomp_plus_ccomp(c_1[0], c_1[1], c_2[0], c_2[1])

        d_1 = CComp.ccomp_multiply_ccomp(c1[0], c1[1], b2[0], b2[1])
        d_2 = CComp.ccomp_multiply_ccomp(d1[0], d1[1], d2[0], d2[1])
        d_3 = CComp.ccomp_plus_ccomp(d_1[0], d_1[1], d_2[0], d_2[1])

        a = a_3
        b = b_3
        c = c_3
        d = d_3

        return a, b, c, d

    @staticmethod
    def void_equals_c22(a, b, c, d):
        """
        等於
        :param a:
        :param b:
        :param c:
        :param d:
        :return:
        """
        a = a
        b = b
        c = c
        d = d
        return a, b, c, d

    @staticmethod
    def cc22_plus_c22(a1, b1, c1, d1, a2, b2, c2, d2):
        """
        相加
        temp.a = a + f.a;
        temp.b = b + f.b;
        temp.c = c + f.c;
        temp.d = d + f.d;
        :param a1:
        :param b1:
        :param c1:
        :param d1:
        :param a2:
        :param b2:
        :param c2:
        :param d2:
        :return:
        """
        a = CComp.ccomp_plus_ccomp(a1[0], a1[1], a2[0], a2[1])
        b = CComp.ccomp_plus_ccomp(b1[0], b1[1], b2[0], b2[1])
        c = CComp.ccomp_plus_ccomp(c1[0], c1[1], c2[0], c2[1])
        d = CComp.ccomp_plus_ccomp(d1[0], d1[1], d2[0], d2[1])

        return a, b, c, d

    @staticmethod
    def cc22_minus_c22(a1, b1, c1, d1, a2, b2, c2, d2):
        """
        相減
        :param a1:
        :param b1:
        :param c1:
        :param d1:
        :param a2:
        :param b2:
        :param c2:
        :param d2:
        :return:
        """
        a = CComp.ccomp_minus_ccomp(a1[0], a1[1], a2[0], a2[1])
        b = CComp.ccomp_minus_ccomp(b1[0], b1[1], b2[0], b2[1])
        c = CComp.ccomp_minus_ccomp(c1[0], c1[1], c2[0], c2[1])
        d = CComp.ccomp_minus_ccomp(d1[0], d1[1], d2[0], d2[1])

        return a, b, c, d

    @staticmethod
    def cc22_multiply_ccomp(a1, b1, c1, d1, r):
        """
        相乘
        :param a1:
        :param b1:
        :param c1:
        :param d1:
        :param r:
        :return:
        """
        a = a1 * r
        b = b1 * r
        c = c1 * r
        d = d1 * r

        a = CComp.ccomp_multiply_ccomp(a1[0], a1[1], r[0], r[1])
        b = CComp.ccomp_multiply_ccomp(b1[0], b1[1], r[0], r[1])
        c = CComp.ccomp_multiply_ccomp(c1[0], c1[1], r[0], r[1])
        d = CComp.ccomp_multiply_ccomp(d1[0], d1[1], r[0], r[1])

        return a, b, c, d

    @staticmethod
    def cc22_multiply_real(a1, b1, c1, d1, f):
        """
        相乘
        :param a1:
        :param b1:
        :param c1:
        :param d1:
        :param f:
        :return:
        """
        a = CComp.ccomp_multiply_real(a1[0], a1[1], f)
        b = CComp.ccomp_multiply_real(b1[0], b1[1], f)
        c = CComp.ccomp_multiply_real(c1[0], c1[1], f)
        d = CComp.ccomp_multiply_real(d1[0], d1[1], f)

        return a, b, c, d

    @staticmethod
    def fmatrix(r, l, c, w):
        """
        CComp t1(R, w * L), t2(0, w * C)
        temp.a = CComp(1, 0) + t1 * t2 / 2
        temp.b = t1 * t1 * t2 / (-4) - t1
        temp.c = t2 * (-1)
        temp.d = temp.a
        :param r:
        :param l:
        :param c:
        :param w:
        :return:
        """
        t1 = [r, w * l] # z  = R = z + xl j
        t2 = [0, w * c] # xc

        # a = CComp.ccomp_divided_real(CComp.ccomp_multiply_ccomp(CComp.ccomp_plus_ccomp(1, 0, t1[0], t1[1])[0],
        #                                                         CComp.ccomp_plus_ccomp(1, 0, t1[0], t1[1])[1],
        #                                                         t2[0], t2[1])[0],
        #                              CComp.ccomp_multiply_ccomp(CComp.ccomp_plus_ccomp(1, 0, t1[0], t1[1])[0],
        #                                                         CComp.ccomp_plus_ccomp(1, 0, t1[0], t1[1])[1],
        #                                                         t2[0], t2[1])[1],
        #                              2)
        a_multiply = CComp.ccomp_multiply_ccomp(t1[0], t1[1], t2[0], t2[1])  # 相乘
        a_divided = CComp.ccomp_divided_real(a_multiply[0], a_multiply[1], 2)  # 相乘後實部*2 = multi_n_divided_2
        a_plus = CComp.ccomp_plus_ccomp(1, 0, a_divided[0], a_divided[1])  # complex (1,0j) + multi_n_divided_2
        a = a_plus
        b_multiply_1 = CComp.ccomp_multiply_ccomp(t1[0], t1[1], t1[0], t1[1])
        b_multiply_2 = CComp.ccomp_multiply_ccomp(b_multiply_1[0], b_multiply_1[1], t2[0], t2[1])
        b_divided = CComp.ccomp_divided_real(b_multiply_2[0], b_multiply_2[1], (-4))
        b_minus = CComp.ccomp_minus_ccomp(b_divided[0], b_divided[1], t1[0], t1[1])
        b = b_minus
        c = CComp.ccomp_multiply_real(t2[0], t2[1], (-1))
        d = a

        return a, b, c, d

