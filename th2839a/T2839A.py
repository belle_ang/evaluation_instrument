import numpy as np
import time
from pytic.commondriver import *
from pytic.tdriver.driver_setting import *
from pytic.tdriver import TISError
import pyvisa


def wait_time(sleep_time):
    start_time2sleep = time.perf_counter()
    while time.perf_counter() < start_time2sleep + sleep_time:
        pass


class OnlineTH2839A( ):
    def __init__(self , visa_resouce, visa_address):
        super(OnlineTH2839A,self).__init__()
        rm = pyvisa.ResourceManager ()
        self.INST_ADDRESS = "USB0::0x0471::0x2839::QF40900002::0::INSTR"
        self.lcr = rm.open_resource(self.INST_ADDRESS)
        self.init_system()

    def init_system(self):
        start_time = time.perf_counter()
        self.lcr.write("*RST")
        self.lcr.write("FUNC:IMP CPG")
        self.lcr.write("FUNC:IMP:RANG:AUTO ON")
        self.lcr.write("BIAS:STAT OFF")
        self.lcr.write("BIAS:VOLT 0")
        time_stamp = time.perf_counter() - start_time
        return time_stamp

    def _check_frequency (self, frequency_):
        if frequency_ < 20 or frequency_ > 2E6:
            raise TISError ("*** set_freq, frequency Error")
        else:
            return str (frequency_)

    def set_frequency_th2839a(self, frequency=999999):
        start_time = time.perf_counter ()
        frequency = self._check_frequency(frequency)
        self.lcr.write("FREQ " + frequency)
        time_stamp = time.perf_counter () - start_time
        return time_stamp


    def _check_integration(self, integration_):
        integration_mode = {1:"FAST", 2:"MED", 3:"SLOW"}
        try:
            return integration_mode[integration_]
        except KeyError:
            error_message = "*** ErrorInput Integration is wrong {}.".format (integration_)
            raise TISError (error_message)


    def _check_signal_level(self, signal_level):
        if signal_level < 0.05 or signal_level > 2.1:
            raise TISError ("*** set_cmu signal_level range error")
        else:
            return signal_level


    def set_cmu84_th2839a(self, integration, signal_level):
        start_time = time.perf_counter()
        integration = self._check_integration(integration)
        signal_level = self._check_signal_level(signal_level)
        self.lcr.write("VOLT {}".format(signal_level))
        self.lcr.write("APER {}".format(integration))
        time_stamp = time.perf_counter() - start_time
        return time_stamp


    def _check_voltage(self,voltage_):
        if abs(voltage_) > 41:
            raise TISError("*** force_v , voltage out of range")
        else:
            return str(voltage_)


    def force_v_th2839a(self,output_range=0, output_voltage=0, current_compliance=0):
        start_time = time.perf_counter ()
        output_voltage = self._check_voltage(output_voltage)
        self.lcr.write ("BIAS:STAT ON")
        self.lcr.write ("BIAS:VOLT " + output_voltage)
    
        time_stamp = time.perf_counter () - start_time
        return time_stamp

    def _check_measurrement_range(self, measurement_range_):
        if measurement_range_ in range[0, 10, 30, 100, 300, 1E3, 3E3, 1E4, 3E4, 1E5]:
            if measurement_range_ == 0:
                self.lcr.write ("FUNC:IMP:RANG:AUTO ON")
            else:
                self.lcr.write ("FUNC:IMP:RANG:AUTO OFF")
                self.lcr.write ("FUNC:IMP:RANG " + str(measurement_range_))
        else:
            raise TISError("*** measurement range Error")

    def measure_cmu84_th2839a(self, measurement_range=0):

        start_time = time.perf_counter()
        # self._check_measurement_range(measurement_range)
        # measure_read =  self.prompt("TRIG;:FETC?") # MUST SEND TOGETHER
        try :
            return time.perf_counter() - start_time, self.prompt("TRIG;:FETC?")
            # return "{}, {}, {}, {}".format (int(status_), np.float64(capacitance_), np.float64(conductance_))
        except:
            raise TISError ("Failed to measure.")


    def _check_step_number(self, start_voltage_, stop_voltage_, step_number_):
        point_aperture_ = (stop_voltage_ - start_voltage_) / (step_number_ -1 )
        # return (str([x for x in np.arange(start_voltage_, stop_voltage_ + point_aperture_, point_aperture_)]).strip('[]'))
        return [x for x in np.arange (start_voltage_, stop_voltage_ + point_aperture_, point_aperture_)]


    def _check_time(self, time_):
        if time_ < 0 or time_ > 650.001:
            raise  TISError("*** set_cv, delay or hold time error.")
        else:
            return time_

    def set_cv84_th2839a(self, start_voltage, stop_voltage, step_number, hold_time, delay_time):
        start_time = time.perf_counter()
        start_voltage = self._check_voltage(start_voltage)
        stop_voltage = self._check_voltage(stop_voltage)
        self.bias_points = self._check_step_number(start_voltage, stop_voltage , step_number)
        self.hold_time = hold_time
        self.delay_time = delay_time
        #self.lcr.write ("TRIG:DEL {}".format (hold_time))
        #self.lcr.write ("FUNC:SDEL {}".format (delay_time)) #todo
        #self.lcr.write ("LIST:MODE SEQ")
        #self.lcr.write ("LIST:BIAS:VOLT {}".format (bias_points))
        return time.perf_counter() - start_time

    def sweep_cv84_th2839a(self, measurement_range = 0):

        start_time = time.perf_counter()

        # self._check_measurement_range (measurement_range)
        wait_time (self.hold_time)
        sweep_result = '\nNO., DC , STATUS,  CAP [F]  ,  COND [S]  ,  TIME [s]\n'
        for pts, volt_i in enumerate (self.bias_points):
            wait_time (self.delay_time)
            # COMMAND SNIFF FROM SOFTWARE
            capacitance_, conductance_, status_ = self.prompt("BIAS:VOLT {};:TRIG;:FETCH?".format(volt_i)).split(",")
            # lst = [pts + 1, volt_i, np.float64(capacitance_), np.float64(conductance_), int(status_), meas_t2 - meas_t1]
            # print ("{}, {} ,{}, {}, {}, {}\n".format (pts + 1, volt_i, np.float64(capacitance_), np.float64(conductance_), int(status_), meas_t2 - meas_t1))
            sweep_result += "{}, {} ,{}, {}, {}\n".format (pts + 1, volt_i, np.float64(capacitance_), np.float64(conductance_), int(status_))

        return time.perf_counter() - start_time, sweep_result,


class OfflineTH2839A(OnlineTH2839A):
    def __init__(self, lcr_visa_resource, lcr_visa_addr,):
        super(OfflineTH2839A, self).__init__(lcr_visa_resource, lcr_visa_addr)


        




