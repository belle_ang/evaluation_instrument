import sys
import time
import numpy as np
import pyvisa
import functools

time_record = []

"""
"""


def track_time(func):
    @functools.wraps(func)
    def time_wrapper(*arg, **kwargs):
        start_time = time.perf_counter()
        ret = func(*arg, **kwargs)
        timestamp = time.perf_counter() - start_time
        return ret

    return time_wrapper


def wait_precise(wait_time):
    wait_time_now = time.perf_counter() + wait_time
    while time.perf_counter() < wait_time_now:
        pass
    return


class Mircotest6632:
    def __init__(self):
        self.lcr = pyvisa.ResourceManager().open_resource("GPIB0::2::INSTR")
        self.time_tracking = {}
        self.init_instrument()
        self.measurement_range = 0

    def iprompt(self, command):
        start_time = time.perf_counter()
        query_read = self.lcr.query(command)
        self.time_tracking[command] = time.perf_counter() - start_time
        return query_read

    def iwrite(self, command):
        start_time = time.perf_counter()
        self.lcr.write(command)
        self.time_tracking[command] = time.perf_counter() - start_time

    def is_connect(self):
        return True if self.iprompt("*IDN?") != "" else False

    def init_instrument(self):
        self.iwrite("*RST")
        self.iwrite(":MEAS:PARA CP,OFF,G,OFF")
        # Measure TRIGGER MODE(原本是repeat)
        #self.iwrite("MEAS:TRIG:MODE SING")

    def set_freq_3570(self, frequency=9999999):
        self.frequency = self.__check_frequency(frequency)
        self.iwrite(f":MEAS:FREQ {frequency}")

    def set_cmu84_3570(self, integration_time: int, signal_level=0.03):
        integration_time = self.__check_integration_time(integration_time)
        signal_level = self.__check_signal_level(signal_level)
        self.iwrite(f":MEAS:SPEE {integration_time}")
        self.iwrite(f":MEAS:CURR:AC {signal_level}")

    def force_v_3570(self, output_voltage):  # OK
        output_voltage = self.__check_voltage(output_voltage)
        self.iwrite(f":MEAS:BIAS:VOL {output_voltage}")
        self.iwrite(f":MEAS:BIAS:STAT ON")

    def set_cv84_3570(self, start_voltage, stop_voltage, steps_number, hold_time=0, delay_time=0):
        """
        # set the trigger delay
        :MEAS:TRIG:DEL {delay_time} , 0.000~5.000
        # set the ac./dc delay


        """
        self.__check_voltage(start_voltage)
        self.__check_voltage(stop_voltage)

        # self.iwrite(f"MEAS:TRIG:DELAY {delay_time}") # 0.000 to 5.000 # todo
        self.iwrite(f"SWE:STAR {start_voltage}")
        self.iwrite(f"SWE:STAR {stop_voltage}")
        self.iwrite(f"MEAS:DELAY {delay_time}")  # 0.000 to 5.000

    def measure_cmu84_3570(self, measurement_range=0):
        self.__check_measurement_range(measurement_range)
        #  status_, capacitance, conductance = self.lcr.query("*TRG?").split(",")
        capacitance, _, conductance, _ = self.lcr.query("*TRG?").split(",")  # :TRIG? , this two are recommend
        capacitance, conductance = float(capacitance), float(conductance)
        print(f"original capacitance :{capacitance},  conductance: {conductance}")

    def sweep_cv84_3570(self, measurement_range=0):
        time_start = time.perf_counter()
        self.__check_measurement_range(measurement_range)
        wait_precise(self.hold_time)
        capacitance_name, conductance_name, status_name = [], [], []
        sweep_result = "\nNO., DC ,  CAP [F]  ,  COND [S]  ,  FORCE V TIME [s] , MEAS TIME [s]\n"  # todo : strip out from method
        for pts, volt_i in enumerate(self.bias_value):
            force_v_time = self.force_v_3570(volt_i)
            wait_precise(self.delay_time)
            meas_status, meas_time, capacitance, conductance = self.measure_cmu84_3570(measurement_range)
            capacitance_name.append(capacitance)
            conductance_name.append(conductance)
            status_name.append(meas_status)
            _, spot_meas_time, cap, cond = self.measure_cmu84_3570(measurement_range)
            sweep_result += f"{pts + 1}, {volt_i} ,{cap}, {cond}, {force_v_time}, {spot_meas_time}\n"
        time_stamp = time.perf_counter() - time_start
        status = 0
        # 4770 : capacitance name, conductance name, bias value name, status name
        # return status, time_stamp, capacitance_name, conductance_name, self.bias_value, status_name

        return status, time_stamp, sweep_result

    def __check_signal_level(self, signal_level_):
        """
        Output impedance:100 Ohm, {0.0001~0.02}
        Output impedance: 25 Ohm, 0.0004~0.04:
        """
        error_message = f"*** [Set_cmu84] ErrorInput signal level '{signal_level_}'"
        if signal_level_ < 0.01 or signal_level_ > 2:  # todo
            sys.exit(error_message)
        else:
            return str(signal_level_)

    def __check_integration_time(self, integration_time_):
        # todo: 1,2,3,4 instead of fast...
        integration_modes = {0: "MAX", 1: "FAST", 2: "MED", 3: "SLOW", 4: "SLOW2"}  # 4770 : 1:SHOR , 2:MED , 3:LONG
        try:
            return integration_modes[integration_time_]
        except KeyError:
            error_message = f"*** [Set_cmu84] Integration is wrong {integration_time_}."
            sys.exit(error_message)

    def __check_measurement_range(self, range_):
        # todo: Measurement range 有分DC, AC?
        self.iwrite(f":MEAS:RANG:DC AUTO")
        # 1,2,3,4,HOLD, AUTO
        """
        看起來比較像是AC，因為他有不同freq的檔位
        """

    def __check_frequency(self, frequency_):
        # 10.0 ~ 30000000.0
        if 10.0 < frequency_ < 30000000.0:
            return frequency_
        else:
            sys.exit("frequency ErrorIncorrect Input Frequency Range:20Hz to 2MHz")

    def __check_voltage(self, volt_):
        if abs(volt_) > 12.000:  # todo: external voltage
            error_message = f"*** ERROR : Voltage '{volt_}' out of range -12~12V"
            sys.exit(error_message)
        else:
            return volt_

    def __check_voltage_0(self, *volts_):
        # checking program of set_iv, force_v
        for volt_i in volts_:
            if abs(volt_i) > 41:
                error_message = "*** ERROR : Voltage '{}' out of range -40~40V".format(volt_i)
                sys.exit(error_message)
        return volts_

    def __check_time(self, time_):
        if time_ < 0 or time_ > 651:
            error_message = f"*** ERROR : Given time '{time_}' is out of range 0~650"
            sys.exit(error_message)

    def __check_time0(self, *times_):

        # Check the delay and hold time, obey by the device limit
        for time_i in list(*times_):
            print(time_i)
            if time_i < 0 or time_i > 651:
                error_message = "*** ERROR : Given time '{}' is out of range 0~650".format(time_i)
                sys.exit(error_message)

    def __step_aperture(self, start_volt_, stop_volt_, steps_number_: int):
        # @return: Dc bias string's list ["point1_v", "point2_v", ....]
        if steps_number_ not in range(2, 1001):
            sys.exit("*** ERROR :  Input Step_Number is out of range 2~1001.".format(steps_number_))
        steps_lst_ = abs((stop_volt_ - start_volt_) / (steps_number_ - 1))
        return ["{0:.3f}".format(x) for x in np.arange(start_volt_, stop_volt_ + steps_lst_, steps_lst_)]

    def __set_cv_setting(self):
        self.iwrite('LIST:PARA CP,G')
        self.iwrite(f'LIST:FREQ {self.frequency}')
        self.iwrite("SWE:VOLT 0.03")  # AC
        self.iwrite(f"LIST:TRIG:MODE REPEAT")  # REP| SINGI | AUTO
        self.iwrite(f"LIST:RANG AUTO")  # AUTO / HOLD
        self.iwrite(f"LIST:RET ALL")  # OFF: no excute/STEP 1: only retest step 1/ ALL
        # self.iwrite(f"LIST:SPEE }")
        self.iwrite(f"SWE:TYPE BIASV")
        self.iwrite(f"SWE:TRIG:MODE REP")  # REP/SINGI
        self.iwrite(f"SWE:SPEE FAST")  # FAST/MED/SLOW
        self.iwrite(f"SWE:FUNC CP,G")  # todo

    def system_error(self):
        return self.iwrite('SYST:ERR?')
