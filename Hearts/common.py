from collections import abc
import sys
import os
import datetime
from functools import wraps
from time import time


error_format = "Category : {0}\nTable Type : {2}\nAlgorithm : {3}\nLine number : {4}\n\nError message : {1}\nHint : {5}\n"
rcp_error_format = "Category : {0}\nTest Item : {1}\nLine number : {2}\n\nError message : {3}\nHint : {4}\n"
fwk_error_format = "Category : {0}\n\nError message : {1}\nHint : {2}\n"
run_rcp_error_format = "Test Item : {0}\nAlgorithm Name: {1}\nSequence of test flow: {2}\n"


class JsonFileLoader:
    """Read Json file and turn json format to dynamic attributes
    Example :
            original : data['Algorithm]['AlgoName'][2]
            new : data.Algorithm.AlgoName[2]
    """

    def __init__(self, src):
        self.__data = dict(src)

    def __iter__(self):
        return iter(self.__data)

    def __getitem__(self, x):
        return self.__data[x]

    def __getattr__(self, item):
        if hasattr(self.__data, item):
            return getattr(self.__data, item)
        else:
            if item not in self.__data:
                raise KeyError(item)
            return JsonFileLoader.build(self.__data[item])

    @classmethod
    def build(cls, obj):
        # dict type
        if isinstance(obj, abc.Mapping):
            return cls(obj)
        # list type
        elif isinstance(obj, abc.MutableSequence):
            return [cls.build(item) for item in obj]
        else:
            return obj


def error_details():
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    return exc_type, fname, exc_tb.tb_lineno


def now():
    return datetime.datetime.now()


def timeit(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        # logger.debug("{},{}".format(func.__name__, t2 - t1))
        # with open('tis_time_analysis.log', 'at') as fout:
        #     fout.write("{},{}\n".format(func.__name__, t2 - t1))
        return result
    return wrapper