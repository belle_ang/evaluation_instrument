import time


def wait_time(wait_second=0):
    time.sleep(wait_second)


while True:
    stb = self.lcr.read_stb()
    print(str(stb))
    if str(stb) != "0":
        break
    else:
        wait_time(.1)


def sweep_exhaustive_range(self, start_v, stop_v, steps, hold_time, delay_time):
    time_start = time.perf_counter()
    wait_time(hold_time)

    sweep_result = '\nRANG, NO., DC , STATUS,  CAP [F]  ,  COND [S]  ,  TIME [s]  \n'
    print(self.set_cv84_3570(start_v, stop_v, steps))
    for pts, volt_i in enumerate(self.set_cv84_3570(start_v, stop_v, steps)):
        for range_num in range(1, 13):
            wait_time(delay_time)
            self.force_v_3570(volt_i)
            sweep_result += "{}, {}, {} ,".format(range_num, pts + 1, volt_i) + self.measure_cmu84_3570(
                range_num) + '\n'
        sweep_result += '\n'
    time_stamp = time.perf_counter()

    tis_status = 0

    return tis_status, time_stamp, sweep_result
