import socket
import sys

# 伺服器資訊


HOST = '127.0.0.1'
PORT = 8001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # 創建socket
s.connect((HOST, PORT))

# 不斷發送使用者輸入的訊息給伺服器
while True:
    msg = input("Please input msg: ")

    if msg == 'STOP':
        sys.exit('Closed')

    s.send(msg.encode('utf-8'))  # 發送
    # data = s.recv(1024)  # 接收伺服器訊息
    server_data = s.recv(1024).decode("utf-8").strip("b\'")
    print(f"Server reply: {server_data}\n")


# client_side.send(f"{cmd_send[count]}".encode())
# print(f'SEND: {cmd_send[count]}')
#
# data = client_side.recv(1024).decode("utf-8").strip("b\'")
# print(f"\tRECEIVED: {data} \n")
