import socket
from collections import OrderedDict
from datetime import datetime
import sys
import time

# Communication flow commands
cmds = OrderedDict()

cmds['ECR'] = 'ECR_0'
cmds['LSOTR'] = 'LSOTR_0'
cmds['ODIR_2_0001_0001_0001'] = 'ODIR_0'
cmds['TRR'] = 'TRR_2_0001'
cmds['LEOTR'] = 'LEOTR_0'
cmds['INI'] = 'INI_0'
cmds['RST'] = 'RST_0'

# Abnormal
cmds_abort = OrderedDict()
cmds_abort['ECR'] = 'ECR_1'
cmds_abort['LSOTR'] = 'LSOTR_1'
cmds_abort['ODIR_2_0001_0001_0001'] = 'ODIR_1'
cmds_abort['TRR'] = 'TRR_2_0001'
cmds_abort['LEOTR'] = 'LEOTR_1'
cmds_abort['INI'] = 'INI_1'
cmds_abort['RST'] = 'RST_18'

logger = open("Communication.log", 'a+')


def printf(word):
    word = f"\n{datetime.now().strftime('%H:%M:%S')} : {word}"
    print(word)
    logger.writelines(word)


def connect():
    try:
        HOST = input("IP: ")
        PORT = int(input("Port: "))

        # HOST, PORT = '127.0.0.1', 8001  # 設定要綁定的地址

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # 創建socket

        s.bind((HOST, PORT))  # 綁定
        s.listen(5)  # 監聽
        print("\n Waiting for Connection...")

        conn, addr = s.accept()

        printf(f'*** Connected by {addr} ***')

        return conn, addr

    except:
        printf(f"{HOST}:{PORT} is Illegal,Try Again!\n")
        connect()


conn, addr = connect()
# 進入無窮迴圈等代客戶端連線
# while True:
abnormal_ = False

error_cmd = None
# 連線成功後，不斷接收並印出資料，並回傳收到
while True:
    # data = conn.recv(1024)

    # if error_cmd != None:
    data = conn.recv(1024).decode("utf-8").strip("b\'")
    printf(f'RECEIVED : {data}')

    # 結束 STOP
    if not data or data == 'STOP':
        print('*** CLOSED ***\n\n')
        time.sleep(2)
        sys.exit("Server Closed!")


    elif data == 'RST':


    elif data == 'INI':

    try:
        # 如果data 有 *
        if '*' in data:
            msg2send = f"{cmds_abort[data]}"
            abnormal_ = True

        else:
            msg2send = f"{cmds[data]}"



    except KeyError:
        msg2send = '[ERROR] Illegal command!'
    # conn.send("server received you message.".encode('utf-8'))
    conn.send(msg2send.encode('utf-8'))
    printf(f'SEND : {msg2send.lstrip("b")}')

logger.close()
